package madcat.studio.interfaces;

import madcat.studio.data.CloudStatus;

public interface OnStatusListener {
	
	void setOnStatusListener(CloudStatus status);

}
