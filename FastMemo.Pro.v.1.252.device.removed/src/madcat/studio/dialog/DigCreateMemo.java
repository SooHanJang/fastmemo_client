package madcat.studio.dialog;

import madcat.studio.async.RequestWriteAsyncTask;
import madcat.studio.constants.Constants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.CloudUtils;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class DigCreateMemo extends Activity implements OnClickListener, OnEditorActionListener {

	private Context mContext;
	private SharedPreferences mConfigPref;

	private TextView mTvSubject;
	private ImageButton mBtnOk, mBtnCancle;
	private EditText mEditMemo;
	
	private RelativeLayout mRlRoot, mRlToolBar;
	private int mDigWidthPx;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.memo_create);
		this.mContext = this;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		
		mRlRoot = (RelativeLayout)findViewById(R.id.memo_create_root_layout);
		mRlToolBar = (RelativeLayout)findViewById(R.id.memo_create_tool_layout);
		mEditMemo = (EditText)findViewById(R.id.memo_create_content);
		mBtnOk = (ImageButton)findViewById(R.id.memo_create_btn_ok);
		mBtnCancle = (ImageButton)findViewById(R.id.memo_create_btn_cancle);
		
		mEditMemo.setOnEditorActionListener(this);
		mBtnOk.setOnClickListener(this);
		mBtnCancle.setOnClickListener(this);
		
		// 색상 설정 로드
		setToolBarColor(mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		mEditMemo.requestFocus();
		
		mDigWidthPx = Utils.pixelFromDip(mContext, (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE));
		mRlRoot.setLayoutParams(new RelativeLayout.LayoutParams(mDigWidthPx, mDigWidthPx));
		
	}
	
	private void setToolBarColor(int colorIdx) {
		if(mRlToolBar != null) {
	    	switch(colorIdx) {
		    	case MemoList.MAIN_COLOR_EMERALD:
		    		mRlToolBar.setBackgroundResource(R.color.main_color_emerald);
		    		break;
		    	case MemoList.MAIN_COLOR_PURPLE:
		    		mRlToolBar.setBackgroundResource(R.color.main_color_purple);
		    		break;
		    	case MemoList.MAIN_COLOR_PINK:
		    		mRlToolBar.setBackgroundResource(R.color.main_color_pink);
		    		break;
		    	case MemoList.MAIN_COLOR_BLUE:
		    		mRlToolBar.setBackgroundResource(R.color.main_color_blue);
		    		break;
	    	}
    	}
	}
	
	@Override
	protected void onUserLeaveHint() {
		Utils.logPrint(getClass(), "onUserLeaveHint 호출");
		
		super.onUserLeaveHint();
		
		String content = mEditMemo.getText().toString().trim();
		
		if(Utils.isTextIntegrity(content)) {
//			Utils.insertMemo(mContext, content);
		}
		
//		finish();
	}
	
	@Override
	protected void onPause() {
		Utils.logPrint(getClass(), "onPause 호출");
		
		super.onPause();
	}
	

	public void onClick(View v) {
		
		switch(v.getId()) {
			case R.id.memo_create_btn_ok:

				String content = mEditMemo.getText().toString().trim();
				long date = Utils.getCurrentTimeStamp();
				
				if(Utils.isTextIntegrity(content)) {
					
					Memo memo = new Memo(0, CloudUtils.doMakeCloudId(), date, content);
					
					int cloudState = mConfigPref.getInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_OFFLINE);
					
					switch(cloudState) {
					
						// 위젯을 통해 오프라인 메모를 작성할 경우,
						case Constants.CLOUD_STATE_OFFLINE:
							Utils.insertMemoToDb(mContext, memo);
							break;
						
						// 위젯을 통해 온라인 메모를 작성할 경우,
						case Constants.CLOUD_STATE_ONLINE:
							RequestWriteAsyncTask requestWriteAsync = 
								new RequestWriteAsyncTask(
										mContext, 
										CloudUtils.getLoginData(mContext, Login.DEFAULT_PASSWORD), 
										memo, 
										RequestWriteAsyncTask.WIDGET_WRITE_EXECUTE);
							
							requestWriteAsync.execute();
							
							requestWriteAsync.setOnStatusListener(new OnStatusListener() {
								public void setOnStatusListener(CloudStatus status) {
									
									// 서버와 응답에 성공했을 경우,
									if(status != null) {
										switch(status.getStatus()) {
											// 메모 작성에 성공하고, 목록을 성공적으로 불러왔을 경우,
											case CloudStatus.MEMO_SAVE_SUCCEED_LOAD_SUCCEED:
												CloudUtils.doCloudCheckToLocalDB(mContext, status.getData());
												break;
											
											// 메모 작성에 성공하고, 목록 불러오기에 실패했을 경우,
											case CloudStatus.MEMO_SAVE_SUCCEED_LOAD_FAILED:
												Toast.makeText(mContext, 
														getString(R.string.toast_title_modify_succeed_load_failed), Toast.LENGTH_SHORT).show();
												break;
											
											// 메모 작성에 실패했을 경우,
											case CloudStatus.MEMO_SAVE_FAILED:
												Toast.makeText(mContext, getString(R.string.toast_title_save_failed), Toast.LENGTH_SHORT).show();
												break;
	
											// 인증키가 유효하지 않을 시,
											case CloudStatus.LOGIN_INVALIDATE_AUTH:
												Utils.doInvalidateAuthLogout(mContext, null);
												break;
										}
										
									}
									// 서버와 응답에 실패했을 경우,
									else {
										Utils.logPrint(getClass(), "메모 작성 중 서버와 응답에 실패하였습니다.");
										CloudUtils.doLogout(mContext, null);
										Toast.makeText(mContext, getString(R.string.dig_msg_server_cannot_connected), Toast.LENGTH_SHORT).show();
									}
									
								}
							});
							
							break;
					
					}
					
					finish();
					
				} else {
					Toast.makeText(mContext, getString(R.string.toast_title_memo_integrity_fail), Toast.LENGTH_SHORT).show();
				}
				
				break;
			case R.id.memo_create_btn_cancle:
				finish();
				break;
		}
	}

	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		Utils.logPrint(getClass(), "onEditorAction call");

		if(actionId == EditorInfo.IME_ACTION_DONE) {
			mBtnOk.performClick();
		}
		
		return false;
	}
	

}








