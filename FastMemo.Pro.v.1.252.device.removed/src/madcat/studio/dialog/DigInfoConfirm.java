package madcat.studio.dialog;

import madcat.studio.constants.Constants;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DigInfoConfirm extends Dialog implements OnClickListener {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private LinearLayout mLiRoot, mLiSubjectBar;
	private TextView mTvTitle, mTvMsg;
	private Button mBtnOk;
	
	private int mDigWidthPx;
	private String mTitle, mMsg;
	
	public DigInfoConfirm(Context context) {
		super(context);
	}
	
	public DigInfoConfirm(Context context, String title, String msg) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		this.mTitle = title;
		this.mMsg = msg;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dig_info_confirm);
		
		// 테두리 제거
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		// Resource Id 초기화
		mLiRoot = (LinearLayout)findViewById(R.id.dig_info_confirm_root_layout);
		mLiSubjectBar = (LinearLayout)findViewById(R.id.dig_info_confirm_subject_layout);
		mTvTitle = (TextView)findViewById(R.id.dig_info_confirm_subject);
		mTvMsg = (TextView)findViewById(R.id.dig_info_confirm_msg);
		mBtnOk = (Button)findViewById(R.id.dig_info_confirm_btn_ok);
		
		// 다이얼로그 크기 설정
		mDigWidthPx = Utils.pixelFromDip(mContext, (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE));
		mLiRoot.setLayoutParams(new LinearLayout.LayoutParams(mDigWidthPx, LinearLayout.LayoutParams.WRAP_CONTENT));
		
		// 제목 바 색상 설정
		Utils.setToolBarColor(mLiSubjectBar, mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		
		// 타이틀 및 메시지 설정
		if(mTitle != null) {
			mTvTitle.setText(mTitle);
		}
		
		if(mMsg != null) {
			mTvMsg.setText(mMsg);
		}
		
		// EventListener 설정
		mBtnOk.setOnClickListener(this);
	}
	
	public void setTitle(String title) {
		this.mTitle = title;
	}
	
	public void setMessage(String msg) {
		this.mMsg = msg;
	}
	
	public void onClick(View v) {
		
		int id = v.getId();
		
		// 확인 버튼을 클릭 했을 경우,
		if(id == mBtnOk.getId()) {
			dismiss();
		}
	}
	
	@Override
	public void onBackPressed() {}

}















