package madcat.studio.dialog;

import java.util.ArrayList;

import madcat.studio.adapter.MemoListAdapter;
import madcat.studio.constants.Constants;
import madcat.studio.data.AppWidgetData;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.utils.Utils;
import android.app.ListActivity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class DigRegistMemoList extends ListActivity {
	
	private final float DIALOG_SIZE										=	0.8f;
	
	private Context mContext;
	
	private LinearLayout mLinearRoot;
	
	private ArrayList<Memo> mItems;
	private MemoListAdapter mAdapter;
	
	private AppWidgetManager mAppWidgetManaer;
	
	private int mAppWidgetId;
	private int mDisplayDpWidth, mDisplayDpHeight;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dig_memo_list);
		
		this.mContext = this;
		this.mAppWidgetManaer = AppWidgetManager.getInstance(mContext);
		
		this.getIntenter();
		
		mLinearRoot = (LinearLayout)findViewById(R.id.dig_memo_list_root);
		
		// Dialog Size 설정
		mDisplayDpWidth = (int) (Utils.getScreenDipWidthSize(mContext) * DIALOG_SIZE);
		
		mLinearRoot.setLayoutParams(
				new LinearLayout.LayoutParams(Utils.pixelFromDip(mContext, mDisplayDpWidth), 
						LinearLayout.LayoutParams.WRAP_CONTENT));
		
		// ListView 설정
		mItems = Utils.getMemoList(mContext);
		mAdapter = new MemoListAdapter(mContext, R.layout.memo_list_row, mItems);
		setListAdapter(mAdapter);

		// 작성된 메모가 존재하지 않을 시, 경고 메시지 출력
		if(mItems.isEmpty()) {
			Toast.makeText(mContext, getString(R.string.toast_title_no_memo_data), Toast.LENGTH_SHORT).show();
			
			setResult(RESULT_CANCELED);
			
			finish();
		}
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		
		if(extras != null) {
			mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
			
			String className = mAppWidgetManaer.getAppWidgetInfo(mAppWidgetId).provider.getClassName();
			
			Utils.logPrint(getClass(), "메모 목록을 호출한 위젯 id : " + mAppWidgetId + ", className : " + className);
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		
		mDisplayDpHeight = (int)(Utils.getScreenDipHeightSize(mContext) * DIALOG_SIZE);
		
		if(mLinearRoot.getHeight() >= Utils.pixelFromDip(mContext, mDisplayDpHeight)) {
			mLinearRoot.setLayoutParams(
					new LinearLayout.LayoutParams(Utils.pixelFromDip(mContext, mDisplayDpWidth), 
							Utils.pixelFromDip(mContext, mDisplayDpHeight)));
		}
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Utils.logPrint(getClass(), "Click item : " + position);

		Intent registMemoIntent = new Intent(Constants.ACTION_WIDGET_MEMO_REGIST);
		registMemoIntent.putExtra(Constants.PUT_WIDGET_ID, mAppWidgetId);
		registMemoIntent.putExtra(Constants.PUT_WIDGET_CONTENT, mItems.get(position).getContent());
		registMemoIntent.putExtra(Constants.PUT_WIDGET_DATE, mItems.get(position).getDate());
		sendBroadcast(registMemoIntent);

		// DB 에 현재 위젯 설정 값을 저장
		AppWidgetData widgetData = new AppWidgetData(mItems.get(position).getMemoId(), 
				mItems.get(position).getCloudId(), 
				mAppWidgetId, 
				mItems.get(position).getDate(), 
				mItems.get(position).getContent(), 
				1);
		Utils.insertWidgetMemo(mContext, widgetData);
		
		// 위젯 초기 설정이 업데이트 되었다는 것을 Return Intent 로 알림
		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
		setResult(RESULT_OK, resultValue);
		finish();
		
		super.onListItemClick(l, v, position, id);
	}
	
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		
		super.onBackPressed();
	}
	

}













