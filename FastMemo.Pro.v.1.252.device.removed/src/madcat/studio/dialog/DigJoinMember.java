package madcat.studio.dialog;

import madcat.studio.async.RequestJoinAsyncTask;
import madcat.studio.constants.Constants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Member;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.Utils;
import android.accounts.Account;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DigJoinMember extends Dialog implements OnClickListener {
	
	private DigJoinMember mDialog;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private int mDigWidthPx;
	
	private LinearLayout mLiRoot, mLiSubjectBar;
	private Button mBtnJoin, mBtnCancel;
	private TextView mTvAccount;
	private EditText mEditPwd, mEditPwdRe;
	
	private Account[] mAccounts;
	private String mSelectedAccount;
	
	private OnStatusListener mStatusListener;
	
	public DigJoinMember(Context context) {
		super(context);
		this.mContext = context;
		this.mDialog = this;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dig_join_member);
		
		// 테두리 제거
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		// Resource Id 초기화
		mLiRoot = (LinearLayout)findViewById(R.id.dig_join_member_root);
		mLiSubjectBar = (LinearLayout)findViewById(R.id.dig_join_member_subject);
		mTvAccount = (TextView)findViewById(R.id.dig_join_member_account);
		mEditPwd = (EditText)findViewById(R.id.dig_join_member_edit_pwd);
		mEditPwdRe = (EditText)findViewById(R.id.dig_join_member_edit_pwd_re);
		mBtnJoin = (Button)findViewById(R.id.dig_join_member_btn_join);
		mBtnCancel = (Button)findViewById(R.id.dig_join_member_btn_cancel);
		
		// 다이얼로그 크기 설정
		mDigWidthPx = Utils.pixelFromDip(mContext, (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE));
		mLiRoot.setLayoutParams(new LinearLayout.LayoutParams(mDigWidthPx, LinearLayout.LayoutParams.WRAP_CONTENT));
		
		// 제목 바 색상 설정
		Utils.setToolBarColor(mLiSubjectBar, mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		// EventListener 설정
		mBtnJoin.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// 계정 설정을 위한 TextView 초기화
		initTvAccounts();
		
		
	}
	
	// 스마트폰에 등록된 계정 목록을 가져와 TextView 에 설정한다.
	private void initTvAccounts() {
		if(mTvAccount != null) {
			
			// 계정 목록 배열을 가져온다.
			mAccounts = Utils.getAccountList(mContext);
			
			if(mAccounts != null && mAccounts.length > 0) {
				mSelectedAccount = mAccounts[0].name.toString();
				mTvAccount.setText(mSelectedAccount);
			}
			
		}
	}
	
	private void showLogInDialog(Context context) {
		DigLogInCloud digLoginCloud = new DigLogInCloud(context);
		digLoginCloud.show();
		
		dismiss();
	}
	
	public void onClick(View v) {
		
		int id = v.getId();
		
		// 회원 가입 확인 버튼 클릭 시,
		if(id == mBtnJoin.getId()) {
			
			String pwd = mEditPwd.getText().toString().trim();
			String pwdRe = mEditPwdRe.getText().toString().trim();
			
			if(pwd.length() < Constants.MEMBER_PWD_MAX_LENGTH || pwdRe.length() < Constants.MEMBER_PWD_MAX_LENGTH) {
				Toast.makeText(mContext, mContext.getString(R.string.toast_title_max_length_pwd), Toast.LENGTH_SHORT).show();
			} else {
				
				// 암호와 암호확인이 일치할 시, 서버에 회원 가입을 요청
				if(pwd.equals(pwdRe)) {
					
					if(Utils.isNetworkAvailable(mContext)) {
						// 회원 가입을 위한 Member 객체 생성
						Member member = new Member(mSelectedAccount, pwd);
						
						// 서버에 회원 가입을 요청
						RequestJoinAsyncTask requestJoinAsync = new RequestJoinAsyncTask(mContext, mDialog, member);
						requestJoinAsync.execute();
						
						requestJoinAsync.setOnStatusListener(new OnStatusListener() {
							public void setOnStatusListener(CloudStatus status) {
								if(mStatusListener != null) {
									mStatusListener.setOnStatusListener(status);
								}
							}
						});
					}
					
				} 
				// 불일치 시,
				else {
					Toast.makeText(mContext, mContext.getString(R.string.toast_title_incollect_re_pwd), Toast.LENGTH_SHORT).show();
				}
			}
		}
		// 회원 가입 취소 버튼 클릭 시,
		else if(id == mBtnCancel.getId()) {
			showLogInDialog(mContext);
		}
		
	}

	@Override
	public void onBackPressed() {
		showLogInDialog(mContext);
	}
	
	public void setOnStatusListener(OnStatusListener listener) {
		this.mStatusListener = listener;
	}
	
	

}











