package madcat.studio.dialog;

import madcat.studio.constants.Constants;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DigConfirm extends Dialog implements OnClickListener{
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private LinearLayout mLiRoot, mLiSubjectBar;
	private TextView mTvSubject, mTvMsg;
	private ImageView mIvSubjectIcon;
	private Button mBtnOk, mBtnCancel;
	
	private boolean mDigState										=	false;
	
	private int mDigWidthPx;

	private String mSubject;
	private String mMsg;
	private int mIconId;
	
	
	public DigConfirm(Context context, String subject, String msg, int iconId) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		this.mSubject = subject;
		this.mMsg = msg;
		this.mIconId = iconId;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dig_confirm);
		
		// 테두리 제거
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		mLiRoot = (LinearLayout)findViewById(R.id.dig_confirm_root_layout);
		mLiSubjectBar = (LinearLayout)findViewById(R.id.dig_confirm_subject_layout);
		mTvSubject = (TextView)findViewById(R.id.dig_confirm_subject);
		mIvSubjectIcon = (ImageView)findViewById(R.id.dig_confirm_subject_icon);
		mTvMsg = (TextView)findViewById(R.id.dig_confirm_title);
		mBtnOk = (Button)findViewById(R.id.dig_confirm_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dig_confirm_btn_cancel);
		
		// 다이얼로그 크기 설정
		mDigWidthPx = Utils.pixelFromDip(mContext, (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE));
		mLiRoot.setLayoutParams(new LinearLayout.LayoutParams(mDigWidthPx, LinearLayout.LayoutParams.WRAP_CONTENT));
		
		// 제목 바 색상 설정
		Utils.setToolBarColor(mLiSubjectBar, mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		// 제목 설정
		mTvSubject.setText(mSubject);
		
		// 제목 아이콘 설정
		mIvSubjectIcon.setBackgroundResource(mIconId);
		
		// 메시지 설정
		mTvMsg.setText(mMsg);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
	}
	
	public boolean getDigState() {
		return mDigState;
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dig_confirm_btn_ok:
				mDigState = true;
				dismiss();
				break;
			case R.id.dig_confirm_btn_cancel:
				mDigState = false;
				dismiss();
				break;
		}
	}

}








