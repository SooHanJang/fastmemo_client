package madcat.studio.dialog;

import java.util.ArrayList;

import madcat.studio.adapter.MemoListAdapter;
import madcat.studio.constants.Constants;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.fastmemo.pro.R.layout;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class DigOtherMemoList extends ListActivity {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private LinearLayout mLinearRoot;
	private TextView mTvTitleBar;
	
	private ArrayList<Memo> mItems;
	private MemoListAdapter mAdapter;
	
	private AppWidgetManager mAppWidgetManaer;
	
	private int mAppWidgetId;
	private int mDisplayDpWidth, mDisplayDpHeight;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dig_memo_list);
		
		this.mContext = this;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		this.getIntenter();
		
		mDisplayDpWidth = (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE);
		
		mLinearRoot = (LinearLayout)findViewById(R.id.dig_memo_list_root);
		mTvTitleBar = (TextView)findViewById(R.id.dig_memo_list_title_bar);
		
		mLinearRoot.setLayoutParams(
				new LinearLayout.LayoutParams(Utils.pixelFromDip(mContext, mDisplayDpWidth), 
						LinearLayout.LayoutParams.WRAP_CONTENT));
		
		setToolBarColor(mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		mItems = Utils.getMemoList(mContext);
		mAdapter = new MemoListAdapter(mContext, R.layout.memo_list_row, mItems);
		setListAdapter(mAdapter);
	}
	
	private void setToolBarColor(int colorIdx) {
		if(mTvTitleBar != null) {
			switch(colorIdx) {
		    	case MemoList.MAIN_COLOR_EMERALD:
		    		mTvTitleBar.setBackgroundResource(R.color.main_color_emerald);
		    		break;
		    	case MemoList.MAIN_COLOR_PURPLE:
		    		mTvTitleBar.setBackgroundResource(R.color.main_color_purple);
		    		break;
		    	case MemoList.MAIN_COLOR_PINK:
		    		mTvTitleBar.setBackgroundResource(R.color.main_color_pink);
		    		break;
		    	case MemoList.MAIN_COLOR_BLUE:
		    		mTvTitleBar.setBackgroundResource(R.color.main_color_blue);
		    		break;
			}
    	}
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Utils.logPrint(getClass(), "onWindowFocusChanged call");
		
		mDisplayDpHeight = (int)(Utils.getScreenDipHeightSize(mContext) * Constants.DIALOG_SIZE);
		
		if(mLinearRoot.getHeight() >= Utils.pixelFromDip(mContext, mDisplayDpHeight)) {
			mLinearRoot.setLayoutParams(
					new LinearLayout.LayoutParams(Utils.pixelFromDip(mContext, mDisplayDpWidth), 
							Utils.pixelFromDip(mContext, mDisplayDpHeight)));
		}
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		
		// 데이터베이스를 업데이트 한 뒤, 해당 위젯 메모를 갱신
		Utils.updateWidgetMemo(mContext, mAppWidgetId, mItems.get(position));
		
		Intent updateMemoIntent = new Intent(Constants.ACTION_WIDGET_MEMO_UPDATE);
		updateMemoIntent.putExtra(Constants.PUT_WIDGET_ID, mAppWidgetId);
		updateMemoIntent.putExtra(Constants.PUT_WIDGET_DATE, mItems.get(position).getDate());
		updateMemoIntent.putExtra(Constants.PUT_WIDGET_CONTENT, mItems.get(position).getContent());
		
		sendBroadcast(updateMemoIntent);
		
		finish();
		
		super.onListItemClick(l, v, position, id);
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mAppWidgetId = intent.getExtras().getInt(Constants.PUT_WIDGET_ID);
		}
	}

}





















