package madcat.studio.dialog;

import madcat.studio.async.RequestModifyAsyncTask;
import madcat.studio.async.RequestWriteAsyncTask;
import madcat.studio.constants.Constants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.CloudUtils;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class DigWriteNModifyMemo extends Dialog implements OnClickListener, OnEditorActionListener {

	public static final int DIG_MODE_MEMO_WRITE						=	1;
	public static final int DIG_MODE_MEMO_MODIFY					=	2;

	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private LinearLayout mLiCreateDate;
	private TextView mTvDate;
	private ImageView mIvDateBar;
	private EditText mEditContent;
	private ImageButton mBtnOk, mBtnCancle;
	
	private Memo mMemo;
	
	private OnStatusListener mStatusListener;
	
	private boolean mDigState										=	false;
	private int mMode												=	DIG_MODE_MEMO_MODIFY;
	
	private RelativeLayout mRlRoot, mRlToolBar;
	private int mDigWidthPx;
	
	public DigWriteNModifyMemo(Context context, int mode) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		this.mMode = mode;
	}
	
	public DigWriteNModifyMemo(Context context, Memo memo, int mode) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		this.mMemo = memo;
		this.mMode = mode;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		setContentView(R.layout.memo_create);
		
		// Resource Id 설정
		mRlRoot = (RelativeLayout)findViewById(R.id.memo_create_root_layout);
		mRlToolBar = (RelativeLayout)findViewById(R.id.memo_create_tool_layout);
		mLiCreateDate = (LinearLayout)findViewById(R.id.memo_create_date_layout);
		mTvDate = (TextView)findViewById(R.id.memo_create_date);
		mIvDateBar = (ImageView)findViewById(R.id.memo_create_date_seperator);
		mEditContent = (EditText)findViewById(R.id.memo_create_content);
		mBtnOk = (ImageButton)findViewById(R.id.memo_create_btn_ok);
		mBtnCancle = (ImageButton)findViewById(R.id.memo_create_btn_cancle);

		mEditContent.setOnEditorActionListener(this);
		mBtnOk.setOnClickListener(this);
		mBtnCancle.setOnClickListener(this);
		
		mDigWidthPx = Utils.pixelFromDip(mContext, (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE));
		mRlRoot.setLayoutParams(new RelativeLayout.LayoutParams(mDigWidthPx, mDigWidthPx));
		
		// 색상 설정 로드
		setToolBarColor(mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		switch(mMode) {
			case DIG_MODE_MEMO_MODIFY:
				// Load Data
				mLiCreateDate.setVisibility(View.VISIBLE);
				
				mTvDate.setText(Utils.getTimestampToDate(mMemo.getDate()));
				mEditContent.setText(mMemo.getContent());
				
				Utils.setLastEditCursor(mEditContent);
				
				break;
			case DIG_MODE_MEMO_WRITE:
				mEditContent.requestFocus();
				
				this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
				break;
		}
	}
	
	private void setToolBarColor(int colorIdx) {
		if(mRlToolBar != null) {
			switch(colorIdx) {
	    	case MemoList.MAIN_COLOR_EMERALD:
	    		mRlToolBar.setBackgroundResource(R.color.main_color_emerald);
	    		mIvDateBar.setBackgroundResource(R.color.main_color_emerald);
	    		break;
	    	case MemoList.MAIN_COLOR_PURPLE:
	    		mRlToolBar.setBackgroundResource(R.color.main_color_purple);
	    		mIvDateBar.setBackgroundResource(R.color.main_color_purple);
	    		break;
	    	case MemoList.MAIN_COLOR_PINK:
	    		mRlToolBar.setBackgroundResource(R.color.main_color_pink);
	    		mIvDateBar.setBackgroundResource(R.color.main_color_pink);
	    		break;
	    	case MemoList.MAIN_COLOR_BLUE:
	    		mRlToolBar.setBackgroundResource(R.color.main_color_blue);
	    		mIvDateBar.setBackgroundResource(R.color.main_color_blue);
	    		break;
			}
    	}
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.memo_create_btn_ok:
				String content = mEditContent.getText().toString().trim();
				int cloudState = mConfigPref.getInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_OFFLINE);
				
				switch(mMode) {
					case DIG_MODE_MEMO_MODIFY:
						Utils.logPrint(getClass(), "prev content : " + mMemo.getContent() + ", curr content : " + content);
						
						// 메모 내용이 변경되었다면,
						if(!content.equals(mMemo.getContent())) {
							if(Utils.isTextIntegrity(content)) {
								Utils.logPrint(getClass(), "메모 값이 정상적으로 변경 되었습니다.");
								
								mMemo.setDate(Utils.getCurrentTimeStamp());
								mMemo.setContent(content);
								
								Utils.logPrint(getClass(), "현재 클라우드 상태 : " + cloudState);
								
								switch(cloudState) {
									
									// Cloud Offline 시,
									case Constants.CLOUD_STATE_OFFLINE:
										Utils.updateMemoContent(mContext, mMemo);
										
										CloudStatus status = new CloudStatus(CloudStatus.NO_CLOUD_MEMO_MODIFY, String.valueOf(mMemo.getMemoId() + "," + mMemo.getDate()));
										if(mStatusListener != null) {
											mStatusListener.setOnStatusListener(status);
										}
										
										break;
									
									// Cloud Online 시,
									case Constants.CLOUD_STATE_ONLINE:
										
										RequestModifyAsyncTask requestModifyAsync = 
											new RequestModifyAsyncTask(mContext, 
													CloudUtils.getLoginData(mContext, Login.DEFAULT_PASSWORD), mMemo, RequestModifyAsyncTask.APP_MODIFY_EXECUTE);
										requestModifyAsync.execute();
										
										requestModifyAsync.setOnStatusListener(new OnStatusListener() {
											public void setOnStatusListener(CloudStatus status) {
												Utils.logPrint(getClass(), "메모를 수정하고 넘어온 Status : " + status);
												
												if(mStatusListener != null) {
													mStatusListener.setOnStatusListener(status);
												}
											}
										});
										
										break;
								}
								
								dismiss();
							} else {
								Toast.makeText(mContext, mContext.getString(R.string.toast_title_memo_integrity_fail), Toast.LENGTH_SHORT).show();
							}
						} else {
							dismiss();
						}
						
						break;
						
					case DIG_MODE_MEMO_WRITE:
						
						if(Utils.isTextIntegrity(content)) {
							
							long date = Utils.getCurrentTimeStamp();
							Memo memo = new Memo(0, CloudUtils.doMakeCloudId(), date, content);
							
							switch(cloudState) {
							
								// Cloud 서비스를 이용중이지 않을 경우,
								case Constants.CLOUD_STATE_OFFLINE:
									Utils.insertMemoToDb(mContext, memo);
									
									CloudStatus status = new CloudStatus(CloudStatus.NO_CLOUD_MEMO_SAVE, null);
									if(mStatusListener != null) {
										mStatusListener.setOnStatusListener(status);
									}
									
									break;
									
								// Cloud 서비스 이용 중일 경우,
								case Constants.CLOUD_STATE_ONLINE:
									
									RequestWriteAsyncTask requestWriteAsync = 
										new RequestWriteAsyncTask(
												mContext, 
												CloudUtils.getLoginData(mContext, null), 
												memo, 
												RequestWriteAsyncTask.APP_WRITE_EXECUTE);
									requestWriteAsync.execute();
									
									requestWriteAsync.setOnStatusListener(new OnStatusListener() {
										public void setOnStatusListener(CloudStatus status) {
											Utils.logPrint(getClass(), "메모를 작성하고 넘어온 Status : " + status);
											
											if(mStatusListener != null) {
												mStatusListener.setOnStatusListener(status);
											}
										}
									});
									
									break;
							
							}
							
							dismiss();
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.toast_title_memo_integrity_fail), Toast.LENGTH_SHORT).show();
						}
						
						break;
				
				}
				
				break;
			case R.id.memo_create_btn_cancle:
				dismiss();
				break;
		}
	}
	

	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		Utils.logPrint(getClass(), "onEditorAction call");
		
		if(actionId == EditorInfo.IME_ACTION_DONE) {
			mBtnOk.performClick();
		}
		
		return false;
	}

	
	public void setOnStatusListener(OnStatusListener listener) {
		this.mStatusListener = listener;
	}

}










