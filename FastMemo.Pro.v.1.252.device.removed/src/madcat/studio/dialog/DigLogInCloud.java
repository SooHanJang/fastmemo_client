package madcat.studio.dialog;

import madcat.studio.async.RequestLoginAsyncTask;
import madcat.studio.constants.Constants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.CloudUtils;
import madcat.studio.utils.Utils;
import android.accounts.Account;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DigLogInCloud extends Dialog implements OnClickListener {
	
	private Context mContext;
	private Dialog mDialog;
	private SharedPreferences mConfigPref;
	
	private int mDigWidthPx;
	private boolean mIsCancel									=	false;
	
	private LinearLayout mLiRoot, mLiSubjectBar;
	private TextView mTvAccount;
	private EditText mEditPwd;
	private Button mBtnLogin, mBtnCancel, mBtnJoin;
	
	private Account[] mAccounts;
	private String mSelectedAccount;
	
	private OnStatusListener mStatusListener;
	
	public DigLogInCloud(Context context) {
		super(context);
		this.mContext = context;
		this.mDialog = this;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dig_login_cloud);
		
		// 테두리 제거
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		// Resource Id 초기화
		mLiRoot = (LinearLayout)findViewById(R.id.dig_login_root);
		mLiSubjectBar = (LinearLayout)findViewById(R.id.dig_login_subject_layout);
		mTvAccount = (TextView)findViewById(R.id.dig_login_account);
		mEditPwd = (EditText)findViewById(R.id.dig_login_edit_pwd);
		mBtnJoin = (Button)findViewById(R.id.dig_login_clound_btn_join);
		mBtnLogin = (Button)findViewById(R.id.dig_login_clound_btn_login);
		mBtnCancel = (Button)findViewById(R.id.dig_login_clound_btn_cancel);
		
		// 다이얼로그 크기 설정
		mDigWidthPx = Utils.pixelFromDip(mContext, (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE));
		mLiRoot.setLayoutParams(new LinearLayout.LayoutParams(mDigWidthPx, LinearLayout.LayoutParams.WRAP_CONTENT));
		
		// 제목 바 색상 설정
		Utils.setToolBarColor(mLiSubjectBar, mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		// EventListener 설정
		mBtnJoin.setOnClickListener(this);
		mBtnLogin.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// 계정 설정을 위한 TextView 초기화
		initTvAccounts();
		
	}

	// 스마트폰에 등록된 계정 목록을 가져와 TextView 에 설정한다.
	private void initTvAccounts() {
		if(mTvAccount != null) {
			
			// 계정 목록 배열을 가져온다.
			mAccounts = Utils.getAccountList(mContext);
			
			if(mAccounts != null && mAccounts.length > 0) {
				mSelectedAccount = mAccounts[0].name.toString();
				mTvAccount.setText(mSelectedAccount);
			}
			
		}
	}
	
	public boolean isCanceled() {
		return mIsCancel;
	}
	
	public void onClick(View v) {
		
		int id = v.getId();
		
		// 로그인 버튼을 클릭 했을 경우,
		if(id == mBtnLogin.getId()) {
			
			Utils.logPrint(getClass(), "로그인 시도 계정 : " + mSelectedAccount);
			Utils.logPrint(getClass(), "패키지 네임 : " + mContext.getPackageName());
			
			String pwd = mEditPwd.getText().toString();
			
			// 암호가 null 이 아니거나, 8자리 이상일 경우,
			if(pwd != null && pwd.length() >= Constants.MEMBER_PWD_MAX_LENGTH) {
				
				// 네트워크를 이용가능할 경우,
				if(Utils.isNetworkAvailable(mContext)) {
					Login logIn = CloudUtils.getLoginData(mContext, pwd);
					RequestLoginAsyncTask requestLoginAsync = new RequestLoginAsyncTask(mContext, mDialog, logIn);
					requestLoginAsync.execute();
					
					requestLoginAsync.setOnStatusListener(new OnStatusListener() {
						public void setOnStatusListener(CloudStatus status) {
							if(mStatusListener != null) {
								Utils.logPrint(getClass(), "DigLoginCloud Status : " + status);
								
								mStatusListener.setOnStatusListener(status);
							}
						}
					});
					
					
				}
				
			} else {
				Toast.makeText(mContext, mContext.getString(R.string.toast_title_max_length_pwd), Toast.LENGTH_SHORT).show();
			}
		} 
		// 취소 버튼을 클릭 했을 경우,
		else if(id == mBtnCancel.getId()) {
			
			DigInfoConfirm digInfoConfirm = new DigInfoConfirm(mContext, 
					mContext.getString(R.string.tv_title_info), 
					mContext.getString(R.string.dig_msg_memo_no_login_explain));
			
			digInfoConfirm.show();
			
			digInfoConfirm.setOnDismissListener(new OnDismissListener() {
				public void onDismiss(DialogInterface dialog) {
					
				}
			});
			
			mIsCancel = true;
			
			dismiss();
			
		}
		// 회원 가입 버튼을 클릭 했을 경우,
		else if(id == mBtnJoin.getId()) {
			DigJoinMember digJoinMember = new DigJoinMember(mContext);
			digJoinMember.show();
			
			// 사용자가 회원 가입이 완료 된 후, 로그인 처리
			digJoinMember.setOnStatusListener(new OnStatusListener() {
				public void setOnStatusListener(CloudStatus status) {
					if(mStatusListener != null) {
						mStatusListener.setOnStatusListener(status);
					}
				}
			});
			
			dismiss();
		}
		
	}
	
	public void onBackPressed() {}
	
	public void setOnStatusListener(OnStatusListener listener) {	this.mStatusListener = listener;	}

}






























