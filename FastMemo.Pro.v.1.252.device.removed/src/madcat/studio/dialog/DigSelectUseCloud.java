package madcat.studio.dialog;

import madcat.studio.constants.Constants;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.utils.CloudUtils;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

public class DigSelectUseCloud extends Dialog implements android.view.View.OnClickListener {

	public static final int SELECT_NO_USED_CLOUD						=	0;
	public static final int SELECT_USED_CLOUD							=	1;
	
	private Context mContext;
	
	private int mDigWidthPx;
	private int mMode													=	SELECT_NO_USED_CLOUD;
	private SharedPreferences mConfigPref;
	
	private LinearLayout mLiRoot, mLiSubjectBar;
	private LinearLayout mLiUseCloud, mLiUseNoCloud;
	
	public DigSelectUseCloud(Context context) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dig_select_use_cloud);
		
		// 테두리 제거
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		// Resource Id 초기화
		mLiRoot = (LinearLayout)findViewById(R.id.dig_select_root);
		mLiSubjectBar = (LinearLayout)findViewById(R.id.dig_select_subject_bar);
		mLiUseCloud = (LinearLayout)findViewById(R.id.dig_select_use_login);
		mLiUseNoCloud = (LinearLayout)findViewById(R.id.dig_select_use_no_login);
		
		// 다이얼로그 크기 설정
		mDigWidthPx = Utils.pixelFromDip(mContext, (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE));
		mLiRoot.setLayoutParams(new LinearLayout.LayoutParams(mDigWidthPx, LinearLayout.LayoutParams.WRAP_CONTENT));
		
		// 제목 바 색상 설정
		Utils.setToolBarColor(mLiSubjectBar, mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		// EventListener 설정
		mLiUseCloud.setOnClickListener(this);
		mLiUseNoCloud.setOnClickListener(this);
		
	}
	
	public int getCloudMode() {
		return mMode;
	}

	public void onClick(View v) {
		
		int id = v.getId();
		
		// 클라우드 사용 클릭 시,
		if(id == mLiUseCloud.getId()) {
			mMode = SELECT_USED_CLOUD;
			
			dismiss();
		}
		// 클라우드 미사용 클릭 시,
		else if(id == mLiUseNoCloud.getId()) {
			mMode = SELECT_NO_USED_CLOUD;
			
			dismiss();
		}
	}
	
	@Override
	public void onBackPressed() {}
	
}
