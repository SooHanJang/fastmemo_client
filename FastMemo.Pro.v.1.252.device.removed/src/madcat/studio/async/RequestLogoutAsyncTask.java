package madcat.studio.async;

import madcat.studio.constants.HttpConstants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.HttpClientFactory;
import madcat.studio.utils.JSONUtils;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * 로그아웃을 서버로 요청하는 AsyncTask 
 * 
 * @author Acsha
 */
public class RequestLogoutAsyncTask extends AsyncTask<Void, Void, Void> {
	
	private Context mContext;
	
	private Login mLogin;
	private CloudStatus mStatus;

	private OnStatusListener mStatusListener;
	
	public RequestLogoutAsyncTask(Context context, Login login) {
		this.mContext = context;
		this.mLogin = login;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		Utils.logPrint(getClass(), "로그아웃 요청");
		
		HttpClientFactory.initializeClient();
		DefaultHttpClient httpClient = HttpClientFactory.getThreadSafeClient();
		
		HttpPost httpPost = new HttpPost(HttpConstants.DOMAIN_API_URL + HttpConstants.POSTFIX_URL_LOGOUT);
		HttpResponse httpResponse = null;
		
		JSONObject jsonObject = null;
		StringEntity entity = null;
		String response = null;
		
		try {
			jsonObject = new JSONObject();
			jsonObject.put(Login.NODE_ACCOUNT, mLogin.getAccount());
			jsonObject.put(Login.NODE_AUTH, mLogin.getAuth());
			
			entity = new StringEntity(jsonObject.toString(), HttpConstants.ENCODING_TYPE);
			entity.setContentType(HttpConstants.CONTENT_TYPE_SUCCEED);
			
			httpPost.setEntity(entity);
			httpResponse = httpClient.execute(httpPost);
			
		} catch(Exception e) {
			
			Utils.logPrint(getClass(), "Logout Error : " + e.getMessage());
			
		} finally {
			entity = null;
			jsonObject = null;
			httpPost = null;
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		if(mStatusListener != null) {
			mStatusListener.setOnStatusListener(mStatus);
		}
		
		Toast.makeText(mContext, mContext.getString(R.string.toast_title_complete_logout), Toast.LENGTH_SHORT).show();
		
	}
	
	public void setOnStatusListener(OnStatusListener listener) {	this.mStatusListener = listener;	}
	
}












