package madcat.studio.async;

import madcat.studio.constants.HttpConstants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Member;
import madcat.studio.dialog.DigInfoConfirm;
import madcat.studio.dialog.DigLogInCloud;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.HttpClientFactory;
import madcat.studio.utils.JSONUtils;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;

/**
 * 회원 가입을 서버로 요청하는 AsyncTask 
 * 
 * @author Acsha
 */
public class RequestJoinAsyncTask extends AsyncTask<Void, Void, Void> {
	
	private Context mContext;
	private Dialog mDialog;
	private ProgressDialog mProDig;
	
	private Member mMember;
	private CloudStatus mStatus;
	
	private OnStatusListener mStatusListener;
	
	
	public RequestJoinAsyncTask(Context context, Dialog dialog, Member member) {
		this.mContext = context;
		this.mDialog = dialog;
		this.mMember = member;
	}
	
	@Override
	protected void onPreExecute() {
		mProDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.prodig_title_cloud_join_server));
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		Utils.logPrint(getClass(), "회원 가입 요청");
		
		HttpClientFactory.initializeClient();
		DefaultHttpClient httpClient = HttpClientFactory.getThreadSafeClient();
		
		HttpPost httpPost = new HttpPost(HttpConstants.DOMAIN_API_URL + HttpConstants.POSTFIX_URL_JOIN);
		HttpResponse httpResponse = null;
		
		JSONObject jsonObject = null;
		StringEntity entity = null;
		String response = null;
		
		try {
			String convertSHA = Utils.convertPwdToSHA256(mMember.getPwd());
			
			// 성공적으로 암호를 SHA 로 변환하였다면, 
			if(convertSHA != null) {
				
				jsonObject = new JSONObject();
				jsonObject.put(Member.NODE_ACCOUNT, mMember.getAccount());
				jsonObject.put(Member.NODE_PWD, convertSHA);
				
				entity = new StringEntity(jsonObject.toString(), HttpConstants.ENCODING_TYPE);
				entity.setContentType(HttpConstants.CONTENT_TYPE_SUCCEED);
				
				httpPost.setEntity(entity);
				httpResponse = httpClient.execute(httpPost);
				
				response = EntityUtils.toString(httpResponse.getEntity(), HttpConstants.ENCODING_TYPE);
				Utils.logPrint(getClass(), "Join Response : " + response);
				
				mStatus = JSONUtils.getStatusToParse(response);
				Utils.logPrint(getClass(), "joinStatus : " + mStatus);
			} 
			
		} catch(Exception e) {
			
			Utils.logPrint(getClass(), "Error : " + e.getMessage());
			
		} finally {
			entity = null;
			jsonObject = null;
			httpPost = null;
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		if(mProDig != null) {
			mProDig.dismiss();
		}
		
		if(mStatusListener != null) {
			mStatusListener.setOnStatusListener(mStatus);
		}
		
		mDialog.dismiss();
		
	}
	
	public void setOnStatusListener(OnStatusListener listener) {
		this.mStatusListener = listener;
	}
	
	
	
}














