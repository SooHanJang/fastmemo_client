package madcat.studio.async;

import madcat.studio.constants.HttpConstants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.HttpClientFactory;
import madcat.studio.utils.JSONUtils;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * 로그인을 서버로 요청하는 AsyncTask 
 * 
 * @author Acsha
 */
public class RequestLoginAsyncTask extends AsyncTask<Void, Void, Void> {
	
	private Context mContext;
	private Dialog mDialog;
	private ProgressDialog mProDig;
	
	private Login mLogin;
	private CloudStatus mStatus;

	private OnStatusListener mStatusListener;
	
	public RequestLoginAsyncTask(Context context, Dialog dialog, Login login) {
		this.mContext = context;
		this.mDialog = dialog;
		this.mLogin = login;
	}
	
	@Override
	protected void onPreExecute() {
		mProDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.prodig_title_cloud_login_server));
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		Utils.logPrint(getClass(), "로그인 요청");
		
		HttpClientFactory.initializeClient();
		DefaultHttpClient httpClient = HttpClientFactory.getThreadSafeClient();
		
		HttpPost httpPost = new HttpPost(HttpConstants.DOMAIN_API_URL + HttpConstants.POSTFIX_URL_LOGIN);
		HttpResponse httpResponse = null;
		
		JSONObject jsonObject = null;
		StringEntity entity = null;
		String response = null;
		
		try {
			String convertSHA = Utils.convertPwdToSHA256(mLogin.getPwd());
			
			// 성공적으로 암호를 SHA 로 변환하였다면, 
			if(convertSHA != null) {
				jsonObject = new JSONObject();
				jsonObject.put(Login.NODE_ACCOUNT, mLogin.getAccount());
				jsonObject.put(Login.NODE_PWD, convertSHA);
				jsonObject.put(Login.NODE_TYPE, mLogin.getType());
				
				entity = new StringEntity(jsonObject.toString(), HttpConstants.ENCODING_TYPE);
				entity.setContentType(HttpConstants.CONTENT_TYPE_SUCCEED);
				
				httpPost.setEntity(entity);
				httpResponse = httpClient.execute(httpPost);
				
				response = EntityUtils.toString(httpResponse.getEntity(), HttpConstants.ENCODING_TYPE);
				Utils.logPrint(getClass(), "Login Response : " + response);
				
				mStatus = JSONUtils.getStatusToParse(response);
				Utils.logPrint(getClass(), "LoginStatus : " + mStatus);
			} 
			
		} catch(Exception e) {
			
			Utils.logPrint(getClass(), "Error : " + e.getMessage());
			
		} finally {
			entity = null;
			jsonObject = null;
			httpPost = null;
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		if(mProDig != null) {
			mProDig.dismiss();
		}
		
		if(mStatusListener != null) {
			mStatusListener.setOnStatusListener(mStatus);
		}
		
		mDialog.dismiss();
		
	}
		
		
		// 서버와 정상적으로 통신 했을 시,
//		if(mStatus != null) {
//			if(mStatusListener != null) {
//				mStatusListener.onLoginListener(mStatus.getStatus());
//			}
//			
//			switch(mStatus.getStatus()) {
//			
//				// 로그인 성공 시,
//				case CloudStatus.LOGIN_SUCCEED:
//					Utils.logPrint(getClass(), "로그인 성공, 인증키(" + mStatus.getData() + ")");
//					
//					// 로그인 모드로 변경
//					CloudUtils.doLogin(mContext, mStatus.getData());
//					
//					if(mLoginListener != null) {
//						mLoginListener.onLoginListener(true);
//					}
//					
//					// 메모 목록을 요청한다.
//					
//					break;
//				
//				// 로그인 실패 시(존재하지 않는 회원),
//				case CloudStatus.LOGIN_NO_MEMBER:
//					Utils.logPrint(getClass(), "로그인 실패(존재하지 않는 회원)");
//					
//					if(mLoginListener != null) {
//						mLoginListener.onLoginListener(false);
//					}
//					
//					break;
//					
//				// 로그인 암호가 틀렸을 시,
//				case CloudStatus.LOGIN_INVALIDATE_PWD:
//					Utils.logPrint(getClass(), "로그인 암호 틀림");
//					
//					if(mLoginListener != null) {
//						mLoginListener.onLoginListener(false);
//					}
//					
//					break;
//				
//				// 로그인 실패 시(인증키 유효하지 않음),
//				case CloudStatus.LOGIN_INVALIDATE_AUTH:
//					Utils.logPrint(getClass(), "로그인 실패(인증키 유효하지 않음)");
//					
//					if(mLoginListener != null) {
//						mLoginListener.onLoginListener(false);
//					}
//					
//					CloudUtils.doLogout(mContext);
//					
//					
//					break;
//				
//				// 에러 발생 시,
//				default:
//					Utils.logPrint(getClass(), "알 수 없는 에러 발생");
//					
//					if(mLoginListener != null) {
//						mLoginListener.onLoginListener(false);
//					}
//					
//					break;
//		
//			}
//		}
//		// 서버와 통신에 실패했을 시,
//		else {
//			Utils.logPrint(getClass(), "서버와 연결 실패");
//			
//			if(mStatusListener != null) {
//				mStatusListener.onLoginListener(CloudStatus.SERVER_CANNOT_CONNECTED);
//			}
//		}
	
	public void setOnStatusListener(OnStatusListener listener) {	this.mStatusListener = listener;	}
	
}












