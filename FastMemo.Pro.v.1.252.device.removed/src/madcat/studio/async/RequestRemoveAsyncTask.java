package madcat.studio.async;

import java.util.ArrayList;

import madcat.studio.constants.HttpConstants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.HttpClientFactory;
import madcat.studio.utils.JSONUtils;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * 삭제할 메모를 서버로 전송하는 AsyncTask 
 * 
 * @author Acsha
 */
public class RequestRemoveAsyncTask extends AsyncTask<Void, Void, Void> {
	
	private Context mContext;
	private ProgressDialog mProDig;

	private Login mLogin;
	private ArrayList<Memo> mMemoArray;
	private CloudStatus mStatus;
	
	private OnStatusListener mStatusListener;
	
	public RequestRemoveAsyncTask(Context context, Login login, ArrayList<Memo> memoArray) {
		this.mContext = context;
		this.mLogin = login;
		this.mMemoArray = memoArray;
	}
	
	@Override
	protected void onPreExecute() {
		mProDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.prodig_title_cloud_sync_server));
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		Utils.logPrint(getClass(), "메모 삭제 요청");

		// 서버에 동기화된 메모가 삭제되었음을 알린다.
		HttpClientFactory.initializeClient();
		DefaultHttpClient httpClient = HttpClientFactory.getThreadSafeClient();
		
		HttpPost httpPost = new HttpPost(HttpConstants.DOMAIN_API_URL + HttpConstants.POSTFIX_URL_MEMO_REMOVE);
		HttpResponse httpResponse = null;
		
		JSONObject jsonObject = null;
		StringEntity entity = null;
		String response = null;
		
		try {
			
			// 전송 프로토콜 (account, auth, type, data(삭제할 메모[]))
			jsonObject = new JSONObject();
			jsonObject.put(Login.NODE_ACCOUNT, mLogin.getAccount());
			jsonObject.put(Login.NODE_AUTH, mLogin.getAuth());
			jsonObject.put(Login.NODE_TYPE, mLogin.getType());
			jsonObject.put(CloudStatus.NODE_DATA, JSONUtils.convertRemoveMemoIdArrayToJSON(mMemoArray));
			
			entity = new StringEntity(jsonObject.toString(), HttpConstants.ENCODING_TYPE);
			entity.setContentType(HttpConstants.CONTENT_TYPE_SUCCEED);
			
			httpPost.setEntity(entity);
			httpResponse = httpClient.execute(httpPost);
			
			response = EntityUtils.toString(httpResponse.getEntity(), HttpConstants.ENCODING_TYPE);
			Utils.logPrint(getClass(), "MemoRemove Response : " + response);
			
			mStatus = JSONUtils.getStatusToParse(response);
			Utils.logPrint(getClass(), "MemoRemove Status : " + mStatus);
			
		} catch(Exception e) {
			
			Utils.logPrint(getClass(), "Error : " + e.getMessage());
			
		} finally {
			entity = null;
			jsonObject = null;
			httpPost = null;
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		if(mProDig != null) {
			mProDig.dismiss();
		}
		
		if(mStatus != null) {
			if(mStatus.getStatus() == CloudStatus.MEMO_DELETE_SUCCEED_LOAD_FAILED) {
				Utils.deleteMemo(mContext, mMemoArray);
			}
		}
		
		
		if(mStatusListener != null) {
			mStatusListener.setOnStatusListener(mStatus);
		}
	}
	
	public void setOnStatusListener(OnStatusListener listener) {
		this.mStatusListener = listener;
	}
	
	
	
}














