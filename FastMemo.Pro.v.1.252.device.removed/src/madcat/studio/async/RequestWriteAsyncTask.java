package madcat.studio.async;

import madcat.studio.constants.HttpConstants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.HttpClientFactory;
import madcat.studio.utils.JSONUtils;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * 작성한 메모를 서버로 전송하는 AsyncTask 
 * 
 * @author Acsha
 */
public class RequestWriteAsyncTask extends AsyncTask<Void, Void, Void> {

	public static final int APP_WRITE_EXECUTE				=	0;
	public static final int WIDGET_WRITE_EXECUTE			=	1;
	
	private Context mContext;
	private ProgressDialog mProDig;

	private Login mLogin;
	private Memo mMemo;
	private CloudStatus mStatus;
	
	private int mExecuteType;
	
	private OnStatusListener mStatusListener;
	
	
	public RequestWriteAsyncTask(Context context, Login login, Memo memo, int executeType) {
		this.mContext = context;
		this.mLogin = login;
		this.mMemo = memo;
		
		this.mExecuteType = executeType;
	}
	
	@Override
	protected void onPreExecute() {
		if(mExecuteType == RequestWriteAsyncTask.APP_WRITE_EXECUTE) {
			mProDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.prodig_title_cloud_sync_server));
		}
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		HttpClientFactory.initializeClient();
		DefaultHttpClient httpClient = HttpClientFactory.getThreadSafeClient();
		
		HttpPost httpPost = new HttpPost(HttpConstants.DOMAIN_API_URL + HttpConstants.POSTFIX_URL_MEMO_WRITE);
		HttpResponse httpResponse = null;
		
		JSONObject jsonObject = null;
		StringEntity entity = null;
		String response = null;
		
		try {
			// 전송 프로토콜 (account, auth, type, data(작성한 메모))
			jsonObject = new JSONObject();
			jsonObject.put(Login.NODE_ACCOUNT, mLogin.getAccount());
			jsonObject.put(Login.NODE_AUTH, mLogin.getAuth());
			jsonObject.put(Login.NODE_TYPE, mLogin.getType());
			jsonObject.put(CloudStatus.NODE_DATA, JSONUtils.convertMemoToJSON(mMemo));
			
			entity = new StringEntity(jsonObject.toString(), HttpConstants.ENCODING_TYPE);
			entity.setContentType(HttpConstants.CONTENT_TYPE_SUCCEED);
			
			httpPost.setEntity(entity);
			httpResponse = httpClient.execute(httpPost);
			
			response = EntityUtils.toString(httpResponse.getEntity(), HttpConstants.ENCODING_TYPE);
			Utils.logPrint(getClass(), "MemoWrite Response : " + response);
			
			mStatus = JSONUtils.getStatusToParse(response);
			Utils.logPrint(getClass(), "MemoWrite Status : " + mStatus);
			
			// 메모가 서버에 성공적으로 저장되었다면, LocalDB 에 해당 메모를 추가한다.
			if(mStatus != null) {
				if(mStatus.getStatus() == CloudStatus.MEMO_SAVE_SUCCEED_LOAD_SUCCEED ||
						mStatus.getStatus() == CloudStatus.MEMO_SAVE_SUCCEED_LOAD_FAILED) {
					
					Utils.insertMemoToDb(mContext, mMemo);
				}
			}
		} catch(Exception e) {
			
			Utils.logPrint(getClass(), "Error : " + e.getMessage());
			
		} finally {
			entity = null;
			jsonObject = null;
			httpPost = null;
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		if(mProDig != null) {
			mProDig.dismiss();
		}
		
		if(mStatusListener != null) {
			mStatusListener.setOnStatusListener(mStatus);
		}
	}
	
	public void setOnStatusListener(OnStatusListener listener) {
		this.mStatusListener = listener;
	}
	
	
	
}














