package madcat.studio.async;

import java.util.ArrayList;

import madcat.studio.constants.HttpConstants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.HttpClientFactory;
import madcat.studio.utils.JSONUtils;
import madcat.studio.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * App과 서버의 메모를 동기화하는 AsyncTask 
 * 
 * @author Acsha
 */
public class RequestSyncnorizeAsyncTask extends AsyncTask<Void, Void, Void> {
	
	private Context mContext;
	private ProgressDialog mProDig;

	private Login mLogin;
	private ArrayList<Memo> mMemoArray;
	private CloudStatus mStatus;
	
	private OnStatusListener mStatusListener;
	
	public RequestSyncnorizeAsyncTask(Context context, Login login) {
		this.mContext = context;
		this.mLogin = login;
	}
	
	@Override
	protected void onPreExecute() {
		mProDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.prodig_title_cloud_sync_server));
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		Utils.logPrint(getClass(), "DB에 저장된 메모 목록 요청");
		
		mMemoArray = Utils.getMemoList(mContext);
		
		Utils.logPrint(getClass(), "서버와 동기화 요청");
		HttpClientFactory.initializeClient();
		DefaultHttpClient httpClient = HttpClientFactory.getThreadSafeClient();
		
		HttpPost httpPost = new HttpPost(HttpConstants.DOMAIN_API_URL + HttpConstants.POSTFIX_URL_SYNC);
		HttpResponse httpResponse = null;
		
		JSONObject jsonObject = null;
		StringEntity entity = null;
		String response = null;
		
		try {
			// 전송 프로토콜 (account, auth, type, data(작성한 메모 목록))
			jsonObject = new JSONObject();
			jsonObject.put(Login.NODE_ACCOUNT, mLogin.getAccount());
			jsonObject.put(Login.NODE_AUTH, mLogin.getAuth());
			jsonObject.put(Login.NODE_TYPE, mLogin.getType());
			jsonObject.put(CloudStatus.NODE_DATA, JSONUtils.convertMemoArrayToJSON(mMemoArray));
			
			entity = new StringEntity(jsonObject.toString(), HttpConstants.ENCODING_TYPE);
			entity.setContentType(HttpConstants.CONTENT_TYPE_SUCCEED);
			
			httpPost.setEntity(entity);
			httpResponse = httpClient.execute(httpPost);
			
			response = EntityUtils.toString(httpResponse.getEntity(), HttpConstants.ENCODING_TYPE);
			Utils.logPrint(getClass(), "Sync Response : " + response);
			
			mStatus = JSONUtils.getStatusToParse(response);
			Utils.logPrint(getClass(), "Sync Status : " + mStatus);
			
		} catch(Exception e) {
			
			Utils.logPrint(getClass(), "Sync Error : " + e.getMessage());
			
		} finally {
			entity = null;
			jsonObject = null;
			httpPost = null;
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		Utils.logPrint(getClass(), "onPost 실행");
		
		
		if(mProDig != null) {
			mProDig.dismiss();
		}
		
		if(mStatusListener != null) {
			mStatusListener.setOnStatusListener(mStatus);
		}
	}
	
	public void setOnStatusListener(OnStatusListener listener) {
		this.mStatusListener = listener;
	}
	
	
	
}














