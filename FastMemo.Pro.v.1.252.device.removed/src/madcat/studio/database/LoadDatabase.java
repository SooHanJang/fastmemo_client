package madcat.studio.database;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import madcat.studio.constants.Constants;
import android.content.Context;
import android.content.res.AssetManager;

public class LoadDatabase {
	
	public static boolean loadDataBase(AssetManager assetManager, Context context) {
		boolean loadFlag = true;
		
		InputStream[] arrIs = new InputStream[Constants.DATABASE_FILE_INDEX];
		BufferedInputStream[] arrBis = new BufferedInputStream[Constants.DATABASE_FILE_INDEX];
		
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		
		try {
			File location = new File(Constants.DATABASE_ROOT_DIR);
			location.mkdirs();
			File f = new File(Constants.DATABASE_ROOT_DIR + Constants.DATABASE_NAME);
			if(f.exists()) {
				f.delete();
				f.createNewFile();
			}
			
			for(int i=0; i < arrIs.length; i++) {
				arrIs[i] = assetManager.open("FastMemoDb" + (i+1) + ".db");
				arrBis[i] = new BufferedInputStream(arrIs[i]);
			}

			fos = new FileOutputStream(f);
			bos = new BufferedOutputStream(fos);
			
			int read = -1;
			byte[] buffer = new byte[1024];
			
			for(int i=0; i < arrIs.length; i++) {
				while((read = arrBis[i].read(buffer, 0, 1024)) != -1) {
					bos.write(buffer, 0, read);
				}
				bos.flush();
			}
		} catch (Exception e) {
			loadFlag = false;
		} finally {
			for(int i=0; i < arrIs.length; i++) {
				try {
					if(arrIs[i] != null) { 
						arrIs[i].close();
					}
				} catch(Exception e) {
				} try {
					if(arrBis[i] != null) {
						arrBis[i].close();
					}
				} catch(Exception e) {
				}
			}
			try {
				if(fos != null) {
					fos.close();
				}
			} catch(Exception e) {}
			try {
				if(bos != null) {
					bos.close();
				}
			} catch(Exception e) {}
			
			arrIs = null;
			arrBis = null;
		}
		
		return loadFlag;
    }
}
