package madcat.studio.data;

public class Memo {
	
	public static final int DEFAULT_MEMO_ID					=	0;
	
	public static final String NODE_CLOUD_ID				=	"_id";
	public static final String NODE_DATE					=	"date";
	public static final String NODE_MEMO					=	"memo";

	private int mMemoId;
	private long mDate;
	private String mCloudId;
	private String mContent;
	
	public Memo(int memoId, String cloudId, long date, String content) {
		this.mMemoId = memoId;	
		this.mCloudId = cloudId;
		this.mDate = date;		
		this.mContent = content;
	}
	
	public int getMemoId() {
		return mMemoId;
	}
	public void setMemoId(int memoId) {
		this.mMemoId = memoId;
	}
	
	public String getCloudId() {
		return mCloudId;
	}
	public void setCloudId(String cloudId) {
		this.mCloudId = cloudId;
	}
	
	
	public long getDate() {
		return mDate;
	}
	public void setDate(long date) {
		this.mDate = date;
	}
	
	public String getContent() {
		return mContent;
	}
	public void setContent(String content) {
		this.mContent = content;
	}
}
