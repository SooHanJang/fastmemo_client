package madcat.studio.data;

public class Login {
	
	public static final int TYPE_FREE							=	0;
	public static final int TYPE_PRO							=	1;
	
	public static final String NODE_ACCOUNT						=	"account";
	public static final String NODE_PWD							=	"pwd";
	public static final String NODE_AUTH						=	"auth";
	public static final String NODE_TYPE						=	"type";
	
	public static final String DEFAULT_PASSWORD					=	null;
	
	private String mAccount;
	private String mPwd;
	private String mAuth;
	private int mType;
	
	public Login(String account, String pwd, String auth, int type) {
		this.mAccount = account;
		this.mPwd = pwd;
		this.mAuth = auth;
		this.mType = type;
	}
	
	public String getAccount() {
		return mAccount;
	}
	public void setAccount(String account) {
		this.mAccount = account;
	}
	public String getPwd() {
		return mPwd;
	}
	public void setPwd(String pwd) {
		this.mPwd = pwd;
	}
	public String getAuth() {
		return mAuth;
	}
	public void setAuth(String auth) {
		this.mAuth = auth;
	}
	public int getType() {
		return mType;
	}
	public void setType(int type) {
		this.mType = type;
	}

}
