package madcat.studio.data;

public class AppWidgetData extends Memo{

	private int mWidgetId, mThemeIdx;
	
	public AppWidgetData(int memoId, String cloudId, int widgetId, long date, String content, int themeIdx) {
		super(memoId, cloudId, date, content);
		
		this.mWidgetId = widgetId;
		this.mThemeIdx = themeIdx;
	}
	
	public int getWidgetId() {
		return mWidgetId;
	}
	public void setWidgetId(int widgetId) {
		this.mWidgetId = widgetId;
	}
	public int getThemeIdx() {
		return mThemeIdx;
	}
	public void setThemeIdx(int themeIdx) {
		this.mThemeIdx = themeIdx;
	}
	public String getWidgetContent() {
		return super.getContent();
	}
	public void setWidgetContent(String content) {
		super.setContent(content);
	}

}
