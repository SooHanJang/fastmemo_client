package madcat.studio.data;

public class CloudStatus {
	
	// 노드 관련 변수
	public static final String NODE_STATUS						=	"status";
	public static final String NODE_DATA						=	"data";
	
	// 로그인 관련 Status
	public static final int LOGIN_SUCCEED						=	100;
	public static final int LOGIN_NO_MEMBER						=	101;
	public static final int LOGIN_INVALIDATE_PWD				=	102;
	public static final int LOGIN_INVALIDATE_AUTH				=	103;
	
	// 회원 가입 관련 Status
	public static final int JOIN_SUCCEED						=	200;
	public static final int JOIN_FAILED							=	201;
	public static final int JOIN_DUPLICATED						=	202;
	
	// 메모 관련 Status
	public static final int MEMO_LIST_LOAD_SUCCEED				=	300;
	public static final int MEMO_LIST_LOAD_FAILED				=	301;
	
	public static final int MEMO_SYNC_SUCCEED					=	300;
	public static final int MEMO_SYNC_FAILED					=	301;
	
	public static final int MEMO_SAVE_SUCCEED_LOAD_SUCCEED		=	310;
	public static final int MEMO_SAVE_SUCCEED_LOAD_FAILED		=	311;
	public static final int MEMO_SAVE_FAILED					=	302;
	
	public static final int MEMO_DELETE_SUCCEED_LOAD_SUCCEED	=	320;
	public static final int MEMO_DELETE_SUCCEED_LOAD_FAILED		=	321;
	public static final int MEMO_DELETE_FAILED					=	322;
	
	public static final int MEMO_MODIFY_SUCCEED_LOAD_SUCCEED	=	330;
	public static final int MEMO_MODIFY_SUCCEED_LOAD_FAILED		=	331;
	public static final int MEMO_MODIFY_FAILED					=	332;
	
	
	// 클라우드 서비스 미이용시 Status
	public static final int NO_CLOUD_MEMO_SAVE					=	400;
	public static final int NO_CLOUD_MEMO_MODIFY				=	410;
	
	public static final int SERVER_CANNOT_CONNECTED				=	998;
	public static final int SERVER_UNKNOWN_ERROR				=	999;
	

	private int mStatus;
	private String mData;
	
	public CloudStatus(int status, String data) {
		this.mStatus = status;
		this.mData = data;
	}
	
	public int getStatus() {
		return mStatus;
	}
	public void setStatus(int status) {
		this.mStatus = status;
	}
	public String getData() {
		return mData;
	}
	public void setData(String data) {
		this.mData = data;
	}
	
	

}
