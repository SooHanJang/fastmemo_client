package madcat.studio.data;

public class Member {
	
	public static final String NODE_ACCOUNT				=	"account";
	public static final String NODE_PWD					=	"pwd";
	
	public static final String NODE_STATUS				=	"status";
	
	private String mAccount;
	private String mPwd;
	
	public Member(String account, String pwd) {
		this.mAccount = account;
		this.mPwd = pwd;
	}
	
	public String getAccount() {
		return mAccount;
	}
	public void setAccount(String account) {
		this.mAccount = account;
	}
	
	public String getPwd() {
		return mPwd;
	}
	public void setPwd(String pwd) {
		this.mPwd = pwd;
	}

}
