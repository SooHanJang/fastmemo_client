package madcat.studio.constants;

public class HttpConstants {
	
	public static final String ENCODING_TYPE							=	"UTF-8";
	public static final String CONTENT_TYPE_SUCCEED						=	"application/json";
	public static final String CONTENT_TYPE_FAILED						=	"text/plain";
	
//	public static final String DOMAIN_API_URL							=	"http://182.222.19.188:4000/api/";
	public static final String DOMAIN_API_URL							=	"http://192.168.0.5:4000/api/";
	
	// 회원 가입 관련 변수
	public static final String POSTFIX_URL_JOIN							=	"join";
	
	// 로그인 관련 변수
	public static final String POSTFIX_URL_LOGIN						=	"login";
	
	// 동기화 관련 변수
	public static final String POSTFIX_URL_SYNC							=	"memo/sync";
	
	// 메모 관련 변수
	public static final String POSTFIX_URL_MEMO_LIST					=	"memolist";
	public static final String POSTFIX_URL_MEMO_WRITE					=	"memo/write";
	public static final String POSTFIX_URL_MEMO_MODIFY					=	"memo/modify";
	public static final String POSTFIX_URL_MEMO_REMOVE					=	"memo/remove";
	
	// 로그아웃 관련 변수
	public static final String POSTFIX_URL_LOGOUT						=	"logout";
	
	
		
}
