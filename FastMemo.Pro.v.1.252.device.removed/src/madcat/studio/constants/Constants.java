package madcat.studio.constants;


public class Constants {
	
	public static final boolean DEVELOPE_MODE								=	true;
	
	// SharedPreference 변수
	public static final String PREF_CONFIG_NAME								=	"FASTMEMO_CONFIG_PREF";
	
	public static final String PREF_CONFIG_APP_FIRST						=	"PREF_APP_FIRST";
	public static final String PREF_CONFIG_APP_COLOR_INDEX					=	"PREF_APP_COLOR_INDEX";
	public static final String PREF_CONFIG_WIDGET_THEME_INDEX				=	"PREF_WIDGET_THEME_INDEX";
	public static final String PREF_CONFIG_LOGIN_AUTH_KEY					=	"PREF_LOGIN_AUTH_KEY";
	public static final String PREF_CONFIG_CLOUD_ONLINE_STATE				=	"PREF_CLOUD_STATE";
	public static final String PREF_CONFIG_IS_CLOUD_UPDATED					=	"PREF_IS_CLOUD_UPDATED";
	
	// PackageName 관련 변수
	public static final String PACKAGE_FREE									=	"madcat.studio.fastmem";
	public static final String PACKAGE_PRO									=	"madcat.studio.fastmemo.pro";
	
	// 데이터 베이스
	public static final String DATABASE_NAME								=	"FastMemoDb.db";
	public static final int DATABASE_VERSION								=	1;
	public static final int DATABASE_FILE_INDEX								=	1;
	public static final String DATABASE_ROOT_DIR							=	"/data/data/madcat.studio.fastmemo.pro/databases/";
	
	// 데이터베이스 테이블 및 컬럼 변수
	public static final String TABLE_MEMO									=	"MEMO";
	
	public static final String MEMO_ID										=	"_id";
	public static final String MEMO_CLOUD_ID								=	"_cloud_id";
	public static final String MEMO_DATE									=	"_date";
	public static final String MEMO_CONTENT									=	"_content";
	
	public static final String TABLE_WIDGET									=	"WIDGET";
	public static final String WIDGET_MEMO_ID								=	"_m_id";
	public static final String WIDGET_ID									=	"_w_id";
	public static final String WIDGET_THEME_IDX								=	"_theme_idx";
	
	// 클라우드 서비스 관련 변수
	public static final int MEMBER_PWD_MAX_LENGTH							=	8;
	
	public static final String DEFAULT_INIT_AUTH_KEY						=	"00000000";
	
	public static final int CLOUD_STATE_OFFLINE								=	0;
	public static final int CLOUD_STATE_ONLINE								=	1;
	
	public static final int CLOUD_DB_MAX_COLUMN_COUNT						=	4;			//	Memo Table 에 속한 Column Count
	
	// Widget Action 관련 변수
	public static final String ACTION_WIDGET_MEMO_UPDATE					=	"action.FastMemo.WIDGET.UPDATE";
	public static final String ACTION_WIDGET_MEMO_REGIST					=	"action.FastMemo.WIDGET.REGIST";
	public static final String ACTION_WIDGET_MEMO_DELETE					=	"action.FastMemo.WIDGET.DELETE"; 
	public static final String ACTION_WIDGET_MEMO_CONTENT_UPDATE			=	"action.FastMemo.WIDGET.CONTENT.UPDATE";
	public static final String ACTION_WIDGET_MEMO_CONTENT_REMOVED_UPDATE	=	"action.FastMemo.WIDGET.CONTENT.REMOVED.UPDATE";
	public static final String ACTION_WIDGET_MEMO_THEME_UPDATE				=	"action.FastMemo.WIDGET.THEME.UPDATE";
	public static final String ACTION_WIDGET_MEMO_CREATE_THEME_UPDATE		=	"action.FastMemo.WIDGET.CREATE.WIDGET.UPDATE";
	
	public static final String ACTION_MEMO_CONTENT_UPDATE					=	"action.FastMemo.CONTENT.UPDATE";
	
	// 검색 관련 변수
	public static final int SEARCH_DATE_MAX_LENGTH							=	8;
	
	// 날짜 포맷 구분자 변수
	public static final String DATE_FORMAT_DELIMETER						=	".";
	
	// Intent 관련 변수
	public static final String PUT_MEMO_ID									=	"PUT_MEMO_ID";
	public static final String PUT_MEMO_DATE								=	"PUT_MEMO_DATE";
	public static final String PUT_MEMO_CONTENT								=	"PUT_MEMO_CONTENT";
	public static final String PUT_WIDGET_ID								=	"PUT_WIDGET_ID";
	public static final String PUT_WIDGET_CONTENT							=	"PUT_WIDGET_CONTENT";
	public static final String PUT_WIDGET_DATE								=	"PUT_WIDGET_DATE";
	public static final String PUT_WIDGET_CTRL								=	"PUT_WIDGET_CTRL";
	public static final String PUT_WIDGET_CALLER_CLASS						=	"PUT_WIDGET_CALLER_CLASS";
	public static final String PUT_WIDGET_THEME_INDEX						=	"PUT_WIDGET_THEME_INDEX";
	
	// 백업 & 복구 관련 변수
	public static final String BACKUP_PATH									=	"/MadCatStudio/FastMemo/Backup";
	public static final String BACKUP_ZIP_PREFIX							=	"FastMemo_";
	public static final String BACKUP_ZIP_POSTFIX							=	".zip";
	
	// 다이얼로그 스케일 관련 변수
	public static final float DIALOG_SIZE									=	0.8f;
	
	// 기타 관련 변수
	public static final int MAX_TEXTVIEW_LINE								=	5;
}











