package madcat.studio.utils;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class HttpClientFactory {
	private static final int TIMEOUT		= 60 * 1000;	//1��
	private static DefaultHttpClient mClient = null;
	
	private HttpClientFactory() { }
	
	public synchronized static DefaultHttpClient getThreadSafeClient() {
		// TODO Auto-generated constructor stub
		
		if (mClient != null) {
			return mClient;
		}
		
		mClient = new DefaultHttpClient();
		
		ClientConnectionManager connManager = mClient.getConnectionManager();
		HttpParams httpParams = mClient.getParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT);
		
		mClient = new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, connManager.getSchemeRegistry()), httpParams);
		
		return mClient;
	}
	
	public synchronized static void initializeClient() {
		if (mClient != null) {
			mClient = null;
		}
	}
}
