package madcat.studio.utils;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.AppWidgetData;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;

import org.bson.types.ObjectId;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.ImageButton;

public class CloudUtils {
	
	/**
	 * 상태를 로그인으로 변경한다.
	 * 
	 * @param context
	 * @param authKey
	 */
	public static void doLogin(Context context, String authKey, ImageButton btnSync) {
		Utils.logPrint(CloudUtils.class, "doLogin 호출");
		
		SharedPreferences configPref = context.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = configPref.edit();

		editor.putInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_ONLINE);
		editor.putString(Constants.PREF_CONFIG_LOGIN_AUTH_KEY, authKey);
		editor.commit();
		
		if(btnSync != null) {
			btnSync.setVisibility(View.VISIBLE);
		}
		
	}
	
	
	/**
	 * 상태를 로그아웃으로 변경한다.
	 * 
	 * @param context
	 */
	public static void doLogout(Context context, ImageButton btnSync) {
		Utils.logPrint(CloudUtils.class, "doLogout 호출");
		
		SharedPreferences configPref = context.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = configPref.edit();

		editor.putInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_OFFLINE);
		editor.putString(Constants.PREF_CONFIG_LOGIN_AUTH_KEY, Constants.DEFAULT_INIT_AUTH_KEY);
		editor.commit();
		
		if(btnSync != null) {
			btnSync.setVisibility(View.GONE);
		}
		
		
	}
	
	/**
	 * 사용자의 CloudService 로그인 데이터 객체를 리턴한다.
	 * 
	 * @param context
	 * @param pwd (필요하지 않을 경우,Null 값 허용)
	 * @return
	 */
	public static Login getLoginData(Context context, String pwd) {
		Utils.logPrint(CloudUtils.class, "getLoginData 호출");
		
		SharedPreferences configPref = context.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		
		String account = Utils.getAccount(context);
		String password = pwd;
		String auth = configPref.getString(Constants.PREF_CONFIG_LOGIN_AUTH_KEY, Constants.DEFAULT_INIT_AUTH_KEY);
		String device = Utils.getDeviceSerialId(context);
		int type = Utils.getTypeOfUse(context);

		Login login = new Login(account, password, auth, type);
		
		return login;
	}
	
	/**
	 * ObjectId 를 생성하여 리턴한다.
	 * 
	 * @return
	 */
	public static String doMakeCloudId() {
		ObjectId objectId = new ObjectId();
		String retObjectId = objectId.get().toString();
		
		Utils.logPrint(CloudUtils.class, "리턴할 ObjectId : " + retObjectId);
		
		return retObjectId;
	}
	
	/**
	 * Server 메모를 Parsing한 MemoArray 와 Local DB 에 저장된 메모를 비교하여 CRUD 한다.
	 * 
	 * @param context
	 * @param jString (Server 로 부터 넘어온 JSON Memo Data)
	 * @return
	 */
	public static boolean doCloudCheckToLocalDB(Context context, String jString) {
		Utils.logPrint(CloudUtils.class, "doCloudCheckToLocalDB 호출");
		
		try {
			// 서버로 부터 넘어온 JSON Memo Data 를 Parsing 하여 ArrayList 로 변환
			ArrayList<Memo> memoArray = JSONUtils.getMemoListToParse(jString);
			
			// Local DB 에 저장된 메모를 읽어온다.
			ArrayList<Memo> dbMemoList = Utils.getMemoList(context);
			
			SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
					null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			ContentValues contentValues = new ContentValues();
			
			ArrayList<Memo> addMemoList = (ArrayList<Memo>) memoArray.clone();			// 추가해야 될 메모를 담기 위한 ArrayList
			ArrayList<Memo> removeMemoList = (ArrayList<Memo>) dbMemoList.clone();		// 삭제해야 될 메모를 담기 위한 ArrayList
			
			int dbMemoSize = dbMemoList.size();
			int cloudMemoSize = memoArray.size();
			
			Utils.logPrint(CloudUtils.class, "ServerMemo(" + cloudMemoSize + "), DbMemo(" + dbMemoSize + ")");
			
			for(int db_i=0; db_i < dbMemoSize; db_i++) {
				for(int cloud_j=0; cloud_j < cloudMemoSize; cloud_j++) {

					String dbCloudId = dbMemoList.get(db_i).getCloudId();
					String serverCloudId = memoArray.get(cloud_j).getCloudId();
					
					int dbIdx = dbMemoList.get(db_i).getMemoId();
					int serverIdx = memoArray.get(cloud_j).getMemoId();
					
					Utils.logPrint(CloudUtils.class, "dbCloudId : " + dbCloudId + ", serverCloudId : " + serverCloudId);
					Utils.logPrint(CloudUtils.class, "dbIdx : " + dbIdx + ", serverIdx : " + serverIdx);
					
					long dbDate = dbMemoList.get(db_i).getDate();
					long serverDate = memoArray.get(cloud_j).getDate();
					
					// LocalDB의 CloudId 가 존재할 경우,
					if(dbCloudId != null) {
						if(dbCloudId.equals(serverCloudId)) {
							
							// TimeStamp 및 메모 값을 변경
							if(dbDate != serverDate) {
								Utils.logPrint(CloudUtils.class, "한번 동기화를 맺은 메모(" + dbCloudId + ") Updated");
								
								contentValues.clear();
								contentValues.put(Constants.MEMO_CONTENT, memoArray.get(cloud_j).getContent());
								contentValues.put(Constants.MEMO_DATE, memoArray.get(cloud_j).getDate());
								sdb.update(Constants.TABLE_MEMO, contentValues, Constants.MEMO_CLOUD_ID + " ='" + serverCloudId + "'", null);
							}
							
							addMemoList.remove(memoArray.get(cloud_j));		//	추가해야 될 메모를 남긴다.
							removeMemoList.remove(dbMemoList.get(db_i));	//	삭제해야 될 메모를 남긴다.
						}
					} 
				}
			}
			
			Utils.logPrint(CloudUtils.class, "AddMemo(" + addMemoList.size() + "), RemoveMemo(" + removeMemoList.size() + ")");
			
			// 로컬 DB 에 존재하는 메모 값을 제외한 서버로 부터 아직 로컬 DB로 추가되지 않은 나머지 메모들을 추가한다.
			int addMemoListSize = addMemoList.size();
			for(int i=0; i < addMemoListSize; i++) {
				contentValues.clear();
				
				contentValues.put(Constants.MEMO_CLOUD_ID, addMemoList.get(i).getCloudId());
				contentValues.put(Constants.MEMO_DATE, addMemoList.get(i).getDate());
				contentValues.put(Constants.MEMO_CONTENT, addMemoList.get(i).getContent());
				
				sdb.insert(Constants.TABLE_MEMO, null, contentValues);
			}
			
			
			// 서버와 동기화 시, 이미 서버에 삭제된 메모가 LocalDB 에 있다면 해당 메모들을 삭제한다.
			// LocalDB 에 있는 메모를 삭제 시, 위젯으로 등록 된 메모가 존재한다면 해당 메모의 값을 '삭제된 메모입니다.' 라고 변경해준다.
			int removeMemoListSize = removeMemoList.size();
			
			for(int i=0; i < removeMemoListSize; i++) {
				contentValues.clear();
				
				int memoId = removeMemoList.get(i).getMemoId();
				String cloudId = removeMemoList.get(i).getCloudId();
			
				Utils.logPrint(CloudUtils.class, "삭제 할 메모를 위젯에 알림");
				ArrayList<AppWidgetData> removedMemoWidget = Utils.getUpdateWidgetContentToMemo(context, memoId);
				for(int j=0; j < removedMemoWidget.size(); j++) {
					
					Intent removedWidgetIntent = new Intent(Constants.ACTION_WIDGET_MEMO_CONTENT_REMOVED_UPDATE);
					removedWidgetIntent.putExtra(Constants.PUT_WIDGET_ID, removedMemoWidget.get(j).getWidgetId());
					removedWidgetIntent.putExtra(Constants.PUT_WIDGET_DATE, removedMemoWidget.get(j).getDate());
					removedWidgetIntent.putExtra(Constants.PUT_WIDGET_CONTENT, context.getString(R.string.widget_title_wrong_removed_memo));
					
					context.sendBroadcast(removedWidgetIntent);
					
				}
				
				Utils.logPrint(CloudUtils.class, "LocalDb에서 메모 삭제");
				sdb.delete(Constants.TABLE_MEMO, Constants.MEMO_CLOUD_ID + " = '" + cloudId + "'", null);
			}
			
			sdb.close();
			
			return true;
			
		} catch(Exception e) {
			Utils.logPrint(CloudUtils.class, "Error : " + e.getMessage());
			
		}
		
		return false;
		
	}

}






















