package madcat.studio.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import madcat.studio.constants.Constants;
import madcat.studio.data.AppWidgetData;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.fastmemo.pro.R;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Utils {

	public static void logPrint(Class cls, String text) {
		if(Constants.DEVELOPE_MODE) {
			Log.d(cls.toString(), text);
		}
	}
	
	/**
	 * 데이터베이스에 저장된 메모 전부를 ArrayList 형태로 가져온다.
	 * 
	 * @param context
	 * @return
	 */
	public static ArrayList<Memo> getMemoList(Context context) {
		Utils.logPrint(Utils.class, "getMemoList 호출");
		
		ArrayList<Memo> items = new ArrayList<Memo>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String memoListSql = "SELECT * FROM " + Constants.TABLE_MEMO + " ORDER BY " + Constants.MEMO_DATE + " DESC;";
		Cursor memoListCursor = sdb.rawQuery(memoListSql, null);
		
		Utils.logPrint(Utils.class, "LocalDB Memo Count : " + memoListCursor.getCount());
		
		while(memoListCursor.moveToNext()) {
			Memo memo = new Memo(memoListCursor.getInt(0), memoListCursor.getString(3), memoListCursor.getLong(1), memoListCursor.getString(2));
			items.add(memo);
		}

		memoListCursor.close();
		sdb.close();
		
		return items;
		
	}
	
	/**
	 * TextView 로 입력받은 Memo Content 의 무결성을 검사한다.
	 * 
	 * @param content
	 * @return
	 */
	public static boolean isTextIntegrity(String content) {
		
		if(content.length() != 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 클라우드 서비스를 이용하지 않는 사용자가 작성한 메모를 오늘 날짜를 포함하여 저장한다.
	 * 
	 * @param content
	 */
	public static void insertMemoToDb(Context context, Memo memo) {
		Utils.logPrint(Utils.class, "insertMemoToDb 호출 cloudId(" + memo.getCloudId() + ")");
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues insertValues = new ContentValues();
		
		insertValues.put(Constants.MEMO_CLOUD_ID, memo.getCloudId());
		insertValues.put(Constants.MEMO_DATE, memo.getDate());
		insertValues.put(Constants.MEMO_CONTENT, memo.getContent());

		sdb.insert(Constants.TABLE_MEMO, null, insertValues);
		
		sdb.close();
	}
	
	/**
	 * 클라우드 서비스를 이용하는 사용자가 작성한 메모를 오늘 날짜를 포함하여 저장한다.
	 * 
	 * @param context
	 * @param cloudId
	 * @param content
	 * @param date
	 */
	public static void insertCloudMemo(Context context, String cloudId, String content, long date) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues insertValues = new ContentValues();
		
		insertValues.put(Constants.MEMO_CLOUD_ID, cloudId);
		insertValues.put(Constants.MEMO_DATE, date);
		insertValues.put(Constants.MEMO_CONTENT, content);

		sdb.insert(Constants.TABLE_MEMO, null, insertValues);
		
		sdb.close();
	}
	
	
	/**
	 * 홈 스크린에 등록된 위젯의 정보를 WidgetData 객체로 만든 뒤, 해당 데이터를 DB의 Widget 테이블에 저장한다.
	 * 
	 * @param context
	 * @param widgetData
	 */
	public static void insertWidgetMemo(Context context, AppWidgetData widgetData) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues insertWidgetValues = new ContentValues();
		
		insertWidgetValues.put(Constants.WIDGET_MEMO_ID, widgetData.getMemoId());
		insertWidgetValues.put(Constants.WIDGET_ID, widgetData.getWidgetId());
		insertWidgetValues.put(Constants.WIDGET_THEME_IDX, widgetData.getThemeIdx());
		
		sdb.insert(Constants.TABLE_WIDGET, null, insertWidgetValues);
		
		sdb.close();
		
	}
	
	/**
	 * 메모 삭제 어댑터를 통해, 메모 목록을 삭제할 경우 호출하는 함수
	 * 
	 * @param context
	 * @param memoArray
	 */
	public static void deleteMemo(Context context, ArrayList<Memo> memoArray) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		int memoArraySize = memoArray.size();
		
		for(int i=0; i < memoArraySize; i++) {
			sdb.delete(Constants.TABLE_MEMO, Constants.MEMO_ID + " = " + memoArray.get(i).getMemoId(), null);
		}
		
		sdb.close();
	}
	
	/**
	 * 위젯으로 등록된 메모 내용이 변경되면, 데이터베이스를 업데이트 한다.
	 * 1. Widget Table 에서 등록된 widget 의 MemoId 를 구해온다. (Sub Query)
	 * 2. Memo Table 에서 MemoId 에 해당하는 메모 내용을 업데이트한다.
	 * 
	 * @param context
	 * @param widgetId
	 * @param content
	 */
	public static void updateWidgetMemoContent(Context context, int widgetId, Memo memo) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.MEMO_CLOUD_ID, memo.getCloudId());
		updateValues.put(Constants.MEMO_CONTENT, memo.getContent());
		updateValues.put(Constants.MEMO_DATE, memo.getDate());
		
		String subMemoIdQuery = "SELECT " + Constants.WIDGET_MEMO_ID + " FROM " + Constants.TABLE_WIDGET + " WHERE " + Constants.WIDGET_ID + " = " + widgetId;
		
		sdb.update(Constants.TABLE_MEMO, updateValues, Constants.MEMO_ID + " = (" + subMemoIdQuery + ")", null);
		
		sdb.close();
		
	}
	
	/**
	 * 앱에서 메모에 등록된 내용을 변경하였다면, 데이터베이스를 업데이트 한다.
	 * 1. Memo Table Update
	 * 2. Widget 으로 등록된 메모가 있다면, 해당 WidgetId를 가져와서 braodcasting 한다.
	 * 
	 * @param context
	 * @param memoId
	 * @param content
	 */
	public static void updateMemoContent(Context context, Memo memo) {
		Utils.logPrint(Utils.class, "updateMemoContent 호출");
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.MEMO_DATE, memo.getDate());
		updateValues.put(Constants.MEMO_CONTENT, memo.getContent());
		
		if(memo.getCloudId() != null) {
			sdb.update(Constants.TABLE_MEMO, updateValues, Constants.MEMO_CLOUD_ID + " = '" + memo.getCloudId() + "'", null);
		} else {
			sdb.update(Constants.TABLE_MEMO, updateValues, Constants.MEMO_ID + " = " + memo.getMemoId(), null);
		}
		
		sdb.close();
	}
	
	
	public static int getWidgetThemeIdx(Context context, int widgetId) {
		int result = 1;
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String themeIdxSql = "SELECT " + Constants.WIDGET_THEME_IDX + " FROM " + Constants.TABLE_WIDGET + " WHERE " + Constants.WIDGET_ID + " = " + widgetId;
		Cursor themeIdxCursor = sdb.rawQuery(themeIdxSql, null);

		if(themeIdxCursor.getCount() > 0) {
			themeIdxCursor.moveToFirst();
			result = themeIdxCursor.getInt(0);
		} 
		
		themeIdxCursor.close();
		sdb.close();
		
		return result; 
	}
	
	/**
	 * widgetId 에 해당하는 메모 내용을 메모 테이블에서 가져온다.
	 * 
	 * @param context
	 * @param widgetId
	 * @return
	 */
	public static Memo getWidgetMemoContent(Context context, int widgetId) {
		Memo result = null;
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String contentSql = "SELECT * " +
							"FROM " + Constants.TABLE_MEMO + " " + 
							"WHERE " + Constants.MEMO_ID + " = " +
							"(SELECT " + Constants.WIDGET_MEMO_ID + " " +
							"FROM " + Constants.TABLE_WIDGET + " " + 
							"WHERE " + Constants.WIDGET_ID + " = " + widgetId + ")";
		
		Cursor contentCursor = sdb.rawQuery(contentSql, null);
		
		if(contentCursor.getCount() > 0) {
			contentCursor.moveToFirst();
			result = new Memo(contentCursor.getInt(0), contentCursor.getString(3), contentCursor.getLong(1), contentCursor.getString(2));
		}
		
		contentCursor.close();
		sdb.close();
		
		return result;
	}
	
	/**
	 * 해당 위젯 아이디에 따라 데이터베이스에 저장된 위젯의 테마 인덱스를 업데이트해준다.
	 * 
	 * @param context
	 * @param widgetId
	 * @param themeIdx
	 */
	public static void updateWidgetThemeIdx(Context context, int widgetId, int themeIdx) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.WIDGET_THEME_IDX, themeIdx);
		
		sdb.update(Constants.TABLE_WIDGET, updateValues, Constants.WIDGET_ID + " = " + widgetId, null);
		
		sdb.close();
	}
	
	/**
	 * 화면에 등록된 위젯이 삭제될 시에, 데이터베이스에 저장된 위젯을 삭제한다.
	 * 
	 * @param context
	 * @param widgetId
	 */
	public static void deleteWidgetMemo(Context context, int widgetId) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		sdb.delete(Constants.TABLE_WIDGET, Constants.WIDGET_ID + " = " + widgetId, null);
		
		sdb.close();
	}
	
	/**
	 * 현재 등록된 위젯 목록을 전부 가져온다.
	 * 
	 * @param context
	 * @return
	 */
	public static ArrayList<AppWidgetData> getWidgetList(Context context) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ArrayList<AppWidgetData> results = new ArrayList<AppWidgetData>();
		
		String widgetListSql = "SELECT M." + Constants.MEMO_ID + ", M." + Constants.MEMO_CLOUD_ID + " " +
				", W." + Constants.WIDGET_ID + ", M." + Constants.MEMO_DATE + 
				", M." + Constants.MEMO_CONTENT + ", W." + Constants.WIDGET_THEME_IDX + " " +
				"FROM " + Constants.TABLE_MEMO + " M, " + Constants.TABLE_WIDGET + " W " +
				"WHERE M." + Constants.MEMO_ID + " == W." + Constants.WIDGET_MEMO_ID;
		Cursor widgetListCursor = sdb.rawQuery(widgetListSql, null);
		
		while(widgetListCursor.moveToNext()) {
			AppWidgetData data = new AppWidgetData(widgetListCursor.getInt(0), widgetListCursor.getString(1), widgetListCursor.getInt(2), 
					widgetListCursor.getLong(3), widgetListCursor.getString(4), widgetListCursor.getInt(5));
			results.add(data);
		}
		
		widgetListCursor.close();
		sdb.close();
		
		return results;
	}
	
	/**
	 * 위젯으로 등록한 메모가 존재하는 지 여부를 리턴합니다.
	 * 1. true - 위젯으로 등록한 메모가 존재, 2. false - 위젯으로 등록한 메모가 존재하지 않음
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isRegistedMemoWidget(Context context) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String regWidgetSql = "SELECT * FROM " + Constants.TABLE_WIDGET;
		Cursor regWidgetCursor = sdb.rawQuery(regWidgetSql, null);
		
		int count = regWidgetCursor.getCount();
		
		regWidgetCursor.close();
		sdb.close();
		
		if(count > 0) {
			return true;
		}
		
		return false;
	}

	/**
	 * 위젯에서 메모 내용을 변경하였다면, 해당 메모를 가리키고 있던 위젯 테이블의 데이터를 가져옵니다.
	 * 
	 * @param context
	 * @param widgetId
	 * @return
	 */
	public static ArrayList<AppWidgetData> getUpdateWidgetContentToWidget(Context context, int widgetId, long widgetDate) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		
		// widgetId 에 해당하는 date 를 업데이트 한다.
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.MEMO_DATE, widgetDate);
		
		sdb.update(Constants.TABLE_MEMO, updateValues, Constants.MEMO_ID + " = " + widgetId, null);
		
		// widgetId 에 해당하는 메모를 가져온다.
		ArrayList<AppWidgetData> results = new ArrayList<AppWidgetData>();
		
		String updateContentSql = "SELECT M." + Constants.MEMO_ID + ", M." + Constants.MEMO_CLOUD_ID + ", " +
				"W." + Constants.WIDGET_ID + ", M." + Constants.MEMO_DATE + ", M." + Constants.MEMO_CONTENT + ", W." + Constants.WIDGET_THEME_IDX + " " +
				"FROM " + Constants.TABLE_MEMO + " M, " + Constants.TABLE_WIDGET + " W " +
				"WHERE M." + Constants.MEMO_ID + " == W." + Constants.WIDGET_MEMO_ID + " " +
				"AND M." + Constants.MEMO_ID + " " +
				"= (SELECT " + Constants.WIDGET_MEMO_ID + " FROM " + Constants.TABLE_WIDGET + " WHERE " + Constants.WIDGET_ID + " =" + widgetId + ")";
		
		Cursor updateContentCursor = sdb.rawQuery(updateContentSql, null);
		
		while(updateContentCursor.moveToNext()) {
			AppWidgetData data = new AppWidgetData(updateContentCursor.getInt(0), updateContentCursor.getString(1), updateContentCursor.getInt(2), 
					updateContentCursor.getLong(3), updateContentCursor.getString(4), updateContentCursor.getInt(5));
			results.add(data);
		}
		
		updateContentCursor.close();
		sdb.close();
		
		
		return results;
	}
	
	/**
	 * memoId 에 해당하는 위젯들을 ArrayList<AppWidgetData> 형태로 리턴합니다.
	 * 
	 * @param context
	 * @param memoId
	 * @return
	 */
	public static ArrayList<AppWidgetData> getUpdateWidgetContentToMemo(Context context, int memoId) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ArrayList<AppWidgetData> results = new ArrayList<AppWidgetData>();
		
		String updateContentSql = "SELECT M." + Constants.MEMO_ID + ", M." + Constants.MEMO_CLOUD_ID + ", " +
					"W." + Constants.WIDGET_ID + ", M." + Constants.MEMO_DATE + ", M." + Constants.MEMO_CONTENT + ", W." + Constants.WIDGET_THEME_IDX + " " +
					"FROM " + Constants.TABLE_MEMO + " M, " + Constants.TABLE_WIDGET + " W " +
					"WHERE M." + Constants.MEMO_ID + " == W." + Constants.WIDGET_MEMO_ID + " " +
					"AND M." + Constants.MEMO_ID + " = " + memoId; 

		Cursor updateContentCursor = sdb.rawQuery(updateContentSql, null);
		
		while(updateContentCursor.moveToNext()) {
			AppWidgetData data = new AppWidgetData(updateContentCursor.getInt(0), updateContentCursor.getString(1), updateContentCursor.getInt(2), 
					updateContentCursor.getLong(3), updateContentCursor.getString(4), updateContentCursor.getInt(5));
			results.add(data);
		}
		
		updateContentCursor.close();
		sdb.close();
		
		
		return results;
	}
	
	/**
	 * 등록된 위젯이 가리키는 메모를 변경합니다.
	 * 
	 * @param context
	 * @param widgetId
	 * @param data
	 */
	public static void updateWidgetMemo(Context context, int widgetId, Memo data) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues updateValues = new ContentValues();
		updateValues.put(Constants.WIDGET_MEMO_ID, data.getMemoId());
		
		sdb.update(Constants.TABLE_WIDGET, updateValues, Constants.WIDGET_ID + " = " + widgetId, null);
		
		sdb.close();
	}
	
	public static ArrayList<Memo> getSearchMemo(Context context, int type, String searchData) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ArrayList<Memo> results = new ArrayList<Memo>();
		
		String searchSql = null;
		
		switch(type) {
			case MemoList.SEARCH_TYPE_WORD:
				searchSql = "SELECT * FROM " + Constants.TABLE_MEMO + " WHERE " + Constants.MEMO_CONTENT + " LIKE '%" + searchData + "%' " +
							"ORDER BY " + Constants.MEMO_DATE + " DESC;";
				break;
				
			case MemoList.SEARCH_TYPE_DATE:
				long todayTimestmap = Utils.getDateToTimestamp(searchData);
				long tomorrowTimestamp = Utils.getTomorrowTimestamp(searchData);
				
				Utils.logPrint(Utils.class, "Today(" + todayTimestmap + "), Tomorrow(" + tomorrowTimestamp + ")");
				
				searchSql = "SELECT * FROM " + Constants.TABLE_MEMO + 
							" WHERE " + Constants.MEMO_DATE + " >= '" + todayTimestmap + "' AND " + Constants.MEMO_DATE + " < '" + tomorrowTimestamp + "' " +
							"ORDER BY " + Constants.MEMO_DATE + " DESC;";
				
				break;
		}
		
		Cursor searchCursor = sdb.rawQuery(searchSql, null);

		while(searchCursor.moveToNext()) {
			Memo memo = new Memo(searchCursor.getInt(0), searchCursor.getString(3), searchCursor.getLong(1), searchCursor.getString(2));
			results.add(memo);
		}
		
		searchCursor.close();
		sdb.close();
		
		return results;
	}
	
	
	/**
	 * 파라미터에 해당하는 메모 리스트 중 위젯으로 등록되어있는지에 대한 여부를 리턴합니다.
	 * 1. true = 위젯으로 등록된 메모가 존재.
	 * 2. false = 위젯으로 등록한 메모가 존재하지 않음.
	 * 
	 * @param memoArray
	 * @return
	 */
	public static boolean isMemoRegistedWidget(Context context, ArrayList<Memo> memoArray) {
		boolean isChecked = false;
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		StringBuilder builder = new StringBuilder();
		
		int memoArraySize = memoArray.size();
		
		builder.append("(");
		for(int i=0; i < memoArraySize; i++) {
			builder.append(memoArray.get(i).getMemoId());

			if((i+1) < memoArraySize) {
				builder.append(", ");
			}
		}
		builder.append(")");
		
		
		String checkSql = "SELECT * FROM " + Constants.TABLE_WIDGET + " WHERE " + Constants.WIDGET_MEMO_ID + " IN " + builder.toString();
		Cursor checkCursor = sdb.rawQuery(checkSql, null);
		
		int checkCount = checkCursor.getCount();
		
		checkCursor.close();
		sdb.close();
		
		if(checkCount > 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 현재 DB 가 클라우드 서비스 전 DB 라면 클라우드 서비스 DB 로 변경한다.
	 * 
	 * @param context
	 */
	public static void convertOldDBToNewDB(Context context) {
		Utils.logPrint(Utils.class, "convertOldDBToNewDB 호출");
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String selectQuery = "SELECT * FROM " + Constants.TABLE_MEMO;
		Cursor selectCursor = sdb.rawQuery(selectQuery, null);
		
		int columnCount = selectCursor.getColumnCount();
		Utils.logPrint(Utils.class, "컬럼 개수 : " + columnCount);
		
		// OldDBColumn = 3, NewDBColumn = 4
		if(selectCursor.getColumnCount() < Constants.CLOUD_DB_MAX_COLUMN_COUNT) {
			Utils.logPrint(Utils.class, "이전 DB 를 CloudDB 로 변경합니다.");
			
			// 클라우드 컬럼 추가
			Utils.logPrint(Utils.class, "CloudDB 에 사용할 Column 추가");
			sdb.execSQL("ALTER TABLE " + Constants.TABLE_MEMO + " ADD COLUMN " + Constants.MEMO_CLOUD_ID + " TEXT DEFAULT NULL");
			
			// 존재하는 메모들의 날짜 변경
			Utils.logPrint(Utils.class, "CloudID 추가, Date 새롭게 변경 및 추가 시작");
			ContentValues updateValues = new ContentValues();
			ArrayList<Memo> memoArray = Utils.getMemoList(context);

			for(int i=0; i < memoArray.size(); i++) {
				updateValues.clear();
				
				String date = String.valueOf(memoArray.get(i).getDate());
				long convertTimestamp = Utils.getDateToTimestamp(date);
				String cloudId = CloudUtils.doMakeCloudId();
				
				Utils.logPrint(Utils.class, "전(" + date + "), 후(" + convertTimestamp + ")");

				updateValues.put(Constants.MEMO_CLOUD_ID, cloudId);
				updateValues.put(Constants.MEMO_DATE, convertTimestamp);
				sdb.update(Constants.TABLE_MEMO, updateValues, Constants.MEMO_ID + " = " + memoArray.get(i).getMemoId(), null);
			}
			
			Utils.logPrint(Utils.class, "CloudID 추가, Date 새롭게 변경 및 추가 완료");
		}
		
		sdb.close();
	}
	
	/**
	 * Widget Table 을 초기화한다.
	 * 
	 * @param context
	 */
	public static void initWidgetTable(Context context) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		sdb.delete(Constants.TABLE_WIDGET, null, null);
		
		sdb.close();
	}
	
	/**
	 * long 타입의 Timestamp 를 String Date 로 변환하여 리턴한다.
	 * 
	 * @param timestamp
	 * @return
	 */
	public static String getTimestampToDate(long timestamp) {
		Date date = new Date(timestamp);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy. MM. dd [HH:mm]");
		
		return sdf.format(date);
	}
	
	/**
	 * 검색에 입력된 String 타입(yyyyMMdd) 의 날짜를 Timestamp 로 변환하여 리턴한다. 
	 * 
	 * @param date
	 * @return
	 */
	public static long getDateToTimestamp(String searchDate) {
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date date = (Date)sdf.parse(searchDate);
			
			Timestamp timestamp = new Timestamp(date.getTime());
			return timestamp.getTime();
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return 0L;
		
	}
	
	/**
	 * 핸드폰 화면의 가로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 가로 크기 dip
	 */
	
	public static int getScreenDipWidthSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipWidth = (int) (displayMetrics.widthPixels / displayMetrics.density);
		
		return dipWidth;
	}

	/**
	 * 핸드폰 화면의 세로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 세로 크기 dip
	 */
	
	public static int getScreenDipHeightSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipHeight = (int) (displayMetrics.heightPixels / displayMetrics.density);
		
		return dipHeight;
	}
	
	/**
	 * dip 를 pixel 로 변환합니다.
	 * 
	 * @param context
	 * @param dip
	 * @return int형의 pixel 값
	 */
	
	public static int pixelFromDip(Context context, int dip) {
		Resources resources = context.getResources();
		int pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, resources.getDisplayMetrics());
		return pixel;
	}
	
	/**
	 * 전달 받은 Action 을 Broadcasting 합니다.
	 * 
	 * @param context
	 * @param action
	 */
	public static void sendBroadCasting(Context context, String action) {
		Intent intent = new Intent(action);
		context.sendBroadcast(intent);
	}
	
	/**
	 * EditText 의 프롬프트를 맨 마지막에 위치시킨다.
	 * 
	 * @param et
	 */
	public static void setLastEditCursor(EditText et) {
		int length = et.getText().length();
		Utils.logPrint(Utils.class, "EditText 길이 : " + length);
		
		et.setSelection(length);
	}
	
	/**
	 * 화면에 키보드를 강제로 표시한다.
	 * 
	 * @param context
	 * @param et
	 */
	public static void showSoftKeyboard(Context context, EditText et) {
		InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.showSoftInput(et, 0);
	}

	/**
	 * 0.1초 정도의 딜레이를 주고 화면에 키보드를 강제로 표시한다.
	 * 
	 * @param context
	 */
	public static void showVirturalKeyboard(final Context context){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
             @Override
             public void run() {
                  InputMethodManager m = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);

                  if(m != null){
                    // m.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    m.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
                  } 
             }

        }, 100);         
    }
	
	/**
	 * 화면에 키보드를 강제로 숨긴다.
	 * 
	 * @param context
	 * @param et
	 */
	public static void hideSoftKeyboard(Context context, EditText et) {
		Utils.logPrint(Utils.class, "hideSoftKeyboard 호출");
		
		InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(et.getWindowToken(), 0);
	}
	
	/**
	 * 화면에 키보드가 떠있는 경우, 키보드를 숨기며 반대로 키보드가 숨겨져 있는 경우 키보드를 띄운다.
	 * 
	 * @param context
	 */
	public static void toggleSoftKeyboard(Context context) {
		Utils.logPrint(Utils.class, "toggleSoftKeyboard 호출");
		
		InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}
	
	
	/**
	 * 미리 설정된 경로에 따라 데이터를 백업한다.
	 * 1. 백업 시에는 먼저 db 경로에서 db 파일을 복사해 온 다음, db 와 같이 압축을 한 뒤, db 파일은 삭제한다.
	 * 
	 * @param context
	 * @return
	 * @throws Exception
	 */
	
	public static boolean dataBackUp(Context context) throws Exception {
		int ZIP_COMPRESS_LEVEL																	=	8;
		
		if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
		
			// 파일명을 날짜로 설정
			Date nowTime = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
			
			// 파일 시스템 컨트롤에 사용할 변수
			String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();	//	SD 카드 경로
			File dbFile = context.getDatabasePath(Constants.DATABASE_NAME);						//	원본 데이터베이스가 저장된 경로
			
			String targetPath = sdCardPath + Constants.BACKUP_PATH;								//	압축 파일을 저장할 경로
			String zipOutPut = Constants.BACKUP_ZIP_PREFIX + dateFormat.format(nowTime) + Constants.BACKUP_ZIP_POSTFIX;		//	압축할 파일명
			String dbOutPut = Constants.DATABASE_NAME;											//	데이터베이스 파일명
	
			// 백업할 경로가 존재하는지를 판단하여, 존재하지 않으면 경로를 생성한다.
			File dir = null;
			dir = new File(targetPath);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			
			// 압축 대상이 될 파일 경로를 설정한다.
			// 데이터베이스 파일을 백업 폴더에 먼저 복사를 한다.
			databaseCopy(dbFile, targetPath, dbOutPut);
				
			// 데이터베이스 복사가 완료되면, 파일 백업 시작한다.
			FileOutputStream fos = null;
			BufferedOutputStream bos = null;
			ZipOutputStream zos = null;
			
			try {
				fos = new FileOutputStream(targetPath + "/" + zipOutPut);					
				bos = new BufferedOutputStream(fos);										
				zos = new ZipOutputStream(bos);
				zos.setLevel(ZIP_COMPRESS_LEVEL);
				zipEntry(dbFile, targetPath, zos);
				zos.finish();
			} finally {
				if(zos != null) {
					zos.close();
				}
				if(bos != null) {
					bos.close();
				}
				if(fos != null) {
					fos.close();
				}
			}
			
			// 백업이 끝나면 복사한 DB 파일은 삭제한다.
			File dbDelFile = new File(targetPath + "/" + dbOutPut);
			dbDelFile.delete();
			
			return true;
		} else {
			Toast.makeText(context, context.getString(R.string.toast_title_sd_card_check), Toast.LENGTH_SHORT).show();
			return false;
		}
	} 
	
	/**
	 * 해당 dbFile 을 targetPath 경로에 output 형태로 복사합니다.
	 * 
	 * @param dbFile
	 * @param targetPath
	 * @param output
	 * @throws IOException
	 */
	
	public static void databaseCopy(File dbFile, String targetPath, String output) throws IOException {
		if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			FileChannel inChannel = new FileInputStream(dbFile).getChannel();
			FileChannel outChannel = new FileOutputStream(targetPath + "/" + output).getChannel();
			
			try {
				int maxCount = (64 * 1024 * 1024) - (32 * 1024);
				long size = inChannel.size();
				long position = 0;
				
				while(position < size) {
					position += inChannel.transferTo(position, maxCount, outChannel);
				}
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				if(inChannel != null) {
					inChannel.close();
				}
				
				if(outChannel != null) {
					outChannel.close();
				}
			}
		}
	}
	
	/**
	 * 압축할 sourceFile 과 압축 경로인 sourcePath 에 맞는 zipEntry List를 생성한 뒤, 설정된 zos 경로에 압축 파일을 생성한다.
	 * 
	 * @param sourceFile
	 * @param sourcePath
	 * @param zos
	 * @throws Exception
	 */
	
	public static void zipEntry(File sourceFile, String sourcePath, ZipOutputStream zos) throws Exception {
		int BUFFER_SIZE = 1024 * 2;
		
		if(sourceFile.isDirectory()) {
			if(sourceFile.getName().equalsIgnoreCase(".metadata")) {
				return;
			}
			
			File[] fileArray = sourceFile.listFiles();
			for(int i=0; i < fileArray.length; i++) {
				zipEntry(fileArray[i], sourcePath, zos);
			}
		} else {
			BufferedInputStream bis = null;
			
			try {
				
				// 1-1. 일반 파일인 경우는 다음과 SD 카드의 경로와 외장 파일이 들어 있는 경로를 파싱하여 압축할 파일 명을 뽑아낸다.
//				String sFilePath = sourceFile.getPath();
//				String zipEntryName = sFilePath.substring(sourcePath.length() + 1, sFilePath.length());
				
				// 1-2. 데이터베이스만을 복사하여 압축할 때는 하단의 처럼 압축할 파일 명을 직접 지정
				String zipEntryName = Constants.DATABASE_NAME;
				
				bis = new BufferedInputStream(new FileInputStream(sourceFile));
				ZipEntry zentry = new ZipEntry(zipEntryName);
				zos.putNextEntry(zentry);
				
				byte[] buffer = new byte[BUFFER_SIZE];
				int cnt = 0;
				while((cnt = bis.read(buffer, 0, BUFFER_SIZE)) != -1) {
					zos.write(buffer, 0, cnt);
				}
				zos.closeEntry();
			} finally {
				if(bis != null) {
					bis.close();
				}
			}
		}
	}
	
	/**
	 * Zip 파일의 압축을 푼다.
	 * 
	 * @param zipFile - 압축 풀 Zip 파일
	 * @param targetDir	- 압축 푼 파일이 들어갈 디렉토리
	 * @param fileNameToLowerCase - 파일명을 소문자로 바꿀지에 대한 여부
	 * @throws Exception
	 */
	
	public static void unZip(File zipFile, File targetDir, boolean fileNameToLowerCase) throws Exception {
		FileInputStream fis = null;
		ZipInputStream zis = null;
		ZipEntry zentry = null;
		
		try {
			fis = new FileInputStream(zipFile);
			zis = new ZipInputStream(fis);
			
			while((zentry = zis.getNextEntry()) != null) {
				String fileNameToUnZip = zentry.getName();
				if(fileNameToLowerCase) {
					fileNameToUnZip = fileNameToUnZip.toLowerCase();
				}
				
				File targetFile = new File(targetDir, fileNameToUnZip);
				
				if(zentry.isDirectory()) {		//	디렉토리인 경우
					targetFile.mkdirs();		//	디렉토리를 생성한다.
				} else {						//	파일인 경우
					unZipEntry(zis, targetFile);
				}
			}
		} finally {
			if(zis != null) {
				zis.close();
			}
			
			if(fis != null) {
				fis.close();
			}
		}
	}
	
	public static File unZipEntry(ZipInputStream zis, File targetFile) throws Exception {
		int BUFFER_SIZE									=	1024 * 2;
		
		FileOutputStream fos = null;
		
		try {
			fos = new FileOutputStream(targetFile);
			
			byte[] buffer = new byte[BUFFER_SIZE];
			int len = 0;
			
			while((len = zis.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
			}
		} finally {
			if(fos != null) {
				fos.close();
			}
		}
		
		return targetFile;
	}
	
	/**
	 * 해당 List 안에 AllThatIdea 파일명이 있는 지를 체크하여 필터링 합니다.
	 * 
	 * @param list
	 * @return
	 */
	
	public static ArrayList<String> filterFileName(ArrayList<String> list) {
		ArrayList<String> fileFilterArray = new ArrayList<String>();
		
		for(int i=0; i < list.size(); i++) {
			String[] splitFileName = list.get(i).split("_");
			
			if(splitFileName.length == 7 && splitFileName[0].equals("FastMemo")) {
				fileFilterArray.add(list.get(i));
			}
		}
		
		return fileFilterArray;
	}
	
	public static String[] splitFileName(Context context, ArrayList<String> list) {
		String[] splitFileArray = new String[list.size()];
		
		for(int i=0; i < list.size(); i++) {
			String[] splitFileName = list.get(i).split("_");
			
			splitFileArray[i] = splitFileName[1] + "." + splitFileName[2] + "." + splitFileName[3] + " " +
								splitFileName[4] + context.getString(R.string.tv_title_backup_suffix_date_hour) + 
								splitFileName[5] + context.getString(R.string.tv_title_backup_suffix_date_min) + 
								context.getString(R.string.tv_title_backup_suffix_data);
		}
		
		return splitFileArray;
	}
	
	/**
	 * sourceFileName으로 넘어온 파일명에 읽어들여 정해진 sourcePath 에 복구합니다.
	 * 
	 * @param context
	 * @param sourceFileName - 복구할 파일명
	 * @return
	 */
	public static boolean dataRestore(Context context, String sourceFileName) {
		String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		String sourcePath = sdCardPath + Constants.BACKUP_PATH + "/" + sourceFileName;
		
		File sourceFile = new File(sourcePath);
		File dbPath = context.getDatabasePath(Constants.DATABASE_NAME);
		File targetPath = new File(dbPath.getParent());
		
		try {
			Utils.unZip(sourceFile, targetPath, false);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * SD 카드가 핸드폰에 삽입 되어있는지에 대한 여부를 리턴합니다.
	 * 1. true - sd 카드 꽂혀있음
	 * 2. false - sd 카드 꽂혀있지 않음
	 * 
	 * @return
	 */
	public static boolean isSDCardMounted() {
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	}
	
	/**
	 * sp 를 px 로 변환하여 반환합니다.
	 * 
	 * @param context
	 * @param sp
	 * @return
	 */
	public static int getSpToPx(Context context, float sp) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		
		float fPixels = metrics.density * sp;
		int pixels = (int)(metrics.density * sp + 0.5f);
		
		Utils.logPrint(Utils.class, "getSpToPx value(" + sp + " -> " + pixels + ")");
		
		return pixels;
	}
	
	/**
	 * 화면 가로 사이즈를 기준으로 텍스트의 라인이 몇줄인지를 반환합니다.
	 * 
	 * @param context
	 * @param text
	 * @param widthDp
	 * @return
	 */
	public static int getTextViewLineBreak(Context context, String text, int widthDp) {
		
		Paint paint = new Paint();
		ArrayList<String> cutStr = new ArrayList<String>();
		
		paint.setTextSize(18);
		paint.setTypeface(Typeface.DEFAULT);
		
		int end = 0;
		
		do {
			end = paint.breakText(text, true, widthDp, null);
			
			if(end > 0) {
				cutStr.add(text.substring(0, end));
				text = text.substring(end);
				
			}
		} while(end > 0);
		
		Utils.logPrint(Utils.class, "총 위젯 텍스트뷰 라인 : " + cutStr.size());
		
		for(int i=0; i < cutStr.size(); i++) {
			Utils.logPrint(Utils.class, "cut text(" + i + ") : " + cutStr.get(i));
		}

		return cutStr.size();
	}
	
	public static String getWidgetModifiedContent(Context context, String text, int widthDp, int maxLines) {
		Utils.logPrint(Utils.class, "getWidgetModifiedContent 호출");
		
		StringBuilder builder = new StringBuilder();
		
		// 실제 구문
		ArrayList<String> results = new ArrayList<String>();
		
		Paint paint = new Paint();
		
		paint.setTextSize(18);
		paint.setTypeface(Typeface.DEFAULT);

		int end = 0;
		int startIdx = 0;
		
		do {
			startIdx = 0;
		
			end = paint.breakText(text, true, widthDp, null);
			Utils.logPrint(Utils.class, "end 값 : " + end);
			
			if(text.length() <= end) {
				results.add(text);
				break;
			}
			
			String cutStr = text.substring(0, end);
			Utils.logPrint(Utils.class, "이번에 수정할 텍스트 : " + cutStr);
			
			char[] charStr = cutStr.toCharArray();
			
			for(int i=0; i < charStr.length; i++) {
				if(charStr[i] == '\n') {
					String cutLineStr = cutStr.substring(startIdx, i);
					results.add(cutLineStr);
					Utils.logPrint(Utils.class, "cutLineStr : " + cutLineStr);
					startIdx = i+1;
				}
				
				// 마지막 글자에 도달하면, 개행문자 전까지의 텍스트를 다음 줄로 넘긴다.
				if((i+1) == charStr.length) {
					if(startIdx == 0) {
						results.add(text.substring(0, end));
						text = text.substring(end);
					} else {
						text = text.substring(startIdx);
					}
					Utils.logPrint(Utils.class, "다음 텍스트\n" + text);
				}
			}
			
		} while(end > 0);
		
		if(results.size() > maxLines) {
			String sampleText = "가나다라마바사아자차카파타하가나다라마바사아자차카파타하";
			String ellipText = results.get(maxLines-1);
			
			int checkedEnd = paint.breakText(sampleText, true, widthDp, null);
			
			if(ellipText.length() + 3 < checkedEnd) {
				ellipText = ellipText + "...";
			} else {
				
				char[] ellipChar = ellipText.toCharArray();
				for(int i=ellipChar.length-1; i > ellipChar.length-4; --i) {
					ellipChar[i] = '.';
				}
				
				for(int i=0; i < ellipChar.length; i++) {
					builder.append(ellipChar[i]);
				}
				
				ellipText = builder.toString();
			}
			
			
			Utils.logPrint(Utils.class, "말줄임표 텍스트 : " + ellipText);
			results.set(maxLines-1, ellipText);
		}
		
		
		builder.setLength(0);
		for(int i=0; i < results.size(); i++) {
			if((i+1) == results.size()) {
				builder.append(results.get(i));
				break;
			}
			
			if(i < maxLines) {
				builder.append(results.get(i));
				builder.append("\n");
			} else {
				builder.append(results.get(i));
				break;
			}
		}
		
		
		return builder.toString();
	}
	
	/**
	 * 스마트폰에 등록된 계정 목록을 리턴한다.
	 * 
	 * @param context
	 * @return
	 */
	public static Account[] getAccountList(Context context) {
		
		Account[] accounts = AccountManager.get(context).getAccounts();
		
		if(accounts == null) {
			return null;
		}
		
		if(accounts.length > 0) {
			return accounts;
		} else {
			return null;
		}
	}
	
	/**
	 * 스마트폰에 등록된 가장 첫번째 계정을 가져온다.
	 * 
	 * @param context
	 * @return
	 */
	public static String getAccount(Context context) {
		
		Account[] accountList = Utils.getAccountList(context);
		
		if(accountList != null) {
			return accountList[0].name;
		}
		
		return null;
	}
	
	/**
	 * 파라메터로 넘어온 해당 레이아웃의 백그라운드 색상을 index 파라메터에 맞춰 바꿔준다.
	 * 
	 * @param liBar
	 * @param colorIdx
	 */
	public static void setToolBarColor(LinearLayout liBar, int colorIdx) {
		if(liBar != null) {
	    	switch(colorIdx) {
		    	case MemoList.MAIN_COLOR_EMERALD:
		    		liBar.setBackgroundResource(R.color.main_color_emerald);
		    		break;
		    	case MemoList.MAIN_COLOR_PURPLE:
		    		liBar.setBackgroundResource(R.color.main_color_purple);
		    		break;
		    	case MemoList.MAIN_COLOR_PINK:
		    		liBar.setBackgroundResource(R.color.main_color_pink);
		    		break;
		    	case MemoList.MAIN_COLOR_BLUE:
		    		liBar.setBackgroundResource(R.color.main_color_blue);
		    		break;
	    	}
    	}
	}
	
	/**
	 * 디바이스의 고유 시리얼 값을 가져오는 함수
	 * 
	 * @param context
	 * @return
	 */
	public static String getDeviceSerialId(Context context) {
		
		String serialNum = null;
		 
		try {
		    Class<?> c = Class.forName("android.os.SystemProperties");
		    Method get = c.getMethod("get", String.class);
		    serialNum = (String) get.invoke(c, "ro.serialno");
		    
		    return serialNum;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 사용자 암호를 SHA 256 bit 로 해쉬 변환하는 함수
	 * 
	 * @param pwd
	 * @return
	 */
	public static String convertPwdToSHA256(String pwd) {
		String resultSHA = null;
		
		try{
			MessageDigest mDigest = MessageDigest.getInstance("SHA-256"); 
			mDigest.update(pwd.getBytes()); 
			byte byteData[] = mDigest.digest();
			StringBuffer sb = new StringBuffer(); 
			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			resultSHA = sb.toString();
			
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace(); 
			resultSHA = null; 
		}
		
		return resultSHA;
		
	}
	
	/**
	 * App 의 패키지 명을 파악하여 Pro, Lite 여부를 결정하여 반환하는 함수
	 * 
	 * @param context
	 * @return
	 */
	public static int getTypeOfUse(Context context) {
		
		String packageName = context.getPackageName();
		
		if(packageName.equals(Constants.PACKAGE_PRO)) {
			return Login.TYPE_PRO;
		}

		return Login.TYPE_FREE;
	}
	
	/**
	 * 현재 이용 가능한 네트워크 망을 확인 한 뒤, 이용 가능 여부를 Boolean 타입으로 리턴한다.
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		boolean isNetworkAvailable = false;

		if(manager.getActiveNetworkInfo() != null) {
			NetworkInfo info = manager.getActiveNetworkInfo();
			
			switch(info.getType()) {
				case ConnectivityManager.TYPE_WIMAX:
					Utils.logPrint(Utils.class, "4G 이용 가능");
					
					isNetworkAvailable = true;
					break;
				case ConnectivityManager.TYPE_MOBILE:
					Utils.logPrint(Utils.class, "3G 이용 가능");
					
					isNetworkAvailable = true;
					break;
				case ConnectivityManager.TYPE_WIFI:
					Utils.logPrint(Utils.class, "WiFi 이용 가능");
					
					isNetworkAvailable = true;
					break;
			}
		} else {
			isNetworkAvailable = false;
			
			Toast.makeText(context, context.getString(R.string.toast_title_check_network_state), Toast.LENGTH_SHORT).show();
		}
		
		return isNetworkAvailable;
	}
	
	/**
	 * 현재 시간에 해당하는 TimeStamp 를 리턴한다.
	 * 
	 * @return
	 */
	public static long getCurrentTimeStamp() {
		long currentTime = System.currentTimeMillis();
		Timestamp timeStamp = new Timestamp(currentTime);
		
		return timeStamp.getTime();
	}
	
	/**
	 * Cloud Service 를 위해 Database 를 업데이트한다.
	 * 
	 * @param context
	 * @return
	 */
	public static boolean doUpdateCloudDB(Context context) {
		Utils.logPrint(Utils.class, "doUpdateCloudDB 호출");
		
		try {
			// DB 를 새로운 버전의 DB 로 업데이트 한다.
			convertOldDBToNewDB(context);
			
			// 클라우드 업데이트 완료로 변경
			SharedPreferences.Editor editor = context.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE).edit();
			editor.putBoolean(Constants.PREF_CONFIG_IS_CLOUD_UPDATED, true);
			editor.commit();
			
			return true;
			
		} catch(Exception e) {
			Utils.logPrint(Utils.class, "Error : " + e.getMessage());
			
		}
		
		return false;
		
	}

	/**
	 * LocalDB 의 Memo 테이블의 마지막 메모 시퀀스를 리턴한다.
	 * 
	 * @param context
	 * @return
	 */
	public static int getLastSequenceFromMemoDB(Context context) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String seqQuery = "SELECT * FROM SQLITE_SEQUENCE WHERE NAME = '" + Constants.TABLE_MEMO + "';";
		Cursor seqCursor = sdb.rawQuery(seqQuery, null);
		
		int lastSeq = 0;
		if(seqCursor.getCount() > 0) {
			seqCursor.moveToFirst();
			lastSeq = seqCursor.getInt(1);
		}
		
		sdb.close();
		
		Utils.logPrint(Utils.class, "현재 메모 테이블의 마지막 메모 시퀀스  : " + lastSeq);
		
		return lastSeq;
	}
	
	/**
	 * 인증키가 유효하지 않을 경우, Toast를 띄워준 뒤, 로그아웃을 한다.
	 * 
	 * @param context
	 * @param btnSync
	 */
	public static void doInvalidateAuthLogout(Context context, ImageButton btnSync) {
		Toast.makeText(context,	context.getString(R.string.toast_title_login_invalidate_auth), Toast.LENGTH_SHORT).show();
		
    	// 로그 아웃을 한다.
		CloudUtils.doLogout(context, btnSync);
	}
	
	/**
	 * 오늘 날짜를 바탕으로 내일 날짜의 타임 스탬프를 구해 리턴한다.
	 * 
	 * @param todayDate (yyyyMMdd)
	 * @return
	 */
	public static long getTomorrowTimestamp(String todayDate) {
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date date = sdf.parse(todayDate);
			
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, 1);
			
			Date tomorrowDate = calendar.getTime();
			Timestamp timestamp = new Timestamp(tomorrowDate.getTime());
			
			return timestamp.getTime();
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return 0L;
	}
	
	/**
	 * SD-Card 에 저장된 백업 파일 목록을 불러와 ArrayList 로 리턴한다.
	 * 
	 * @param context
	 * @return
	 */
	public static ArrayList<String> getBackupFileList(final Context context) {
		
		if(Utils.isSDCardMounted()) {
			
			// 파일이 존재하는지 확인
			String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
			String targetPath = sdCardPath + Constants.BACKUP_PATH;
			
			File fileList = new File(targetPath);
			
			if(fileList.listFiles() == null) {
				Toast.makeText(context, context.getString(R.string.toast_title_restore_file_not_exist), Toast.LENGTH_SHORT).show();
			} else {
				ArrayList<String> fileName = new ArrayList<String>();
				
				for(File file : fileList.listFiles()) {
					fileName.add(file.getName());
				}
				
				// 조건에 따라, 정확한 복원 파일을 필터링 해서 저장한다.
				ArrayList<String> filterFileName = Utils.filterFileName(fileName);
				
				return filterFileName;
			}
			
		} else {
			Toast.makeText(context, context.getString(R.string.toast_title_sd_card_check), Toast.LENGTH_SHORT).show();
		}
		
		return null;
		
	}
	
}















