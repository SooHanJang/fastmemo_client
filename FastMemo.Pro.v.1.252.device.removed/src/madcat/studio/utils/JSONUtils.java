package madcat.studio.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import madcat.studio.constants.Constants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Memo;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtils {

	/**
	 * Server 로 부터 응답되어 넘어온 Response 를 status, data 로 Parsing 하여 리턴한다.
	 * 
	 * @param jString
	 * @return
	 * @throws JSONException
	 */
	public static CloudStatus getStatusToParse(String jString) throws JSONException {
		
		JSONObject order = new JSONObject(jString);
		
		CloudStatus cloudStatus = null;
		int status = 0;
		String data = null;
		
		if(!order.isNull(CloudStatus.NODE_STATUS)) {
			status = order.getInt(CloudStatus.NODE_STATUS);
		}
		
		if(!order.isNull(CloudStatus.NODE_DATA)) {
			data = order.getString(CloudStatus.NODE_DATA);
		}
		
		cloudStatus = new CloudStatus(status, data);
		
		return cloudStatus;
	}
	
	/**
	 * Server 로 부터 응답되어 넘어온 Response 를 Memo ArrayList 로 Parsing 하여 리턴한다.
	 * 
	 * @param jString
	 * @return
	 * @throws JSONException
	 */
	public static ArrayList<Memo> getMemoListToParse(String jString) {
		
		ArrayList<Memo> memoArray = new ArrayList<Memo>();
		
		try {
			JSONArray jsonArray = new JSONArray(jString);
			
			int jsonArrayLength = jsonArray.length();
			for(int i=0; i < jsonArrayLength; i++) {
				JSONObject order = jsonArray.getJSONObject(i);
				Utils.logPrint(JSONUtils.class, "order(" + i + ") : " + order.toString());
				
				// Memo 객체 파싱
				String cloudId = null, memoContent = null;
				long date = 0l;
				
				if(!order.isNull(Memo.NODE_CLOUD_ID)) {
					cloudId = order.getString(Memo.NODE_CLOUD_ID);
					Utils.logPrint(JSONUtils.class, "MemoCloudId : " + cloudId);
				}
				
				if(!order.isNull(Memo.NODE_DATE)) {
					date = order.getLong(Memo.NODE_DATE);
					Utils.logPrint(JSONUtils.class, "MemoDate : " + date);
				}
				
				if(!order.isNull(Memo.NODE_MEMO)) {
					memoContent = order.getString(Memo.NODE_MEMO);
					Utils.logPrint(JSONUtils.class, "MemoContent : " + memoContent);
				}
				
				Memo memo = new Memo(Memo.DEFAULT_MEMO_ID, cloudId, date, memoContent);
				memoArray.add(memo);
			}
			
			return memoArray;
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Memo 객체를 JSON 으로 변환하여 JSONObject 로 리턴하는 함수
	 * 
	 * @param memo
	 * @return
	 * @throws JSONException
	 */
	public static JSONObject convertMemoToJSON(Memo memo) throws JSONException {
		JSONObject jsonMemo = new JSONObject();
		
		jsonMemo.put(Memo.NODE_CLOUD_ID, memo.getCloudId());
		jsonMemo.put(Memo.NODE_DATE, memo.getDate());
		jsonMemo.put(Memo.NODE_MEMO, memo.getContent());
		
		return jsonMemo;
	}
	
	/**
	 * Memo ArrayList 객체를 JSON 으로 변환하여 JSONArray 로 리턴하는 함수
	 * 
	 * @param memoArray
	 * @return
	 * @throws JSONException
	 */
	public static JSONArray convertMemoArrayToJSON(ArrayList<Memo> memoArray) throws JSONException {
		
		JSONArray jsonMemoArray = new JSONArray();

		int memoSize = memoArray.size();
		for(int i=0; i < memoSize; i++) {
			jsonMemoArray.put(JSONUtils.convertMemoToJSON(memoArray.get(i)));
		}
		
		return jsonMemoArray;
	}
	
	/**
	 * 삭제할 MemoArrayList 객체에서 CloudId 만을 JSONArray 로 변환하여 리턴하는 함수
	 * 
	 * @param memoArray
	 * @return
	 * @throws JSONException
	 */
	public static JSONArray convertRemoveMemoIdArrayToJSON(ArrayList<Memo> memoArray) throws JSONException {
		
		JSONArray jsonRemoveMemoIdArray = new JSONArray();
		int memoRemoveSize = memoArray.size();
		for(int i=0; i < memoRemoveSize; i++) {
			jsonRemoveMemoIdArray.put(JSONUtils.convertMemoToJSON(memoArray.get(i)).getString(Memo.NODE_CLOUD_ID));
		}
		
		return jsonRemoveMemoIdArray;
		
	}
	
}





















