package madcat.studio.widget;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.control.CtrlModifyMemoWidget;
import madcat.studio.control.CtrlOtherMemoWidget;
import madcat.studio.control.CtrlThemeWidget;
import madcat.studio.data.AppWidgetData;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.utils.Utils;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

public class RegistMemoWidget extends AppWidgetProvider {

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		
		String action = intent.getAction();
		
		Utils.logPrint(getClass(), "onReceive 호출, Receive Action : " + intent.getAction());
		
		// 위젯 업데이트 호출 되었을 경우,
		if(action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			
			ArrayList<AppWidgetData> results = Utils.getWidgetList(context);
			
			for(int i=0; i < results.size(); i++) {
				initWidgetState(context, appWidgetManager, results.get(i));
			}
			
			
		} 
		// 메모 등록이 호출 되었을 경우
		else if(action.equals(Constants.ACTION_WIDGET_MEMO_REGIST)) {
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

			int widgetId = intent.getExtras().getInt(Constants.PUT_WIDGET_ID);
			String content = intent.getExtras().getString(Constants.PUT_WIDGET_CONTENT);
			long date = intent.getExtras().getLong(Constants.PUT_WIDGET_DATE);
			
			Utils.logPrint(getClass(), "memo regist(id : " + widgetId + ", content : " + content + ")");
			
			registMemo(context, appWidgetManager, widgetId, content, date);

		}
		// 메모 업데이트가 호출 되었을 경우
		else if(action.equals(Constants.ACTION_WIDGET_MEMO_UPDATE)) {

			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			
			int widgetId = intent.getExtras().getInt(Constants.PUT_WIDGET_ID);
			long date = intent.getExtras().getLong(Constants.PUT_WIDGET_DATE);
			String content = intent.getExtras().getString(Constants.PUT_WIDGET_CONTENT);
			
			updateWidgetMemo(context, appWidgetManager, widgetId, content, date);
			
			
		} 
		// 메모 내용이 업데이트 되었을 경우
		else if(action.equals(Constants.ACTION_WIDGET_MEMO_CONTENT_UPDATE)) {
			Utils.logPrint(getClass(), "위젯 메모 내용을 업데이트 호출");
			
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

			int widgetId = intent.getExtras().getInt(Constants.PUT_WIDGET_ID);
			long widgetDate = intent.getExtras().getLong(Constants.PUT_WIDGET_DATE);
			
			ArrayList<AppWidgetData> results = Utils.getUpdateWidgetContentToWidget(context, widgetId, widgetDate);
			for(int i=0; i < results.size(); i++) {
				updateWidgetContent(context, appWidgetManager, results.get(i));
			}
			
		}
		// 메모 내용을 강제로 업데이트 할 경우,
		else if(action.equals(Constants.ACTION_WIDGET_MEMO_CONTENT_REMOVED_UPDATE)) {
			Utils.logPrint(getClass(), "위젯 메모 내용을 강제로 업데이트 합니다.");
			
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

			int widgetId = intent.getExtras().getInt(Constants.PUT_WIDGET_ID);
			long widgetDate = intent.getExtras().getLong(Constants.PUT_WIDGET_DATE);
			String widgetContent = intent.getExtras().getString(Constants.PUT_WIDGET_CONTENT);
			
			ArrayList<AppWidgetData> widgetData = Utils.getUpdateWidgetContentToWidget(context, widgetId, widgetDate);
			for(int i=0; i < widgetData.size(); i++) {
				widgetData.get(i).setWidgetContent(widgetContent);
				updateWidgetContent(context, appWidgetManager, widgetData.get(i));
			}
			
		}
		
		// 위젯 테마가 변경 되었을 경우
		else if(action.equals(Constants.ACTION_WIDGET_MEMO_THEME_UPDATE)) {
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			
			int widgetId = intent.getExtras().getInt(Constants.PUT_WIDGET_ID);
			int themeIdx = intent.getExtras().getInt(Constants.PUT_WIDGET_THEME_INDEX);
			
			Utils.logPrint(getClass(), "theme update(id : " + widgetId + ", themeIdx : " + themeIdx);
			
			updateWidgetTheme(context, appWidgetManager, widgetId, themeIdx);
		} 
		// 다른 메모로 변경하고자 하는 경우,
		else if(action.equals(Constants.ACTION_MEMO_CONTENT_UPDATE)) {
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			
			int memoId = intent.getExtras().getInt(Constants.PUT_MEMO_ID);
			
			Utils.logPrint(getClass(), "전달받은 메모 아이디 : " + memoId);
			
			ArrayList<AppWidgetData> results = Utils.getUpdateWidgetContentToMemo(context, memoId);
			
			for(int i=0; i < results.size(); i++) {
				updateWidgetContent(context, appWidgetManager, results.get(i));
			}
		}
	}
	
	/**
	 * 재부팅시 등록된 위젯의 정보를 읽어와서 메모 내용과 테마 인덱스에 맞춰 위젯을 재설정합니다.
	 * 
	 * @param context
	 * @param appWidgetManager
	 * @param data
	 */
	private void initWidgetState(Context context, AppWidgetManager appWidgetManager, AppWidgetData data) {
		registMemo(context, appWidgetManager, data.getWidgetId(), data.getContent(), data.getDate());
		
		RemoteViews updateView = new RemoteViews(context.getPackageName(), R.layout.widget_regist_memo);
		
		int bgHeadResId = context.getResources().getIdentifier("icon_widget_post_head_" + data.getThemeIdx(), "drawable", context.getPackageName());
		int bgBodyResId = context.getResources().getIdentifier("icon_widget_post_body_" + data.getThemeIdx(), "drawable", context.getPackageName());
		
		updateView.setInt(R.id.widget_post_head, "setBackgroundResource", bgHeadResId);
		updateView.setInt(R.id.widget_post_body, "setBackgroundResource", bgBodyResId);
		updateView.setInt(R.id.widget_post_date_bar, "setBackgroundResource", getDateBarColorId(context, data.getThemeIdx()));
		
		appWidgetManager.updateAppWidget(data.getWidgetId(), updateView);
	}

	private static int getDateBarColorId(Context context, int themeIdx) {
		int colorId = 0;
		
		switch(themeIdx) {
			case 1:
				colorId = R.color.main_color_emerald;
				break;
			case 2:
				colorId = R.color.main_color_purple;
				break;
			case 3:
				colorId = R.color.main_color_pink;
				break;
			case 4:
				colorId = R.color.main_color_blue;
				break;
		}
		
		Utils.logPrint(RegistMemoWidget.class, "리턴할 컬러 아이디 : " + colorId);
		
		return colorId;
	}
	
	/**
	 * 파라미터에 등록된 데이터를 바탕으로 위젯을 홈스크린에 등록합니다.
	 * 
	 * @param context
	 * @param appWidgetManager
	 * @param appWidgetId
	 * @param content
	 * @param date
	 */
	public static void registMemo(Context context, AppWidgetManager appWidgetManager, int appWidgetId, String content, long date) {
		Utils.logPrint(Utils.class, "registMemo 호출, appWidgetId : " + appWidgetId);
		
		RemoteViews updateView = new RemoteViews(context.getPackageName(), R.layout.widget_regist_memo);

		updateView.setTextViewText(R.id.widget_post_content, Utils.getWidgetModifiedContent(context, content, 208, Constants.MAX_TEXTVIEW_LINE));
		updateView.setTextViewText(R.id.widget_post_date, Utils.getTimestampToDate(date));
		
		Uri appWidgetUri = Uri.parse("" + appWidgetId);

		// 테마 변경
		Intent themeIntent = new Intent(context, CtrlThemeWidget.class);
		themeIntent.setData(appWidgetUri);
		
		updateView.setOnClickPendingIntent(R.id.widget_post_change_theme, PendingIntent.getActivity(context, 0, themeIntent, 0));
		
		// 다른 메모 선택
		Intent otherMemoIntent = new Intent(context, CtrlOtherMemoWidget.class);
		otherMemoIntent.setData(appWidgetUri);
		
		updateView.setOnClickPendingIntent(R.id.widget_post_other_memo, PendingIntent.getActivity(context, 0, otherMemoIntent, 0));
		
		// 메모 수정 호출
		Intent modifyMemoIntent = new Intent(context, CtrlModifyMemoWidget.class);
		modifyMemoIntent.setData(appWidgetUri);
		
		updateView.setOnClickPendingIntent(R.id.widget_post_content, PendingIntent.getActivity(context, 0, modifyMemoIntent, 0));
		
//		// App 실행 호출
//		Intent executeMemoIntent = new Intent(context, MemoList.class);
//		executeMemoIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//		updateView.setOnClickPendingIntent(R.id.widget_post_execute_memo, PendingIntent.getActivity(context, 0, executeMemoIntent, 0));
		
		
		// 설정 값 Update
		appWidgetManager.updateAppWidget(appWidgetId, updateView);
		
		
	}

	/**
	 * 사용자가 위젯을 삭제하면, DB 에 저장된 위젯 데이터를 같이 삭제합니다.
	 * 
	 * @param context
	 * @param appWidgetId
	 */
	private void deleteMemo(Context context, int appWidgetId) {
		Utils.logPrint(getClass(), "deleteMemo 호출");
		
		Utils.deleteWidgetMemo(context, appWidgetId);
		
	}
	
	/**
	 * 파라미터에 따라, 위젯의 메모 값을 변경합니다.
	 * 
	 * @param context
	 * @param appWidgetManager
	 * @param appId
	 * @param content
	 * @param date
	 */
	private void updateWidgetMemo(Context context, AppWidgetManager appWidgetManager, int appId, String content, long date) {
		RemoteViews updateView = new RemoteViews(context.getPackageName(), R.layout.widget_regist_memo);
		
		updateView.setTextViewText(R.id.widget_post_content, Utils.getWidgetModifiedContent(context, content, 208, Constants.MAX_TEXTVIEW_LINE));
		updateView.setTextViewText(R.id.widget_post_date, Utils.getTimestampToDate(date));
		
		appWidgetManager.updateAppWidget(appId, updateView);
	}
	
	/**
	 * 파라미터를 바탕으로 테마 변경을 클릭하면, 테마를 변경한 뒤 데이터베이스를 업데이트합니다.
	 * 
	 * @param context
	 * @param appWidgetManager
	 * @param appWidgetId
	 * @param themeIdx
	 */
	public static void updateWidgetTheme(Context context, AppWidgetManager appWidgetManager, int appWidgetId, int themeIdx) {
		Utils.logPrint(Utils.class, "updateWidgetTheme (appWidgetId : " + appWidgetId + ", themeIdx : " + themeIdx + ")");
		
		RemoteViews updateView = new RemoteViews(context.getPackageName(), R.layout.widget_regist_memo);
		
		// 테마 변경 설정
		int currThemeIdx = themeIdx;
		
		switch(currThemeIdx) {
			case 1:
				currThemeIdx = 2;
				break;
			case 2:
				currThemeIdx = 3;
				break;
			case 3:
				currThemeIdx = 4;
				break;
			case 4:
				currThemeIdx = 1;
				break;
		}
		
		Utils.logPrint(Utils.class, "변경전 테마 인덱스 : " + themeIdx + ", 변경 후 테마 인덱스 : " + currThemeIdx);
		
		int bgHeadResId = context.getResources().getIdentifier("icon_widget_post_head_" + currThemeIdx, "drawable", context.getPackageName());
		int bgBodyResId = context.getResources().getIdentifier("icon_widget_post_body_" + currThemeIdx, "drawable", context.getPackageName());
		
		updateView.setInt(R.id.widget_post_head, "setBackgroundResource", bgHeadResId);
		updateView.setInt(R.id.widget_post_body, "setBackgroundResource", bgBodyResId);
		updateView.setInt(R.id.widget_post_date_bar, "setBackgroundResource", getDateBarColorId(context, currThemeIdx));
		
		Utils.logPrint(RegistMemoWidget.class, "변경 성공");
		
		// 바뀐 값을 데이터베이스에 저장
		Utils.updateWidgetThemeIdx(context, appWidgetId, currThemeIdx);
		
		// 바뀐 값을 위젯에 적용
		appWidgetManager.updateAppWidget(appWidgetId, updateView);
	}
	
	/**
	 * 변경된 위젯 메모의 내용을 위젯에 실제로 적용합니다.
	 * 
	 * @param context
	 * @param appWidgetManager
	 * @param data
	 */
	private void updateWidgetContent(Context context, AppWidgetManager appWidgetManager, AppWidgetData data) {
		Utils.logPrint(getClass(), "updateWidgetContent 호출");
		
		RemoteViews updateView = new RemoteViews(context.getPackageName(), R.layout.widget_regist_memo);
		
		updateView.setTextViewText(R.id.widget_post_content, Utils.getWidgetModifiedContent(context, data.getContent(), 208, Constants.MAX_TEXTVIEW_LINE));
		updateView.setTextViewText(R.id.widget_post_date, Utils.getTimestampToDate(data.getDate()));
		
		appWidgetManager.updateAppWidget(data.getWidgetId(), updateView);
	}
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		super.onDeleted(context, appWidgetIds);

		deleteMemo(context, appWidgetIds[0]);
	}
	
}












