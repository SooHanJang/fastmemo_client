package madcat.studio.widget;

import madcat.studio.constants.Constants;
import madcat.studio.dialog.DigCreateMemo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

public class CreateMemoWidget extends AppWidgetProvider {

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);

		if(intent.getAction().equals(Constants.ACTION_WIDGET_MEMO_CREATE_THEME_UPDATE)) {
			Utils.logPrint(getClass(), "메모 작성 위젯 테마 변경");
			
			updateWidget(context);
		}
	}
	
	private void updateWidget(Context context) {
		ComponentName thisAppWidget = new ComponentName(context.getPackageName(), CreateMemoWidget.class.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
		
		onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	
	@Override
	/**
	 * 위젯을 Id 들을 읽어와서 등록된 위젯들을 업데이트 합니다.
	 */
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for(int i=0; i < appWidgetIds.length; i++) {
			int appwidgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, appwidgetId);		// 위젯을 업데이트 하는 메소드
		}
	}
	
	/**
	 * 파라미터에 따른 위젯을 실제로 업데이트합니다.
	 * 
	 * @param context
	 * @param appWidgetManager
	 * @param appWidgetId
	 */
	public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		RemoteViews updateView = new RemoteViews(context.getPackageName(), R.layout.widget_create_memo);
		
		SharedPreferences configPref = context.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		int themeIdx = configPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD);
		int themeResId = context.getResources().getIdentifier("icon_widget_create_memo_" + themeIdx, "drawable", context.getPackageName());
		
		updateView.setInt(R.id.widget_create_memo, "setBackgroundResource", themeResId);
		
		Intent createMemoIntent = new Intent(context, DigCreateMemo.class);
		
		updateView.setOnClickPendingIntent(R.id.widget_create_memo, 
				PendingIntent.getActivity(context, 0, createMemoIntent, 0));
		
		appWidgetManager.updateAppWidget(appWidgetId, updateView);
		
	}
}










