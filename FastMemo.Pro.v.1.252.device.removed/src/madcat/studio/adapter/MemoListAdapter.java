package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MemoListAdapter extends ArrayAdapter<Memo>{
	
	private Context mContext;
	private int mResId;
	private ArrayList<Memo> mItems;
	private LayoutInflater mInflater;
	
	public MemoListAdapter(Context context, int resId, ArrayList<Memo> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.textContent = (TextView)convertView.findViewById(R.id.memo_list_row_content);
			holder.textDate = (TextView)convertView.findViewById(R.id.memo_list_row_date);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.textContent.setText(mItems.get(position).getContent());
		holder.textDate.setText(Utils.getTimestampToDate(mItems.get(position).getDate()));
		
		
		return convertView;
	}
	
	
	class ViewHolder {
		private TextView textContent;
		private TextView textDate;
	}

}
