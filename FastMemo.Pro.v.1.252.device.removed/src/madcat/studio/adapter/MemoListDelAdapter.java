package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.data.AppWidgetData;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MemoListDelAdapter extends ArrayAdapter<Memo>{
	
	private Context mContext;
	private int mResId;
	private ArrayList<Memo> mItems;
	private LayoutInflater mInflater;
	
	private boolean mAllCheckedFlag										=	false;
	
	private boolean[] isCheckedConfirm;
	
	public MemoListDelAdapter(Context context, int resId, ArrayList<Memo> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.isCheckedConfirm = new boolean[mItems.size()];
	}
	
	public void setAllCheckedItems(boolean isChecked) {
		int tempSize = isCheckedConfirm.length;
		
		for(int i=0; i < tempSize; i++) {
			isCheckedConfirm[i] = isChecked;
		}
		
		// 전체 선택이 된 상태에서 현재 위젯으로 등록된 메모가 있다면 해당 메모는 체크를 해제한다.
		if(isChecked) {
			ArrayList<AppWidgetData> widgetData = Utils.getWidgetList(mContext);

			for(int i=0; i < widgetData.size(); i++) {
				for(int j=0; j < tempSize; j++) {
					if(mItems.get(j).getMemoId() == widgetData.get(i).getMemoId()) {
						isCheckedConfirm[j] = false;
					}
				}
			}
		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.liMemoLayout = (LinearLayout)convertView.findViewById(R.id.memo_list_del_row_layout);
			holder.textContent = (TextView)convertView.findViewById(R.id.memo_list_del_row_content);
			holder.textDate = (TextView)convertView.findViewById(R.id.memo_list_del_row_date);
			holder.checkMemo = (CheckBox)convertView.findViewById(R.id.memo_list_del_row_check);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.textContent.setText(mItems.get(position).getContent());
		holder.textDate.setText(Utils.getTimestampToDate(mItems.get(position).getDate()));
		
		holder.liMemoLayout.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				holder.checkMemo.setChecked(!holder.checkMemo.isChecked());
			}
		});

		holder.checkMemo.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					isCheckedConfirm[position] = true;
				} else {
					isCheckedConfirm[position] = false;
				}
			}
		});
		
		if(isCheckedConfirm[position]) {
			holder.checkMemo.setChecked(true);
		} else {
			holder.checkMemo.setChecked(false);
		}
		
		return convertView;
	}
	
	public ArrayList<Memo> getDelCheckedList() {
		ArrayList<Memo> results = new ArrayList<Memo>();
		
		for(int i=0; i < isCheckedConfirm.length; i++) {
			if(isCheckedConfirm[i]) {
				results.add(mItems.get(i));
			}
		}
		
		return results;
	}
	
	class ViewHolder {
		private LinearLayout liMemoLayout;
		private TextView textContent;
		private TextView textDate;
		private CheckBox checkMemo;
	}

}
