package madcat.studio.control;

import madcat.studio.async.RequestModifyAsyncTask;
import madcat.studio.async.RequestSyncnorizeAsyncTask;
import madcat.studio.constants.Constants;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.fastmemo.pro.MemoList;
import madcat.studio.fastmemo.pro.R;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.utils.CloudUtils;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CtrlModifyMemoWidget extends Activity implements OnClickListener {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private ImageButton mBtnOk, mBtnCancle;
	private EditText mEditMemo;
	private TextView mTvDate;
	private ImageView mIvDateBar;
	
	private int mAppWidgetId;
	private Memo mMemoData;
//	private String mMemoContent;
	
	private RelativeLayout mRlRoot, mRlToolBar;
	private LinearLayout mLiDate;
	private int mDigWidthPx;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Utils.logPrint(getClass(), "CtrlModifyMemoWidget 호출");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.memo_create);
		this.mContext = this;
		this.mConfigPref = mContext.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
		
		// Resource Id 설정
		mRlRoot = (RelativeLayout)findViewById(R.id.memo_create_root_layout);
		mRlToolBar = (RelativeLayout)findViewById(R.id.memo_create_tool_layout);
		mLiDate = (LinearLayout)findViewById(R.id.memo_create_date_layout);
		mEditMemo = (EditText)findViewById(R.id.memo_create_content);
		mTvDate = (TextView)findViewById(R.id.memo_create_date);
		mIvDateBar = (ImageView)findViewById(R.id.memo_create_date_seperator);
		mBtnOk = (ImageButton)findViewById(R.id.memo_create_btn_ok);
		mBtnCancle = (ImageButton)findViewById(R.id.memo_create_btn_cancle);

		mBtnOk.setOnClickListener(this);
		mBtnCancle.setOnClickListener(this);
		
		mDigWidthPx = Utils.pixelFromDip(mContext, (int) (Utils.getScreenDipWidthSize(mContext) * Constants.DIALOG_SIZE));
		mRlRoot.setLayoutParams(new RelativeLayout.LayoutParams(mDigWidthPx, mDigWidthPx));
		
		// 위젯을 통해 메모 수정 다이얼로그를 표시 시, 클라우드 서비스 상태일 경우 동기화를 먼저 진행한다.
		int cloudState = mConfigPref.getInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_OFFLINE);
		
		// 네트워크가 이용 가능할 경우,
		if(Utils.isNetworkAvailable(mContext)) {
			
			// 클라우드 서비스 이용 중일 경우,
			if(cloudState == Constants.CLOUD_STATE_ONLINE) {
				
				RequestSyncnorizeAsyncTask requestSyncnorizeAsyncTask = new RequestSyncnorizeAsyncTask(mContext, CloudUtils.getLoginData(mContext, Login.DEFAULT_PASSWORD));
				requestSyncnorizeAsyncTask.execute();
				
				requestSyncnorizeAsyncTask.setOnStatusListener(new OnStatusListener() {
					public void setOnStatusListener(CloudStatus status) {
						
						// 서버에 응답이 있을 경우, 동기화
						if(status != null) {
							switch(status.getStatus()) {
							
								// 메모 동기화 성공 시,
								case CloudStatus.MEMO_SYNC_SUCCEED:
									Utils.logPrint(getClass(), "메모 동기화 성공");
									
									boolean isSyncToLocalDB = CloudUtils.doCloudCheckToLocalDB(mContext, status.getData());
									Utils.logPrint(getClass(), "서버와 로컬DB 동기화 완료 : " + isSyncToLocalDB);
									
									doWidgetUpdate();
									
									break;
								
								// 메모 동기화 실패 시,
								case CloudStatus.MEMO_SYNC_FAILED:
									Utils.logPrint(getClass(), "메모 동기화 실패");
									
									break;
								
								// 인증키가 유효하지 않을 시,
								case CloudStatus.LOGIN_INVALIDATE_AUTH:
									Utils.logPrint(getClass(), "인증키가 유효하지 않음");
									Utils.doInvalidateAuthLogout(mContext, null);
									break;
							
							}
						}
						// 서버에 응답이 없을 경우,
						else {
							// 로그아웃을 한 뒤,
							CloudUtils.doLogout(mContext, null);
							doWidgetUpdate();
						}
						
					}
				});
				
			} 
			// 클라우드 서비스를 이용 중이지 않을 경우,
			else {
				doWidgetUpdate();
			}
			
		}
		// 네트워크가 이용 가능하지 않을 경우,
		else {
			CloudUtils.doLogout(mContext, null);
			doWidgetUpdate();
		}
		
		// 색상 설정 로드
		setToolBarColor(mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MemoList.MAIN_COLOR_EMERALD));
		
		Utils.setLastEditCursor(mEditMemo);
	}
	
	private void doWidgetUpdate() {
		Utils.logPrint(getClass(), "doWidgetUpdate 호출");
		
		// 전달받은 appId 설정
		Uri data = getIntent().getData();
		mAppWidgetId = Integer.parseInt(data.toString());
		mMemoData = Utils.getWidgetMemoContent(mContext, mAppWidgetId);
		
		Utils.logPrint(getClass(), "widgetId : " + mAppWidgetId + ", memoData : " + mMemoData);

		// 위젯으로 등록한 메모가 LocalDB 에 존재할 경우,
		if(mMemoData != null) {
			mEditMemo.setText(mMemoData.getContent());
			mTvDate.setText(Utils.getTimestampToDate(mMemoData.getDate()));
			mLiDate.setVisibility(View.VISIBLE);
		} 
		// 위젯으로 등록한 메모가 LocalDB 에 존재하지 않을 경우,
		else {
			mEditMemo.setText(getString(R.string.widget_title_wrong_removed_memo));
			mLiDate.setVisibility(View.INVISIBLE);
		}
	}
	
	private void setToolBarColor(int colorIdx) {
		if(mRlToolBar != null) {
	    	switch(colorIdx) {
		    	case MemoList.MAIN_COLOR_EMERALD:
		    		mRlToolBar.setBackgroundResource(R.color.main_color_emerald);
		    		mIvDateBar.setBackgroundResource(R.color.main_color_emerald);
		    		break;
		    	case MemoList.MAIN_COLOR_PURPLE:
		    		mRlToolBar.setBackgroundResource(R.color.main_color_purple);
		    		mIvDateBar.setBackgroundResource(R.color.main_color_purple);
		    		break;
		    	case MemoList.MAIN_COLOR_PINK:
		    		mRlToolBar.setBackgroundResource(R.color.main_color_pink);
		    		mIvDateBar.setBackgroundResource(R.color.main_color_pink);
		    		break;
		    	case MemoList.MAIN_COLOR_BLUE:
		    		mRlToolBar.setBackgroundResource(R.color.main_color_blue);
		    		mIvDateBar.setBackgroundResource(R.color.main_color_blue);
		    		break;
	    	}
    	}
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.memo_create_btn_ok:
	
				String content = mEditMemo.getText().toString().trim();
				final long date = Utils.getCurrentTimeStamp();
				
				// 위젯으로 등록한 메모가 LocalDB 에 존재할 경우
				if(mMemoData != null) {
					// 메모 내용이 변경 되었을 경우,
					if(Utils.isTextIntegrity(content)) {
						if(mMemoData != null) {
							// 메모 값이 변경 되었다면, 수정한다.
							if(mMemoData.getContent().toString().trim() != content) {
								int cloudState = mConfigPref.getInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_OFFLINE);
								
								mMemoData.setContent(content);
								mMemoData.setDate(date);
								
								Utils.logPrint(getClass(), "수정된 메모 : " + content + ", 시간(" + date + ")");
								
								switch(cloudState) {
								
									case Constants.CLOUD_STATE_OFFLINE:
										
										Utils.logPrint(getClass(), "오프라인 상태에서 위젯을 통해 메모 수정");
										
										Utils.updateWidgetMemoContent(mContext, mAppWidgetId, mMemoData);
										
										Intent updateContentIntent = new Intent(Constants.ACTION_WIDGET_MEMO_CONTENT_UPDATE);
										updateContentIntent.putExtra(Constants.PUT_WIDGET_ID, mAppWidgetId);
										updateContentIntent.putExtra(Constants.PUT_WIDGET_DATE, date);
										
										sendBroadcast(updateContentIntent);
										
										break;
										
									case Constants.CLOUD_STATE_ONLINE:
										Utils.logPrint(getClass(), "온라인 상태에서 위젯을 통해 메모 수정");
										
										RequestModifyAsyncTask requestModifyAsync = 
											new RequestModifyAsyncTask(mContext, 
													CloudUtils.getLoginData(mContext, Login.DEFAULT_PASSWORD), 
													mMemoData, 
													RequestModifyAsyncTask.WIDGET_MODIFY_EXECUTE);
										requestModifyAsync.execute();
										
										requestModifyAsync.setOnStatusListener(new OnStatusListener() {
											public void setOnStatusListener(CloudStatus status) {
												
												Utils.logPrint(getClass(), "위젯을 통해 메모를 수정 중인 경우 status : " + status);
												
												if(status != null) {
													switch(status.getStatus()) {
													
														// 서버에 메모를 성공적으로 수정하였을 경우,
														case CloudStatus.MEMO_MODIFY_SUCCEED_LOAD_SUCCEED:
														case CloudStatus.MEMO_MODIFY_SUCCEED_LOAD_FAILED:
				
															// 서버에 메모를 수정하고, 목록을 성공적으로 불러왔을 경우,
															if(status.getStatus() == CloudStatus.MEMO_MODIFY_SUCCEED_LOAD_SUCCEED) {
																boolean isSyncToLocalDB = CloudUtils.doCloudCheckToLocalDB(mContext, status.getData());
															} 
															// 서버에 메모를 수정하였지만, 목록을 불러오는 데 실패했을 경우,
															else {
																Toast.makeText(mContext, getString(R.string.toast_title_modify_succeed_load_failed), Toast.LENGTH_SHORT).show();
																Utils.updateWidgetMemoContent(mContext, mAppWidgetId, mMemoData);
															}
															
															Intent updateContentIntent = new Intent(Constants.ACTION_WIDGET_MEMO_CONTENT_UPDATE);
															updateContentIntent.putExtra(Constants.PUT_WIDGET_ID, mAppWidgetId);
															updateContentIntent.putExtra(Constants.PUT_WIDGET_DATE, date);
															
															sendBroadcast(updateContentIntent);
															
															break;
														
														// 서버에 메모를 수정하는데 실패하였을 경우,
														case CloudStatus.MEMO_MODIFY_FAILED:
															
															Toast.makeText(mContext, getString(R.string.toast_title_modify_failed), Toast.LENGTH_SHORT).show();
															
															break;
														
														// 인증키가 유효하지 않을 경우,
														case CloudStatus.LOGIN_INVALIDATE_AUTH:
															Utils.logPrint(getClass(), "위젯을 통해 수정 중 인증키가 유효하지 않습니다.");
															
															CloudUtils.doLogout(mContext, null);
															
															Utils.updateWidgetMemoContent(mContext, mAppWidgetId, mMemoData);
															
															Intent updateMemoIntent = new Intent(Constants.ACTION_WIDGET_MEMO_CONTENT_UPDATE);
															updateMemoIntent.putExtra(Constants.PUT_WIDGET_ID, mAppWidgetId);
															updateMemoIntent.putExtra(Constants.PUT_WIDGET_DATE, date);
															
															sendBroadcast(updateMemoIntent);
															
															break;
														
													}
												}
												// 서버와 응답에 실패했을 경우,
												else {
													Utils.logPrint(getClass(), "메모 수정 중 서버와 응답에 실패했습니다.");
												}
											}
													
										});
										
										break;
								
								}
							}
						}
						
						finish();
					} 
					// 메모 내용이 변경되지 않았을 경우,
					else {
						Toast.makeText(mContext, getString(R.string.toast_title_memo_integrity_fail), Toast.LENGTH_SHORT).show();
					}
					
				}
				// 위젯으로 등록한 메모가 LocalDB 에 존재하지 않을 경우,
				else {
					Toast.makeText(mContext, getString(R.string.toast_title_memo_widget_force_removed), Toast.LENGTH_SHORT).show();
					finish();
				}
				
				
				break;
			case R.id.memo_create_btn_cancle:
				finish();
				break;
		}
	}

}
