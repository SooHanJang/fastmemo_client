package madcat.studio.control;

import madcat.studio.constants.Constants;
import madcat.studio.dialog.DigOtherMemoList;
import madcat.studio.utils.Utils;
import madcat.studio.widget.RegistMemoWidget;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class CtrlThemeWidget extends Activity {
	
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mContext = this;
		
		Uri data = getIntent().getData();
		int appWidgetId = Integer.parseInt(data.toString());
		
		Utils.logPrint(getClass(), "Receive id : " + appWidgetId);

		int themeIdx = Utils.getWidgetThemeIdx(mContext, appWidgetId);
		Intent themeIndexIntent = new Intent(Constants.ACTION_WIDGET_MEMO_THEME_UPDATE);
		themeIndexIntent.putExtra(Constants.PUT_WIDGET_ID, appWidgetId);
		themeIndexIntent.putExtra(Constants.PUT_WIDGET_THEME_INDEX, themeIdx);
		
		sendBroadcast(themeIndexIntent);
		
		finish();
			
		
		
	}

}
