package madcat.studio.control;

import madcat.studio.constants.Constants;
import madcat.studio.dialog.DigOtherMemoList;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class CtrlOtherMemoWidget extends Activity {
	
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mContext = this;
		
		Uri data = getIntent().getData();
		int appWidgetId = Integer.parseInt(data.toString());
		
		Utils.logPrint(getClass(), "Receive id : " + appWidgetId);
		
		Intent otherMemoListIntent = new Intent(CtrlOtherMemoWidget.this, DigOtherMemoList.class);
		otherMemoListIntent.putExtra(Constants.PUT_WIDGET_ID, appWidgetId);
		startActivity(otherMemoListIntent);
		
		finish();
	}

}
