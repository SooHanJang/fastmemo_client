package madcat.studio.fastmemo.pro;

import java.io.File;
import java.util.ArrayList;

import madcat.studio.adapter.MemoListAdapter;
import madcat.studio.adapter.MemoListDelAdapter;
import madcat.studio.async.RequestLogoutAsyncTask;
import madcat.studio.async.RequestRemoveAsyncTask;
import madcat.studio.async.RequestSyncnorizeAsyncTask;
import madcat.studio.constants.Constants;
import madcat.studio.data.AppWidgetData;
import madcat.studio.data.CloudStatus;
import madcat.studio.data.Login;
import madcat.studio.data.Memo;
import madcat.studio.database.LoadDatabase;
import madcat.studio.dialog.DigConfirm;
import madcat.studio.dialog.DigInfoConfirm;
import madcat.studio.dialog.DigLogInCloud;
import madcat.studio.dialog.DigSelectUseCloud;
import madcat.studio.dialog.DigWriteNModifyMemo;
import madcat.studio.interfaces.OnStatusListener;
import madcat.studio.quickaction.ActionItem;
import madcat.studio.quickaction.QuickAction;
import madcat.studio.quickaction.QuickAction.OnActionItemClickListener;
import madcat.studio.utils.CloudUtils;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class MemoList extends ListActivity implements OnClickListener, OnActionItemClickListener, 
								OnEditorActionListener, OnCheckedChangeListener, OnItemLongClickListener {
	
	public static final int MAIN_COLOR_EMERALD									=	0;
	public static final int MAIN_COLOR_PURPLE									=	1;
	public static final int MAIN_COLOR_PINK										=	2;
	public static final int MAIN_COLOR_BLUE										=	3;
	
	public static final int QUICK_DELETE_MEMO									=	0;
	public static final int QUICK_BACKUP_MEMO									=	1;
	public static final int QUICK_RESTORE_MEMO									=	2;
	public static final int QUICK_LOGIN_CLOUD									=	3;
	public static final int QUICK_LOGOUT_CLOUD									=	4;
	
	public static final int SEARCH_TYPE_WORD									=	0;
	public static final int SEARCH_TYPE_DATE									=	1;
	
	public static final int MEMO_LIST_STATE_DEFAULT								=	0;
	public static final int MEMO_LIST_STATE_DELETE								=	1;
	
	public static final int MEMO_DATA_BACKUP									=	0;
	public static final int MEMO_DATA_RESTORE									=	1;
	
	private Context mContext;
	private SharedPreferences mConfigPref;

	private MemoListAdapter mAdapter;
	private MemoListDelAdapter mDelAdapter;
	private ArrayList<Memo> mItems, mLongclickItems;

	private int mMemoMode														=	MEMO_LIST_STATE_DEFAULT;
	private int mMemoSearchType													=	SEARCH_TYPE_WORD;
	
	// Memo List View 관련 변수
	private ListView mListView;
	private RelativeLayout mRlChangeColorBar;
	private LinearLayout mLiDynamicDel;
	private Button mBtnDelOk, mBtnDelCancel;
	private ImageButton mBtnSyncCloud, mBtnMemoAdd;
	
	// Search Layout 관련 변수
	private LinearLayout mLiHeader;
	private QuickAction mQuickAction;
	private ImageView mIvSearchBar, mIvSearchBarLeft, mIvSearchBarRight;
	private ImageButton mBtnSearch, mBtnConfig, mBtnSearchType;
	private EditText mEditSearch;
	
	// Memo Del Layout 관련 변수
	private LinearLayout mLiDelHeader;
	private CheckBox mCbMemoAllChecked;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memo_list);
        
        // 기본 설정
        this.mContext = this;
        this.mConfigPref = getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
        
        // 앱의 최초 실행 여부
        // 앱을 최초 실행 = true, 앱을 이미 실행 = false
        boolean appFirstFlag = mConfigPref.getBoolean(Constants.PREF_CONFIG_APP_FIRST, true);
        
        // 테스트 코드
//        appFirstFlag = true;
        
        InitLoadAsync initLoadAsync = new InitLoadAsync(appFirstFlag);
        initLoadAsync.execute();
        
    }

    /**
     * 앱에 사용할 Resource 전체를 초기화 한다.
     * 
     */
    private void initMemoListView() {
    	 // 롱 클릭에 사용할 ArrayList 객체 생성
        mLongclickItems = new ArrayList<Memo>();
    	
    	int cloudState = mConfigPref.getInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_OFFLINE);
    	
    	initMemoLayout(cloudState);
    	initMemoDelLayout();
    	
    	initMemoSearchLayout(cloudState);
		updateMemoListAdapter();
    }
    
    /**
     * 메모 리스트의 기본 자원을 설정한다.
     * 
     */
    private void initMemoLayout(int cloudOnlineState) {
    	// 메모 리스트 Resource Id 설정
    	mRlChangeColorBar = (RelativeLayout)findViewById(R.id.memo_list_change_color_bar);
    	mLiHeader = (LinearLayout)findViewById(R.id.memo_list_header_layout);
        mLiDynamicDel = (LinearLayout)findViewById(R.id.memo_list_del_layout);
        
        mBtnDelOk = (Button)findViewById(R.id.memo_list_del_btn_ok);
        mBtnDelCancel = (Button)findViewById(R.id.memo_list_del_btn_cancel);
        mBtnSyncCloud = (ImageButton)findViewById(R.id.memo_list_sync_cloud);
        mBtnMemoAdd = (ImageButton)findViewById(R.id.memo_list_add_memo);
        
        mListView = getListView();
        
        // Event Listener 설정
        mRlChangeColorBar.setOnClickListener(this);
        mBtnDelOk.setOnClickListener(this);
        mBtnDelCancel.setOnClickListener(this);
        mBtnSyncCloud.setOnClickListener(this);
        mBtnMemoAdd.setOnClickListener(this);
        
        mListView.setOnItemLongClickListener(this);
        
        // 동기화 버튼 표시 여부 설정
        switch(cloudOnlineState) {
	        case Constants.CLOUD_STATE_ONLINE:
	        	mBtnSyncCloud.setVisibility(View.VISIBLE);
	        	break;
	        	
	        case Constants.CLOUD_STATE_OFFLINE:
	        	mBtnSyncCloud.setVisibility(View.GONE);
	        	break;
        }
    }
    
    /**
     * Search Layout 에 해당하는 기본 자원을 설정한다.
     * 
     */
    private void initMemoSearchLayout(int cloudOnlineState) {
		
		// Header Resource Id 설정
    	mIvSearchBar = (ImageView)findViewById(R.id.memo_list_header_search_bar);
    	mIvSearchBarLeft = (ImageView)findViewById(R.id.memo_list_header_search_bar_left);
    	mIvSearchBarRight = (ImageView)findViewById(R.id.memo_list_header_search_bar_right);
    	
        mBtnSearch = (ImageButton)findViewById(R.id.memo_list_header_btn_search);
        mBtnSearchType = (ImageButton)findViewById(R.id.memo_list_header_btn_search_type);
        mEditSearch = (EditText)findViewById(R.id.memo_list_header_edit_search);
        mBtnConfig = (ImageButton)findViewById(R.id.memo_list_header_config);
        
        mEditSearch.setFocusable(true);
        
        // Header Event Listener 설정
        mBtnSearch.setOnClickListener(this);
        mEditSearch.setOnClickListener(this);
        mEditSearch.setOnEditorActionListener(this);
        mBtnSearchType.setOnClickListener(this);
        mBtnConfig.setOnClickListener(this);
        
        // 기본 설정 로드
        setSearchType(mMemoSearchType);
        setMainColor(mConfigPref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MAIN_COLOR_EMERALD));
        
        // Quick Action 설정
        mQuickAction = new QuickAction(mContext);
        
        // 클라우드 서비스 이용 상태에 따라 Quick Action 메뉴 설정
        switch(cloudOnlineState) {
        
	        case Constants.CLOUD_STATE_OFFLINE:
	        	
	        	ActionItem qOffItemDelMemo = new ActionItem(QUICK_DELETE_MEMO, 
	            		getString(R.string.quick_delete_memo), getResources().getDrawable(R.drawable.icon_quickaction_memo_delete));
	    		ActionItem qOffItemMngMemo = new ActionItem(QUICK_BACKUP_MEMO,
	    				getString(R.string.quick_backup_memo), getResources().getDrawable(R.drawable.icon_quickaction_memo_backup));
	    		ActionItem qOffItemRestoreMemo = new ActionItem(QUICK_RESTORE_MEMO,
	    				getString(R.string.quick_restore_memo), getResources().getDrawable(R.drawable.icon_quickaction_memo_restore));
	    		ActionItem qOffItemCloudLogin = new ActionItem(QUICK_LOGIN_CLOUD,
	    				getString(R.string.quick_login_cloud), getResources().getDrawable(R.drawable.icon_quickaction_cloud_login));
	    		
	    		mQuickAction.addActionItem(qOffItemDelMemo);
	    		mQuickAction.addActionItem(qOffItemMngMemo);
	    		mQuickAction.addActionItem(qOffItemRestoreMemo);
	    		mQuickAction.addActionItem(qOffItemCloudLogin);
	        	
	        	break;
	        
	        case Constants.CLOUD_STATE_ONLINE:
	        	
	        	ActionItem qOnItemDelMemo = new ActionItem(QUICK_DELETE_MEMO, 
	            		getString(R.string.quick_delete_memo), getResources().getDrawable(R.drawable.icon_quickaction_memo_delete));
	        	ActionItem qOnItemCloudLogin = new ActionItem(QUICK_LOGOUT_CLOUD,
	    				getString(R.string.quick_logout_cloud), getResources().getDrawable(R.drawable.icon_quickaction_cloud_login));
	        	
	        	mQuickAction.addActionItem(qOnItemDelMemo);
	    		mQuickAction.addActionItem(qOnItemCloudLogin);
	        	
	        	break;
        
        }
		
		mQuickAction.setOnActionItemClickListener(this);
    }
    
    private void setSearchType(int type) {
    	mMemoSearchType = type;
    	
    	switch(type) {
	    	case SEARCH_TYPE_WORD:
	    		mBtnSearchType.setBackgroundResource(R.drawable.icon_memo_search_type_word);
	    		mEditSearch.setText("");
	    		mEditSearch.setHint("ex) keyword");
	    		mEditSearch.setInputType(InputType.TYPE_CLASS_TEXT);
	    		break;
	    	case SEARCH_TYPE_DATE:
	    		mBtnSearchType.setBackgroundResource(R.drawable.icon_memo_search_type_calendar);
	    		mEditSearch.setText("");
	    		mEditSearch.setHint("ex) 20120817");
	    		mEditSearch.setInputType(InputType.TYPE_CLASS_NUMBER);
	    		break;
    	}
    }
    
    private void setMainColor(int colorIdx) {
    	if(mRlChangeColorBar != null) {
    		Utils.sendBroadCasting(mContext, Constants.ACTION_WIDGET_MEMO_CREATE_THEME_UPDATE);
    		
	    	switch(colorIdx) {
		    	case MAIN_COLOR_EMERALD:
		    		mRlChangeColorBar.setBackgroundResource(R.color.main_color_emerald);
		    		mIvSearchBar.setBackgroundResource(R.drawable.icon_memo_search_bar_0);
		    		mIvSearchBarLeft.setBackgroundResource(R.drawable.icon_memo_search_bar_left_0);
		    		mIvSearchBarRight.setBackgroundResource(R.drawable.icon_memo_search_bar_right_0);
		    		break;
		    	case MAIN_COLOR_PURPLE:
		    		mRlChangeColorBar.setBackgroundResource(R.color.main_color_purple);
		    		mIvSearchBar.setBackgroundResource(R.drawable.icon_memo_search_bar_1);
		    		mIvSearchBarLeft.setBackgroundResource(R.drawable.icon_memo_search_bar_left_1);
		    		mIvSearchBarRight.setBackgroundResource(R.drawable.icon_memo_search_bar_right_1);
		    		break;
		    	case MAIN_COLOR_PINK:
		    		mRlChangeColorBar.setBackgroundResource(R.color.main_color_pink);
		    		mIvSearchBar.setBackgroundResource(R.drawable.icon_memo_search_bar_2);
		    		mIvSearchBarLeft.setBackgroundResource(R.drawable.icon_memo_search_bar_left_2);
		    		mIvSearchBarRight.setBackgroundResource(R.drawable.icon_memo_search_bar_right_2);
		    		break;
		    	case MAIN_COLOR_BLUE:
		    		mRlChangeColorBar.setBackgroundResource(R.color.main_color_blue);
		    		mIvSearchBar.setBackgroundResource(R.drawable.icon_memo_search_bar_3);
		    		mIvSearchBarLeft.setBackgroundResource(R.drawable.icon_memo_search_bar_left_3);
		    		mIvSearchBarRight.setBackgroundResource(R.drawable.icon_memo_search_bar_right_3);
		    		break;
	    	}
    	}
    }
    
    /**
     * Memo Delete Layout 에 해당하는 기본 자원을 설정한다.
     * 
     */
    private void initMemoDelLayout() {
    	mLiDelHeader = (LinearLayout)findViewById(R.id.memo_list_del_header_layout);
    	mCbMemoAllChecked = (CheckBox)findViewById(R.id.memo_list_del_header_check);
    	
    	mLiDelHeader.setOnClickListener(this);
    	mCbMemoAllChecked.setOnCheckedChangeListener(this);
    }
    
    /**
     * DB 에서 전체 메모 값을 불러온다.
     * 
     */
    private void updateMemoListAdapter() {
    	Utils.logPrint(getClass(), "updateMemoListAdapter 호출");
    	
    	// 실제 앱을 실행했을 시의 구현은 이곳에 작성
    	if(mAdapter != null) {
    		mAdapter.clear();
    	}
    	
		mItems = Utils.getMemoList(mContext);
        Utils.logPrint(getClass(), "Local DB 에 저장 된 메모의 개수 : " + mItems.size());
        
    	mAdapter = new MemoListAdapter(mContext, R.layout.memo_list_row, mItems);
    	
        setListAdapter(mAdapter);
        
//        // 위젯으로 등록된 목록을 가져온 뒤,
//        for(int i=0; i < mItems.size(); i++) {
//        	ArrayList<AppWidgetData> widgetArray = Utils.getUpdateWidgetContentToMemo(mContext, mItems.get(i).getMemoId());
//        	for(int j=0; j < widgetArray.size(); j++) {
//        		
//        		int dbMemoId = mItems.get(i).getMemoId();
//        		int widgetMemoId = widgetArray.get(j).getMemoId();
//        		long dbDate = mItems.get(i).getDate();
//        		
//        		// LocalDB 에 저장된 값과 Widget으로 등록된 메모Id 값이 일치한다면,
//        		if(dbMemoId == widgetMemoId) {
//        			// 위젯을 업데이트 한다.
//					Intent updateWidgetMemo = new Intent(Constants.ACTION_MEMO_CONTENT_UPDATE);
//					updateWidgetMemo.putExtra(Constants.PUT_MEMO_ID, dbMemoId);
//					updateWidgetMemo.putExtra(Constants.PUT_MEMO_DATE, dbDate);
//					
//					mContext.sendBroadcast(updateWidgetMemo);
//        		}
//        		
//        	}
//        }
        
        
    }
    
    /**
     * 서버로 부터 넘어온 메모 데이터와 LocalDB 를 동기화 한다.
     * 
     * @param jString
     */
    private void updateCloudMemoListAdapter(String jString) {
    	Utils.logPrint(getClass(), "updateCloudMemoListAdapter");
    	
		boolean isSyncToLocalDB = CloudUtils.doCloudCheckToLocalDB(mContext, jString);
		Utils.logPrint(getClass(), "서버와 로컬DB 동기화 완료 : " + isSyncToLocalDB);
		
		if(isSyncToLocalDB) {
			updateMemoListAdapter();
		}
    }

    /**
     * 서버에 동기화를 요청한 뒤, 서버의 메모를 읽어와서 LocalDB 와 동기화 후 ListView 를 설정한다.
     * 
     * @param jString
     */
    private void doSyncCloudMemoList() {
    	Utils.logPrint(getClass(), "doSyncCloudMemoList 호출");
    	
    	// 메모 목록을 기본 어댑터로 변경
		ctrlMemoListState(MEMO_LIST_STATE_DEFAULT);
    	
    	// 정보 다이얼로그 설정
    	final DigInfoConfirm digInfoConfirm =	new DigInfoConfirm(mContext);
    	digInfoConfirm.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				initMemoListView();
			}
		});
    	
    	// 동기화 애니메이션 시작
    	if(mBtnSyncCloud != null) {
    		mBtnSyncCloud.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.anim_sync_cloud));
    	}

    	// 서버와 동기화 요청 및 처리
    	RequestSyncnorizeAsyncTask requestSyncAsync = 
    		new RequestSyncnorizeAsyncTask(mContext, CloudUtils.getLoginData(mContext, Login.DEFAULT_PASSWORD));
    	requestSyncAsync.execute();
    	
    	requestSyncAsync.setOnStatusListener(new OnStatusListener() {
			public void setOnStatusListener(CloudStatus status) {
				
				// 서버에 응답이 있을 경우, 동기화
				if(status != null) {
					switch(status.getStatus()) {
					
						// 메모 동기화 성공 시,
						case CloudStatus.MEMO_SYNC_SUCCEED:
							Utils.logPrint(getClass(), "메모 동기화 성공");
							
							if(mAdapter != null) {
					    		mAdapter.clear();
					    	}
							
							boolean isSyncToLocalDB = CloudUtils.doCloudCheckToLocalDB(mContext, status.getData());
							Utils.logPrint(getClass(), "서버와 로컬DB 동기화 완료 : " + isSyncToLocalDB);
							
							// 클라우드 서비스 이용 중, 수정하려는 메모가 서버에 남아 있는지 확인하여 업데이트 한다.
							if(isSyncToLocalDB) {
								updateMemoListAdapter();
							}
							
							break;
						
						// 메모 동기화 실패 시,
						case CloudStatus.MEMO_SYNC_FAILED:
							Utils.logPrint(getClass(), "메모 동기화 실패");
							
							digInfoConfirm.setTitle(getString(R.string.tv_title_caution));
							digInfoConfirm.setMessage(getString(R.string.dig_msg_sync_error));
							digInfoConfirm.show();
							
							break;
						
						// 인증키가 유효하지 않을 시,
						case CloudStatus.LOGIN_INVALIDATE_AUTH:
							Utils.logPrint(getClass(), "인증키가 유효하지 않음");
							
							doCheckAuthFailed(mContext);
							break;
					
					}
				}
				// 서버에 응답이 없을 경우,
				else {
					// 로그아웃을 한 뒤,
					CloudUtils.doLogout(mContext, mBtnSyncCloud);
					
					// 서버에 응답이 없다는 다이얼로그를 띄워준다.
					digInfoConfirm.setTitle(getString(R.string.tv_title_caution));
					digInfoConfirm.setMessage(getString(R.string.dig_msg_server_cannot_connected));
					digInfoConfirm.show();
				}
				
				// 동기화 애니메이션 멈춤
				if(mBtnSyncCloud != null) {
					mBtnSyncCloud.clearAnimation();
				}
			}
		});
    }
    
    /**
     * 서버가 응답이 없을 경우, 로그아웃을 진행한 뒤 메모 목록을 갱신한다.
     * 
     * @param context
     */
    public void doLogoutToServerDown(Context context) {
    	Utils.logPrint(getClass(), "doLogoutToServerDown 호출");
    	
    	DigInfoConfirm digInforConfirm = 
    		new DigInfoConfirm(mContext, getString(R.string.tv_title_caution), getString(R.string.dig_msg_server_cannot_connected));
    	digInforConfirm.show();
    	
    	digInforConfirm.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				CloudUtils.doLogout(mContext, mBtnSyncCloud);
				initMemoListView();
			}
		});
    }
    

    /**
     * 인증키가 유효하지 않을 시, 로그아웃을 진행한 뒤, 메모 목록을 갱신한다.
     * 
     * @param context
     */
    private void doCheckAuthFailed(Context context) {
    	Utils.logPrint(getClass(), "doCheckAuthFailed 호출");
    	
    	Utils.doInvalidateAuthLogout(context, mBtnSyncCloud);
		
		// LocalDB 의 메모 데이터를 불러와 어댑터를 설정한다.
    	initMemoListView();
    }
    
    /**
     * 조건에 따른 메모 목록 값을 불러온다.
     * 
     * @param items
     */
    private void updateMemoListAdapter(ArrayList<Memo> items) {
    	if(mAdapter != null) {
    		mAdapter.clear();
    	}
    	
    	mItems = items;
    	mAdapter = new MemoListAdapter(mContext, R.layout.memo_list_row, mItems);
        setListAdapter(mAdapter);
    }
    
    private void deleteMemoListAdapter() {
    	if(mDelAdapter != null) {
    		mDelAdapter.clear();
    	}
    	
    	mDelAdapter = new MemoListDelAdapter(mContext, R.layout.memo_list_del_row, mItems);
    	setListAdapter(mDelAdapter);
    }
    
    private void ctrlMemoListState(int state) {
    	Utils.logPrint(getClass(), "ctrlMemoListState 호출");
    	
    	mMemoMode = state;
    	mCbMemoAllChecked.setChecked(false);
    	
    	switch(state) {
	    	case MEMO_LIST_STATE_DEFAULT:
	    		mLiHeader.setVisibility(View.VISIBLE);
	    		mLiDelHeader.setVisibility(View.GONE);
	    		
	    		updateMemoListAdapter();
	    		mEditSearch.setText("");
	    		mLiDynamicDel.setVisibility(View.GONE);
	    		
	    		mBtnSearch.setClickable(true);
	    		mBtnSearchType.setClickable(true);
	    		mEditSearch.setClickable(true);
	    		mEditSearch.setFocusable(true);
	    		mEditSearch.setFocusableInTouchMode(true);
	    		
	    		mBtnMemoAdd.setBackgroundResource(R.drawable.common_add_btn);
	    		
	    		break;
	    	case MEMO_LIST_STATE_DELETE:
	    		mLiHeader.setVisibility(View.GONE);
	    		mLiDelHeader.setVisibility(View.VISIBLE);
	    		
	    		deleteMemoListAdapter();
	    		mEditSearch.setText("");
	    		mLiDynamicDel.setVisibility(View.VISIBLE);
	    		
	    		mBtnSearch.setClickable(false);
	    		mBtnSearchType.setClickable(false);
	    		mEditSearch.setClickable(false);
	    		mEditSearch.setFocusable(false);
	    		mEditSearch.setFocusableInTouchMode(false);
	    		
	    		mBtnMemoAdd.setBackgroundResource(R.drawable.icon_memo_delete);
	    		
	    		
	    		break;
    	}
    }
    
    @Override
    protected void onListItemClick(ListView l, View v, final int position, long id) {
    	Utils.logPrint(getClass(), "pos(" + position + "), dbId(" + mItems.get(position).getMemoId() + "), cloudId(" + mItems.get(position).getCloudId() + ")");
    	
    	final Memo modifiedMemo = mItems.get(position);
    	
    	final DigWriteNModifyMemo digModify = new DigWriteNModifyMemo(mContext, modifiedMemo, DigWriteNModifyMemo.DIG_MODE_MEMO_MODIFY);
    	digModify.show();
    	
    	digModify.setOnStatusListener(new OnStatusListener() {
			public void setOnStatusListener(CloudStatus status) {
				
				// 서버와 응답에 성공했을 경우,
				if(status != null) {
					Utils.logPrint(getClass(), "메모 수정 후 Status : " + status.getStatus());
					
					switch(status.getStatus()) {
						
						// Cloud 서비스 미 이용 시, 메모 수정 했을 경우,
						case CloudStatus.NO_CLOUD_MEMO_MODIFY:
							// 메모 어댑터 업데이트
							updateMemoListAdapter();
							
							// 위젯 업데이트를 위한 BroadCasting
							// 클라우드 서비스를 이용하지 않은 상태에서 메모를 업데이트 할 때는, status 에 메모 아이디, Timestamp 로 Return 된다.
							String[] updateMemo = status.getData().split(",");
							
							Intent updateNoCloudMemoIntent = new Intent(Constants.ACTION_MEMO_CONTENT_UPDATE);
							updateNoCloudMemoIntent.putExtra(Constants.PUT_MEMO_ID, Integer.parseInt(updateMemo[0]));
							updateNoCloudMemoIntent.putExtra(Constants.PUT_MEMO_DATE, Long.parseLong(updateMemo[1]));
							
							mContext.sendBroadcast(updateNoCloudMemoIntent);
							
							break;
							
						// 서버에 메모를 성공적으로 수정하였을 경우,
						case CloudStatus.MEMO_MODIFY_SUCCEED_LOAD_SUCCEED:
						case CloudStatus.MEMO_MODIFY_SUCCEED_LOAD_FAILED:

							// 서버에 메모를 수정하고, 목록을 성공적으로 불러왔을 경우,
							if(status.getStatus() == CloudStatus.MEMO_MODIFY_SUCCEED_LOAD_SUCCEED) {
								updateCloudMemoListAdapter(status.getData());
							} 
							// 서버에 메모를 수정하였지만, 목록을 불러오는 데 실패했을 경우,
							else {
								Toast.makeText(mContext, getString(R.string.toast_title_modify_succeed_load_failed), Toast.LENGTH_SHORT).show();
								updateMemoListAdapter();
							}
							
							// 클라우드 서비스 이용 중, 수정하려는 메모가 서버에 남아 있는지 확인하여 업데이트 한다.
							for(int i=0; i < mItems.size(); i++) {
								int memoId = mItems.get(i).getMemoId();
								long memoDate = mItems.get(i).getDate();
								
								if(modifiedMemo.getMemoId() == memoId) {
									// 위젯 업데이트를 위한 BroadCasting
									Intent updateCloudMemoIntent = new Intent(Constants.ACTION_MEMO_CONTENT_UPDATE);
									updateCloudMemoIntent.putExtra(Constants.PUT_MEMO_ID, memoId);
									updateCloudMemoIntent.putExtra(Constants.PUT_MEMO_DATE, memoDate);
									
									mContext.sendBroadcast(updateCloudMemoIntent);
								}
							}

							break;
						
						// 서버에 메모를 수정하는데 실패하였을 경우,
						case CloudStatus.MEMO_MODIFY_FAILED:
							
							Toast.makeText(mContext, getString(R.string.toast_title_modify_failed), Toast.LENGTH_SHORT).show();
							
							break;
						
						// 인증키가 유효하지 않을 경우,
						case CloudStatus.LOGIN_INVALIDATE_AUTH:
							doCheckAuthFailed(mContext);
							break;
						
					}
				} 
				// 서버와 응답에 실패했을 경우,
				else {
					Utils.logPrint(getClass(), "메모 수정 중 서버와 응답에 실패했습니다.");
					doLogoutToServerDown(mContext);
				}
				
				initSearchEditText();
				
			
			}	// end statusListener
		});
    	
    	
    	super.onListItemClick(l, v, position, id);
    }
    
 // Search EditText 초기화
    private void initSearchEditText() {
		mEditSearch.setText("");
		mEditSearch.setFocusable(false);
		Utils.toggleSoftKeyboard(mContext);
		mEditSearch.setFocusableInTouchMode(true);
    }
    
    public void onClick(View v) {
    	
    	int id = v.getId();

    	// 퀵 액션 버튼 클릭
    	if(id == mBtnConfig.getId()) {
    		if(mMemoMode != MEMO_LIST_STATE_DELETE) {
				mQuickAction.show(v);
			}
    	}
    	// 검색 버튼 클릭
    	else if(id == mBtnSearch.getId()) {
    		if(mMemoMode != MEMO_LIST_STATE_DELETE) {
				String searchData = mEditSearch.getText().toString().trim();
				
				// 검색 결과 값이 없을 경우, 기본 어댑터로 설정
				if(searchData.length() == 0) {
					updateMemoListAdapter();
				} 
				// 검색어가 1글자 이하일 경우, 경고 토스트 표시
				else if(searchData.length() > 0 && searchData.length() <= 1) {
					Toast.makeText(mContext, getString(R.string.toast_title_search_length), Toast.LENGTH_SHORT).show();
				} 
				// 검색어가 2글자 이상일 경우,
				else {
					ArrayList<Memo> items = null;
					
					switch(mMemoSearchType) {
						case SEARCH_TYPE_WORD:
							items = Utils.getSearchMemo(mContext, SEARCH_TYPE_WORD, searchData);
							break;
						case SEARCH_TYPE_DATE:
							
							// 날짜 검색 값이 8 글자가 아닐 경우,
							if(searchData.length() != Constants.SEARCH_DATE_MAX_LENGTH) {
								Toast.makeText(mContext, getString(R.string.toast_title_search_date_length), Toast.LENGTH_SHORT).show();
								initSearchEditText();
							} 
							// 날짜 검색 값이 8글자일 경우,
							else {
								items = Utils.getSearchMemo(mContext, SEARCH_TYPE_DATE, searchData);
							}
							break;
					}
					
					if(items != null) {
						// 일치하는 값이 존재하지 않을 경우,
						if(items.isEmpty()) {
							Toast.makeText(mContext, getString(R.string.toast_title_search_no_data), Toast.LENGTH_SHORT).show();
							initSearchEditText();
						} 
						
						updateMemoListAdapter(items);
					}
				}

//				Utils.toggleSoftKeyboard(mContext);
			}
    	}
    	// 삭제 확인 버튼 클릭
    	else if(id == mBtnDelOk.getId()) {
    		// 위젯에 등록한 메모를 삭제하려고 한다면, "위젯으로 등록한 메모가 존재합니다. 위젯을 제거한 뒤, 메모를 삭제해주세요." Toast 출력
			// 위젯으로 등록한 메모가 존재하지 않는다면, "메모를 삭제하시겠습니까?" 다이얼로그 띄워준 뒤, 확인을 누르면 삭제
			if(mDelAdapter != null) {
				if(mDelAdapter.getDelCheckedList().isEmpty()) {			//	삭제 목록이 존재하지 않은 채, 확인 버튼을 눌렀을 경우
					
					Toast.makeText(mContext, getString(R.string.toast_title_memo_del_check), Toast.LENGTH_SHORT).show();
					
				} else {		//	삭제할 목록이 존재하는 경우
					
					// true = 위젯으로 등록한 메모가 존재, false = 위젯으로 등록한 메모가 존재하지 않음
					if(Utils.isMemoRegistedWidget(mContext, mDelAdapter.getDelCheckedList())) {
						Toast.makeText(mContext, getString(R.string.toast_title_memo_widget_registed), Toast.LENGTH_LONG).show();
					} else {
						doRemoveMemo(mDelAdapter.getDelCheckedList());
					}
				}
			}
    	}
    	// 삭제 취소 버튼 클릭
    	else if(id == mBtnDelCancel.getId()) {
    		ctrlMemoListState(MEMO_LIST_STATE_DEFAULT);
    	}
    	// 메모 추가 버튼 클릭
    	else if(id == mBtnMemoAdd.getId()) {
    		if(mMemoMode != MEMO_LIST_STATE_DELETE) {
				final DigWriteNModifyMemo digWriteMemo = new DigWriteNModifyMemo(mContext, DigWriteNModifyMemo.DIG_MODE_MEMO_WRITE);
				digWriteMemo.show();
				
				digWriteMemo.setOnStatusListener(new OnStatusListener() {
					public void setOnStatusListener(CloudStatus status) {
						Utils.logPrint(getClass(), "MemoList 에서 메모 작성 후 전송 받은 Status : " + status);
						
						// 서버와 응답에 성공했을 경우,
						if(status != null) {
							Utils.logPrint(getClass(), "Memo작성 Status : " + status.getStatus());
							
							switch(status.getStatus()) {
							
								// 클라우드 미 이용시 메모를 작성했을 경우,
								case CloudStatus.NO_CLOUD_MEMO_SAVE:
									updateMemoListAdapter();
									break;
								
								// 메모 작성에 성공하고, 목록을 성공적으로 불러왔을 경우,
								case CloudStatus.MEMO_SAVE_SUCCEED_LOAD_SUCCEED:
									
									updateCloudMemoListAdapter(status.getData());
									
									break;
								
								// 메모 작성에 성공하고, 목록 불러오기에 실패했을 경우,
								case CloudStatus.MEMO_SAVE_SUCCEED_LOAD_FAILED:
									
									updateMemoListAdapter();
									
									Toast.makeText(mContext, 
											getString(R.string.toast_title_modify_succeed_load_failed), Toast.LENGTH_SHORT).show();
									
									break;
								
								// 메모 작성에 실패했을 경우,
								case CloudStatus.MEMO_SAVE_FAILED:
									
									Toast.makeText(mContext, getString(R.string.toast_title_save_failed), Toast.LENGTH_SHORT).show();
									break;

								// 인증키가 유효하지 않을 시,
								case CloudStatus.LOGIN_INVALIDATE_AUTH:
									doCheckAuthFailed(mContext);
									break;
							}
						} 
						// 서버와 응답에 실패했을 경우,
						else {
							Utils.logPrint(getClass(), "메모 작성 중 서버와 응답에 실패하였습니다.");
							doLogoutToServerDown(mContext);
						}
					}
				});				
			}
    	}
    	// 검색 창 클릭
    	else if(id == mEditSearch.getId()) {
    		Utils.logPrint(getClass(), "검색창 클릭 이벤트 발생");
			
			if(mMemoMode != MEMO_LIST_STATE_DELETE) {
				
				mEditSearch.setFocusable(true);
				mEditSearch.requestFocusFromTouch();
				Utils.showVirturalKeyboard(mContext);
					
			}
    	}
    	// 메모 전체 삭제 버튼 클릭
    	else if(id == mLiDelHeader.getId()) {
    		mCbMemoAllChecked.setChecked(!mCbMemoAllChecked.isChecked());
    	}
    	// 검색 타입 버튼 클릭
    	else if(id == mBtnSearchType.getId()) {
    		mMemoSearchType = mMemoSearchType == SEARCH_TYPE_WORD ? SEARCH_TYPE_DATE : SEARCH_TYPE_WORD;
			setSearchType(mMemoSearchType);
			
			Utils.hideSoftKeyboard(mContext, mEditSearch);
			updateMemoListAdapter();
    	}
    	// 색상 변경 바 클릭
    	else if(id == mRlChangeColorBar.getId()) {
    		Utils.logPrint(getClass(), "changeColorBar 클릭");
    		
			int colorIdx = changeMainColorIdx(mContext);
			setMainColor(colorIdx);
    	}
		// 동기화 버튼 클릭
    	else if(id == mBtnSyncCloud.getId()) {
    		doSyncCloudMemoList();
    		
    	}
				
	}
    
    private int changeMainColorIdx(Context context) {
    	SharedPreferences pref = context.getSharedPreferences(Constants.PREF_CONFIG_NAME, Activity.MODE_PRIVATE);
    	
    	int idx = pref.getInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, MAIN_COLOR_EMERALD);;
    	
    	if(idx < 3) {
    		idx += 1;
		} else {
			idx = 0;
		}
    	
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt(Constants.PREF_CONFIG_APP_COLOR_INDEX, idx);
		editor.commit();
		
		return idx;
    }

    /**
     * QuickAction Click 시 발생하는 EventListener
     * 
     */
	public void onItemClick(QuickAction source, int pos, int actionId) {
		switch(actionId) {
			// 메모 삭제 퀵 액션 클릭
			case QUICK_DELETE_MEMO:
				if(!mItems.isEmpty()) {
					if(mMemoMode != MEMO_LIST_STATE_DELETE) {
						ctrlMemoListState(MEMO_LIST_STATE_DELETE);
					}
				} else {
					Toast.makeText(mContext, getString(R.string.toast_title_delete_memo_no_data), Toast.LENGTH_SHORT).show();
				}
				
				break;
		
			// 메모 백업 퀵 액션 클릭
			case QUICK_BACKUP_MEMO:
				
				if(Utils.isSDCardMounted()) {
					final DigConfirm backConfirmDig = 
						new DigConfirm(mContext, getString(R.string.quick_backup_memo), 
								getString(R.string.dig_msg_memo_backup), R.drawable.icon_quickaction_memo_backup);
					backConfirmDig.show();
					
					backConfirmDig.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							if(backConfirmDig.getDigState()) {
								DataBackupRecoveryAsync backupAsync = new DataBackupRecoveryAsync(MEMO_DATA_BACKUP);
								backupAsync.execute();
							}
						}
					});
				} else {
					Toast.makeText(mContext, getString(R.string.toast_title_sd_card_check), Toast.LENGTH_SHORT).show();
				}
				
				break;
				
			// 메모 복원 퀵 액션 클릭
			case QUICK_RESTORE_MEMO:

				// 위젯으로 등록된 메모가 존재할 경우,
				if(Utils.isRegistedMemoWidget(mContext)) {
					Toast.makeText(mContext, getString(R.string.toast_title_memo_not_restore), Toast.LENGTH_LONG).show();
				} 
				// 위젯으로 등록된 메모가 존재하지 않을 경우,
				else {
					final ArrayList<String> backupFileList = Utils.getBackupFileList(mContext);
					
					// BackupFile 목록이 존재할 경우,
					if(backupFileList != null) {
						final String[] listfileItems = Utils.splitFileName(mContext, backupFileList);
						
						if(listfileItems.length != 0) {
							AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
							builder.setTitle(getString(R.string.tv_title_select_restore_memo));
							builder.setItems(listfileItems, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, final int position) {
									
									final DigConfirm digRestoreConfirm = 
										new DigConfirm(mContext, getString(R.string.quick_restore_memo), 
												listfileItems[position] + getString(R.string.dig_msg_memo_recovery),
												R.drawable.icon_quickaction_memo_restore);
									
									digRestoreConfirm.setOnDismissListener(new OnDismissListener() {
										public void onDismiss(DialogInterface dialog) {
											if(digRestoreConfirm.getDigState()) {
												DataBackupRecoveryAsync dataRecoverAsync = 
													new DataBackupRecoveryAsync(MEMO_DATA_RESTORE, backupFileList.get(position));
												dataRecoverAsync.execute();
											}
										}
									});
									
									digRestoreConfirm.show();
								}
							});
							
							builder.show();
							
						}
					}
				}
				
				break;
			
			// 로그인 퀵 버튼 클릭 시,
			case QUICK_LOGIN_CLOUD:
				showLoginDialog();
				
				break;
				
			// 로그아웃 퀵 버튼 클릭 시,
			case QUICK_LOGOUT_CLOUD:
				
				RequestLogoutAsyncTask requestLogoutAsync = new RequestLogoutAsyncTask(mContext, CloudUtils.getLoginData(mContext, null));
				requestLogoutAsync.execute();
				
				requestLogoutAsync.setOnStatusListener(new OnStatusListener() {
					public void setOnStatusListener(CloudStatus status) {
						CloudUtils.doLogout(mContext, mBtnSyncCloud);
						initMemoListView();
					}
				});
				
				break;
			
		}
		
	}
	

	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(isChecked) {
			Toast.makeText(mContext, getString(R.string.toast_title_except_widget_memo), Toast.LENGTH_SHORT).show();
		}
		
		mDelAdapter.setAllCheckedItems(isChecked);
		mDelAdapter.notifyDataSetChanged();
		
	}

	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		mBtnSearch.performClick();
		
		return false;
	}
	
	/**
	 * Login 을 요청하는 다이얼로그를 화면에 표시하는 함수
	 * 
	 */
	public void showLoginDialog() {
		Utils.logPrint(getClass(), "showLogin Dialog 호출");
		
		final DigLogInCloud digLoginCloud = new DigLogInCloud(mContext);
		digLoginCloud.show();
		
		// 로그인 관련 시,
		digLoginCloud.setOnStatusListener(new OnStatusListener() {
			public void setOnStatusListener(CloudStatus status) {

				if(status != null) {
					Utils.logPrint(getClass(), "login status : " + status.getStatus());
					
					switch(status.getStatus()) {
					
						// 로그인 Status,
						// 로그인 성공 시,
						case CloudStatus.LOGIN_SUCCEED:
							Utils.logPrint(getClass(), "로그인 성공, 동기화 요청");
							
							// 로그인 상태로 전환 및 어댑터 초기화
							CloudUtils.doLogin(mContext, status.getData(), mBtnSyncCloud);
							initMemoListView();
							
							// 동기화 요청 및 어댑터 갱신
							doSyncCloudMemoList();
							
							break;
						
						// 로그인 실패 및 회원 가입 실패 시(존재하지 않는 회원)
						case CloudStatus.LOGIN_NO_MEMBER:
						case CloudStatus.LOGIN_INVALIDATE_PWD:
						case CloudStatus.LOGIN_INVALIDATE_AUTH:
						case CloudStatus.JOIN_SUCCEED:
						case CloudStatus.JOIN_FAILED:
						case CloudStatus.JOIN_DUPLICATED:
						case CloudStatus.SERVER_CANNOT_CONNECTED:
						case CloudStatus.SERVER_UNKNOWN_ERROR:
							
							String title = null, msg = null;
							if(status.getStatus() == CloudStatus.LOGIN_NO_MEMBER) {
								title = mContext.getString(R.string.tv_title_caution);
								msg = mContext.getString(R.string.dig_msg_login_no_member);
							} else if(status.getStatus() == CloudStatus.LOGIN_INVALIDATE_PWD) {
								title = mContext.getString(R.string.tv_title_caution);
								msg = mContext.getString(R.string.dig_msg_login_invalidate_pwd);
							} else if(status.getStatus() == CloudStatus.LOGIN_INVALIDATE_AUTH) {
								title = mContext.getString(R.string.tv_title_caution);
								msg = mContext.getString(R.string.dig_msg_login_invalidate_auth);
							} else if(status.getStatus() == CloudStatus.JOIN_SUCCEED) {
								title = mContext.getString(R.string.tv_title_info);
								msg = mContext.getString(R.string.dig_msg_join_member_succeed);
							} else if(status.getStatus() == CloudStatus.JOIN_FAILED) {
								title = mContext.getString(R.string.tv_title_caution);
								msg = mContext.getString(R.string.dig_msg_join_member_failed);
							} else if(status.getStatus() == CloudStatus.JOIN_DUPLICATED) {
								title = mContext.getString(R.string.tv_title_caution);
								msg = mContext.getString(R.string.dig_msg_join_member_duplicated);
							} else if(status.getStatus() == CloudStatus.SERVER_CANNOT_CONNECTED) {
								title = mContext.getString(R.string.tv_title_caution);
								msg = mContext.getString(R.string.dig_msg_server_cannot_connected);
							} else if(status.getStatus() == CloudStatus.SERVER_UNKNOWN_ERROR) {
								title = mContext.getString(R.string.tv_title_caution);
								msg = mContext.getString(R.string.dig_msg_server_unknown_error);
							}
							
							DigInfoConfirm digInfoConfirm = new DigInfoConfirm(mContext, title, msg);
							digInfoConfirm.show();
							
							digInfoConfirm.setOnDismissListener(new OnDismissListener() {
								public void onDismiss(DialogInterface dialog) {
									digLoginCloud.show();
								}
							});
							
							CloudUtils.doLogout(mContext, mBtnSyncCloud);
							initMemoListView();
							
							break;
					
					}
					
				}
				else {
					Utils.logPrint(getClass(), "Server 응답 에러, status 가 Null");
					
					DigInfoConfirm digInfoConfrim = 
						new DigInfoConfirm(mContext, getString(R.string.tv_title_caution), 
								getString(R.string.dig_msg_server_cannot_connected));
					
					digInfoConfrim.show();
					digInfoConfrim.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							CloudUtils.doLogout(mContext, mBtnSyncCloud);
							initMemoListView();
						}
					});
				}
			}
		});
		
		// 로그인 다이얼로그 취소, 클릭 시
		digLoginCloud.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				if(digLoginCloud.isCanceled()) {
					CloudUtils.doLogout(mContext, mBtnSyncCloud);
					initMemoListView();
				}
			}
		});
		
	}
	
	
	@Override
	public void onBackPressed() {

		if(mMemoMode == MEMO_LIST_STATE_DELETE) {
			ctrlMemoListState(MEMO_LIST_STATE_DEFAULT);
		} else { 
			finish();
		}
	}
	
	/*
     * 앱 최초 실행 시 수행하는 데이터베이스를 로드 하는 Async InnerClass
     * 
     */
    class InitLoadAsync extends AsyncTask<Void, Void, Void> {
    	
    	SharedPreferences.Editor configEditor;
    	
    	ProgressDialog proDig;
    	boolean isAppFirst, isDBSucceed, isCloudUpdateSucceed;
    	int cloudOnlineState;
    	
    	public InitLoadAsync(boolean isAppFirst) {
    		this.isAppFirst = isAppFirst;
    		this.configEditor = mConfigPref.edit();
		}
    	
    	
    	@Override
    	protected void onPreExecute() {
    		if(isAppFirst) {
    			proDig = ProgressDialog.show(mContext, "", getString(R.string.prodig_title_db_creating));
    		} else {
    			proDig = ProgressDialog.show(mContext, "", getString(R.string.prodig_title_cloud_sync_server));
    		}
    	}
    	
    	@Override
    	protected Void doInBackground(Void... params) {
    		// App 을 처음 실행 했을 시(Database 생성)
    		if(isAppFirst) {
    			isDBSucceed = LoadDatabase.loadDataBase(mContext.getResources().getAssets(), mContext);
    			Utils.logPrint(getClass(), "데이터베이스 성공 여부 : " + isDBSucceed);
    		}
    		// App 을 한번 이상 실행 했을 시(Cloud 상태 파악)
    		else {
    			cloudOnlineState = mConfigPref.getInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_OFFLINE);
    		}
    		
    		// Cloud DB Update 가 적용이 되지 않았을 경우,
    		isCloudUpdateSucceed = mConfigPref.getBoolean(Constants.PREF_CONFIG_IS_CLOUD_UPDATED, false);
    		
    		// 테스트 코드(상용화시 삭제)
//    		isCloudUpdateSucceed = false;
    		
    		// Cloud DB Update 
			if(!isCloudUpdateSucceed) {
				isCloudUpdateSucceed = Utils.doUpdateCloudDB(mContext);
			} 
			
			Utils.logPrint(getClass(), "Cloud Update 성공 여부 : " + isCloudUpdateSucceed);

    		return null;
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		if(proDig.isShowing()) {
    			proDig.dismiss();
    		}

    		// App 이  한번 이상 정상적으로 동작 하였을 경우,
    		if(!isAppFirst) {
    			initMemoListView();
    		} 
    		// App 을 처음 실행하여, LocalDB 와 CloudDB 가 정상적으로 생성, 업데이트 되었을 경우, 
    		else if(isCloudUpdateSucceed && isAppFirst && isDBSucceed) {
    			Utils.logPrint(getClass(), "App을 처음 실행하여, DB 생성에 성공하였습니다.");
    			
    			// 앱을 한 번 이상 실행했음으로 변경 (isAppFirst = false)
    			configEditor.putBoolean(Constants.PREF_CONFIG_APP_FIRST, false);
    			configEditor.commit();
    			
    			// 클라우드 서비스 이용 여부 다이얼로그 표시
    			final DigSelectUseCloud digSelectUseCloud = new DigSelectUseCloud(mContext);
    			digSelectUseCloud.show();
    			
    			digSelectUseCloud.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						switch(digSelectUseCloud.getCloudMode()) {
							// 클라우드 서비스 미 이용시,
							case DigSelectUseCloud.SELECT_NO_USED_CLOUD:
								// 로그아웃 호출
								CloudUtils.doLogout(mContext, mBtnSyncCloud);
								
								// 정보 다이얼로그 표시
								DigInfoConfirm digInfoConfirm = new DigInfoConfirm(mContext, 
										mContext.getString(R.string.tv_title_info), 
										mContext.getString(R.string.dig_msg_memo_no_login_explain));
								
								digInfoConfirm.show();
								
								// 정보 다이얼로그 확인 클릭 시, DB로 부터 불러와, 메모 리스트 설정
								digInfoConfirm.setOnDismissListener(new OnDismissListener() {
									public void onDismiss(DialogInterface dialog) {
										initMemoListView();
									}
								});
								
								break;
							
							// 클라우드 서비스 이용시,
							case DigSelectUseCloud.SELECT_USED_CLOUD:
								showLoginDialog();
								
								break;
						}
					}
				});
    			
    			
    		}
    		// App DB 생성 시, 문제가 발생하였을 경우,
    		else {
    			Utils.logPrint(getClass(), "App Database 생성 시, 문제가 발생하였습니다.");
    			
    			// LocalDB 나 CloudDB 생성에 실패했을 경우,
    			if(!isCloudUpdateSucceed) {
    				Toast.makeText(mContext, getString(R.string.toast_title_update_cloud_failed), Toast.LENGTH_SHORT).show();
    			} else if(!isDBSucceed){
    				Toast.makeText(mContext, getString(R.string.toast_title_db_creat_fail), Toast.LENGTH_SHORT).show();
    			}
    			
    			finish();
    		}

    	}
    }
	
    /**
     * DB 로 부터 데이터를 백업하고 복구하기 위한 AsyncTask
     * 
     * @author Acsha
     */
	class DataBackupRecoveryAsync extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog prgDig;
		boolean succeedFlag;
		int stateFlag;
		String restoreFileName;
		
		public DataBackupRecoveryAsync(int state) {
			this.stateFlag = state;
		}
		
		public DataBackupRecoveryAsync(int state, String restoreFileName) {
			this.stateFlag = state;
			this.restoreFileName = restoreFileName;
		}
		
		@Override
		protected void onPreExecute() {
			
			switch(stateFlag) {
				case MEMO_DATA_BACKUP:
					prgDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.prodig_title_db_backup));
					break;
				case MEMO_DATA_RESTORE:
					prgDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.prodig_title_db_restore));
					break;
			}
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			
			switch(stateFlag) {
				case MEMO_DATA_BACKUP:
					try {
						succeedFlag = Utils.dataBackUp(mContext);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;

				case MEMO_DATA_RESTORE:
					Utils.logPrint(getClass(), "복구할 파일 명 : " + restoreFileName);
					
					succeedFlag = Utils.dataRestore(mContext, restoreFileName);	
					
					if(succeedFlag) {
						Utils.convertOldDBToNewDB(mContext);
						Utils.initWidgetTable(mContext);
					}
					
					break;
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(prgDig.isShowing()) {
				prgDig.dismiss();
			}
			
			Utils.logPrint(getClass(), "성공 유무 : " + succeedFlag);
			
			switch(stateFlag) {
				case MEMO_DATA_BACKUP:
					if(succeedFlag) {
						Toast.makeText(mContext, getString(R.string.toast_title_backup_complete), Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(mContext, getString(R.string.toast_title_backup_failed), Toast.LENGTH_SHORT).show();
					}
					
					break;
				case MEMO_DATA_RESTORE:
					if(succeedFlag) {
						updateMemoListAdapter();
						Toast.makeText(mContext, getString(R.string.toast_title_restore_complete), Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(mContext, getString(R.string.toast_title_restore_failed), Toast.LENGTH_SHORT).show();
					}
					
					
					break;
			}
		}
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		mEditSearch.setFocusable(false);
		Utils.hideSoftKeyboard(mContext, mEditSearch);
		
		return super.dispatchTouchEvent(ev);
	}

	public boolean onItemLongClick(AdapterView<?> parent, View v, final int position, long id) {
		Utils.logPrint(getClass(), "롱 클릭 된 아이템 포지션을 삭제 : " + position);

		if(mLongclickItems == null) {
			mLongclickItems = new ArrayList<Memo>();
		} else {
			mLongclickItems.clear();
		}
		
		mLongclickItems.add(mItems.get(position));
		
		doRemoveMemo(mLongclickItems);
		
		return true;
	}
	
	private void doRemoveMemo(final ArrayList<Memo> memoArray) {
		final DigConfirm delConfirmDig = 
			new DigConfirm(mContext, getString(R.string.quick_delete_memo), 
							getString(R.string.dig_msg_memo_del), R.drawable.icon_quickaction_memo_delete);
		delConfirmDig.show();
		
		delConfirmDig.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				if(delConfirmDig.getDigState()) {
					
					int cloudState = mConfigPref.getInt(Constants.PREF_CONFIG_CLOUD_ONLINE_STATE, Constants.CLOUD_STATE_OFFLINE);
					switch(cloudState) {
					
						// 클라우드 서비스 오프라인 일 때,
						case Constants.CLOUD_STATE_OFFLINE:
							Utils.deleteMemo(mContext, memoArray);
							break;
							
						// 클라우드 서비스 온라인 일 때,
						case Constants.CLOUD_STATE_ONLINE:
							
							RequestRemoveAsyncTask requestRemoveAsyncTask = 
								new RequestRemoveAsyncTask(mContext, CloudUtils.getLoginData(mContext, Login.DEFAULT_PASSWORD), memoArray);
							requestRemoveAsyncTask.execute();
							
							requestRemoveAsyncTask.setOnStatusListener(new OnStatusListener() {
								
								public void setOnStatusListener(CloudStatus status) {
									
									// 서버와 응답에 성공했을 경우,
									if(status != null) {
										
										switch(status.getStatus()) {
										
											// 서버의 메모가 성공적으로 삭제되고, 목록을 성공적으로 불러왔을 경우,
											case CloudStatus.MEMO_DELETE_SUCCEED_LOAD_SUCCEED:
												
												updateCloudMemoListAdapter(status.getData());
												
												break;
												
											// 서버의 메모가 성공적으로 삭제되었으나, 목록 불러오기에 실패했을 경우,
											case CloudStatus.MEMO_DELETE_SUCCEED_LOAD_FAILED:
												
												Toast.makeText(mContext, 
														getString(R.string.toast_title_delete_succeed_load_failed), 
														Toast.LENGTH_SHORT).show();
												
												updateMemoListAdapter();
												
												break;
											
											// 메모 삭제에 실패했을 경우,
											case CloudStatus.MEMO_DELETE_FAILED:
												Toast.makeText(mContext, 
														getString(R.string.toast_title_delete_failed),
														Toast.LENGTH_SHORT).show();
												
												break;
											
											// 인증키가 유효하지 않을 경우,
											case CloudStatus.LOGIN_INVALIDATE_PWD:
												
												doCheckAuthFailed(mContext);
												
												break;
										
										}
										
									}
									// 서버와 응답에 실패했을 경우,
									else {
										Utils.logPrint(getClass(), "메모 삭제 중 서버와 응답에 실패하였습니다.");
										doLogoutToServerDown(mContext);
									}
									
								}
							});
							
							break;
					
					}
					
					// 메모 목록을 기본 어댑터로 변경
					ctrlMemoListState(MEMO_LIST_STATE_DEFAULT);
					
				}
			}
		});
	}

}

























