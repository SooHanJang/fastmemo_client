package madcat.studio.fastmemo.pro;

import madcat.studio.utils.Utils;

import org.bson.types.ObjectId;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class DummyLoad extends Activity {
	
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.mContext = this;
		
		Intent intent = new Intent(mContext, MemoList.class);
		startActivity(intent);
		
		finish();
		
		overridePendingTransition(0, 0);
	}
	
}


















